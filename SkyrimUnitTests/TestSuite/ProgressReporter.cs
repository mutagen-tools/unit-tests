﻿namespace SkyrimUnitTests.TestSuite; 

public interface IProgressReporter {
    public delegate void ProgressReportHandler(string message, float progress);
    public event ProgressReportHandler? Progress;
}
