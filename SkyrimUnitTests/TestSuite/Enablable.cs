﻿namespace SkyrimUnitTests.TestSuite; 

public interface IEnablable {
    public bool Enabled { get; set; }
    public bool DefaultEnabled { get; set; }
}
