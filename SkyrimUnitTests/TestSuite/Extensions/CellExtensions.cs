﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Skyrim;
using SkyrimUnitTests.TestSuite.Tester.Record;
namespace SkyrimUnitTests.TestSuite;

public static class CellExtensions {
    private static readonly ConcurrentDictionary<FormKey, IReadOnlySet<ILocationGetter>> CellLocationCache = new();

    public static IEnumerable<ILocationGetter> GetAllLocations(this ICellGetter cell) {
        if (CellLocationCache.TryGetValue(cell.FormKey, out var locations)) return locations;

        //Add all parent location form keys
        var cellLocations = new HashSet<ILocationGetter>();
        var location = cell.Location.TryResolve(IRecordTest.LinkCache);
        while (location != null) {
            cellLocations.Add(location);
            location = location.ParentLocation.TryResolve(IRecordTest.LinkCache);
        }

        //Get world locations
        if ((cell.Flags & Cell.Flag.IsInteriorCell) == 0) {
            var context = IRecordTest.LinkCache.ResolveContext<ICell, ICellGetter>(cell.FormKey);
            var world = (IWorldspaceGetter?) context.Parent?.Record;
            if (world != null) {
                foreach (var worldLocation in world.GetWorldLocations()) {
                    cellLocations.Add(worldLocation);
                }
            }
        }

        CellLocationCache.TryAdd(cell.FormKey, cellLocations);

        return cellLocations;
    }

    public static bool IsSettlementCell(this ICellGetter cell) {
        if (!cell.IsInteriorCell()) return false;
        var locations = cell.GetAllLocations().ToList();
        if (locations.Count == 0) return false;

        return locations.Exists(location => location.IsSettlementLocation());
    }

    public static bool IsInteriorCell(this ICellGetter cell) {
        return (cell.Flags & Cell.Flag.IsInteriorCell) != 0;
    }

    public static bool IsExteriorCell(this ICellGetter cell) {
        return (cell.Flags & Cell.Flag.IsInteriorCell) == 0;
    }

    public static bool IsPublic(this ICellGetter cell) {
        return (cell.Flags & Cell.Flag.PublicArea) != 0;
    }
}
