﻿using Mutagen.Bethesda.Skyrim;
namespace SkyrimUnitTests.TestSuite;

public static class ActorValueExtensions {
	public static bool IsSkill(this ActorValue actorValue) {
		return (int) actorValue is >= 6 and <= 23;
	}
}
