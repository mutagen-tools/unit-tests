﻿using System.Collections.Generic;
using System.Linq;
using Mutagen.Bethesda.Skyrim;
namespace SkyrimUnitTests.TestSuite;

public static class RecordExtension {
	public static IEnumerable<IConditionGetter>? GetConditions(this ISkyrimMajorRecordGetter record) {
		return record switch {
			ICameraPathGetter cameraPath => cameraPath.Conditions,
			IConstructibleObjectGetter constructibleObject => constructibleObject.Conditions,
			IDialogResponsesGetter dialogResponses => dialogResponses.Conditions,
			IFactionGetter faction => faction.Conditions,
			IIdleAnimationGetter idleAnimation => idleAnimation.Conditions,
			ILoadScreenGetter loadScreen => loadScreen.Conditions,
			IMagicEffectGetter magicEffect => magicEffect.Conditions,
			IMessageGetter message => message.MenuButtons.SelectMany(x => x.Conditions),
			IMusicTrackGetter musicTrack => musicTrack.Conditions,
			IPackageGetter package => package.Conditions,
			IPerkGetter perk => perk.Conditions,
			IQuestGetter quest => quest.DialogConditions
				.Concat(quest.EventConditions)
				.Concat(quest.Aliases.SelectMany(a => a.Conditions))
				.Concat(quest.Stages.SelectMany(s => s.LogEntries.SelectMany(e => e.Conditions)))
				.Concat(quest.Objectives.SelectMany(o => o.Targets.SelectMany(t => t.Conditions))),
			ISceneGetter scene => scene.Conditions,
			ISoundDescriptorGetter soundDescriptor => soundDescriptor.Conditions,
			IAStoryManagerNodeGetter storyManagerNode => storyManagerNode.Conditions,
			_ => []
		};
	}

	public static IEnumerable<ICondition>? GetConditions(this ISkyrimMajorRecord record) {
		return record switch {
			ICameraPath cameraPath => cameraPath.Conditions,
			IConstructibleObject constructibleObject => constructibleObject.Conditions,
			IDialogResponses dialogResponses => dialogResponses.Conditions,
			IFaction faction => faction.Conditions,
			IIdleAnimation idleAnimation => idleAnimation.Conditions,
			ILoadScreen loadScreen => loadScreen.Conditions,
			IMagicEffect magicEffect => magicEffect.Conditions,
			IMessage message => message.MenuButtons.SelectMany(x => x.Conditions),
			IMusicTrack musicTrack => musicTrack.Conditions,
			IPackage package => package.Conditions,
			IPerk perk => perk.Conditions,
			IQuest quest => quest.DialogConditions
				.Concat(quest.EventConditions)
				.Concat(quest.Aliases.SelectMany(a => a.Conditions))
				.Concat(quest.Stages.SelectMany(s => s.LogEntries.SelectMany(e => e.Conditions)))
				.Concat(quest.Objectives.SelectMany(o => o.Targets.SelectMany(t => t.Conditions))),
			IScene scene => scene.Conditions,
			ISoundDescriptor soundDescriptor => soundDescriptor.Conditions,
			IAStoryManagerNode storyManagerNode => storyManagerNode.Conditions,
			_ => []
		};
	}

}
