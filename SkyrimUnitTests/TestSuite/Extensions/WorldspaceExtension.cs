﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Skyrim;
using SkyrimUnitTests.TestSuite.Tester.Record;
namespace SkyrimUnitTests.TestSuite;

public static class WorldspaceExtension {
    private static readonly ConcurrentDictionary<FormKey, IReadOnlySet<ILocationGetter>> WorldLocationCache = new();
    public static IEnumerable<ILocationGetter> GetWorldLocations(this IWorldspaceGetter world) {
        var worldLocations = new HashSet<ILocationGetter>();
        
        if (WorldLocationCache.TryGetValue(world.FormKey, out var locations)) {
            worldLocations = [..locations];
        } else {
            var worldLocation = world.Location.TryResolve(IRecordTest.LinkCache);
            while (worldLocation != null) {
                worldLocations.Add(worldLocation);
                worldLocation = worldLocation.ParentLocation.TryResolve(IRecordTest.LinkCache);
            }
            WorldLocationCache.TryAdd(world.FormKey, worldLocations);
        }
        
        return worldLocations;
    }

}
