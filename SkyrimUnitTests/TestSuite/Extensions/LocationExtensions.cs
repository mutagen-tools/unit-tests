﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Mutagen.Bethesda;
using Mutagen.Bethesda.FormKeys.SkyrimSE;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Skyrim;
using SkyrimUnitTests.TestSuite.Tester.Record;
namespace SkyrimUnitTests.TestSuite;

public static class LocationExtensions {
    private static HashSet<FormKey>? _locationInteriorCache;

    public static bool IsLocationAppliedToInterior(this ILocationGetter location) {
        // todo with reference cache, this can be optimized
        if (_locationInteriorCache is null) {
            _locationInteriorCache = new HashSet<FormKey>();
            foreach (var cell in IRecordTest.LinkCache.PriorityOrder.WinningOverrides<ICellGetter>()) {
                if (cell.IsInteriorCell() && !cell.Location.FormKey.IsNull) {
                    _locationInteriorCache.Add(cell.Location.FormKey);
                }
            }
        }

        return _locationInteriorCache.Contains(location.FormKey);
    }

    private static readonly ConcurrentDictionary<FormKey, bool> SettlementLocationCache = new();

    public static bool IsSettlementLocation(this ILocationGetter location) {
        if (SettlementLocationCache.TryGetValue(location.FormKey, out var isSettlementLocation))
            return isSettlementLocation;

        if (location.Keywords is null) return false;

        var isSettlement = location.Keywords.Any(k =>
            k.FormKey == Skyrim.Keyword.LocTypeHabitation.FormKey
         || k.FormKey == Skyrim.Keyword.LocTypeHabitationHasInn.FormKey
         || k.FormKey == Skyrim.Keyword.LocTypeFarm.FormKey
         || k.FormKey == Skyrim.Keyword.LocTypeCity.FormKey
         || k.FormKey == Skyrim.Keyword.LocTypeHouse.FormKey
         || k.FormKey == Skyrim.Keyword.LocTypeSettlement.FormKey
         || k.FormKey == Skyrim.Keyword.LocTypePlayerHouse.FormKey
         || k.FormKey == Skyrim.Keyword.LocTypeInn.FormKey
         || k.FormKey == Skyrim.Keyword.LocTypeStore.FormKey);

        SettlementLocationCache.TryAdd(location.FormKey, isSettlement);

        return isSettlement;
    }

    public static bool IsSettlementHouseLocation(this ILocationGetter location) {
        if (location.Keywords is null) return false;

        return location.Keywords.Any(k =>
            k.FormKey == Skyrim.Keyword.LocTypeHouse.FormKey
         || k.FormKey == Skyrim.Keyword.LocTypePlayerHouse.FormKey
         || k.FormKey == Skyrim.Keyword.LocTypeInn.FormKey
         || k.FormKey == Skyrim.Keyword.LocTypeStore.FormKey);
    }

    public static bool IsSettlementHouseLocationNotPlayerHome(this ILocationGetter location) {
        if (location.Keywords is null) return false;

        if (location.Keywords.Any(k => k.FormKey == Skyrim.Keyword.LocTypePlayerHouse.FormKey)) {
            return false;
        }

        return location.Keywords.Any(k =>
            k.FormKey == Skyrim.Keyword.LocTypeHouse.FormKey
         || k.FormKey == Skyrim.Keyword.LocTypeInn.FormKey
         || k.FormKey == Skyrim.Keyword.LocTypeStore.FormKey);
    }

    public static bool IsInnLocation(this ILocationGetter location) {
        if (location.Keywords == null) return false;

        return location.Keywords.Any(k => k.FormKey == Skyrim.Keyword.LocTypeInn.FormKey);
    }
}
