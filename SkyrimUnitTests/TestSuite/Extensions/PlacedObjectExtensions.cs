﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Skyrim;
using SkyrimUnitTests.TestSuite.Tester.Record;
namespace SkyrimUnitTests.TestSuite;

public static class PlacedObjectExtensions {
    public static bool LeadsToExterior(this IPlacedObjectGetter placedObject, [MaybeNullWhen(false)] out IPlacedObjectGetter exteriorDoor) {
        exteriorDoor = null;

        // Has a teleport destination
        if (placedObject.TeleportDestination is null || placedObject.TeleportDestination.Door.IsNull) return false;
        // Teleport destination is a door
        if (!IRecordTest.LinkCache.TryResolve<IDoorGetter>(placedObject.Base.FormKey, out _)) return false;

        if (IRecordTest.LinkCache.TryResolveSimpleContext<IPlacedObjectGetter>(placedObject.TeleportDestination.Door.FormKey, out var destinationContext)
            && destinationContext.Parent?.Record is ICellGetter destinationCell) {
            exteriorDoor = destinationContext.Record;
            return destinationCell.IsExteriorCell();
        }

        return false;
    }

    public static bool LeadsToExterior(this IPlacedObjectGetter placedObject) {
        return LeadsToExterior(placedObject, out _);
    }

    private static HashSet<FormKey>? _merchantChests;
    public static bool IsMerchantChest(this IPlacedObjectGetter placedObject) {
        if (_merchantChests == null) {
            _merchantChests = new HashSet<FormKey>();
            foreach (var mod in IRecordTest.LinkCache.ListedOrder) {
                foreach (var faction in mod.EnumerateMajorRecords<IFactionGetter>()) {
                    if ((faction.Flags & Faction.FactionFlag.Vendor) != 0) {
                        _merchantChests.Add(faction.MerchantContainer.FormKey);
                    }
                }
            }
        }

        return _merchantChests.Contains(placedObject.FormKey);
    }

    private static readonly ConcurrentDictionary<FormKey, bool> BedCache = new();
    public static bool IsBed(this IPlacedObjectGetter placedObject) {
        var furnitureFormKey = placedObject.Base.FormKey;
        if (BedCache.TryGetValue(furnitureFormKey, out var isBed)) return isBed;

        isBed = IsBedImpl(furnitureFormKey);

        BedCache.TryAdd(furnitureFormKey, isBed);
        return isBed;
        
        static bool IsBedImpl(FormKey furnitureFormKey) {
            if (!IRecordTest.LinkCache.TryResolve<IFurnitureGetter>(furnitureFormKey, out var furniture)) return false;
            // if (furniture.EditorID == null) return false;
            // if (!furniture.EditorID.Contains("bed", StringComparison.OrdinalIgnoreCase)) return false;

            if (furniture.Markers is null) return false;
            return furniture.Markers.Any(m =>
                m.EntryPoints != null
                && (m.EntryPoints.Type & Furniture.AnimationType.Lay) != 0);
        }
    }
}
