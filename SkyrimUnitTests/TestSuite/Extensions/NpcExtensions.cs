﻿using System;
using System.Linq;
using Mutagen.Bethesda;
using Mutagen.Bethesda.FormKeys.SkyrimSE;
using Mutagen.Bethesda.Skyrim;
using SkyrimUnitTests.TestSuite.Tester.Record;
namespace SkyrimUnitTests.TestSuite;

public static class NpcExtensions {
    public static IScriptEntryGetter? GetScript(this INpcGetter npc, string name) {
        return npc.VirtualMachineAdapter?.Scripts.FirstOrDefault(script => script.Name == name);
    }
    
    public static bool HasScript(this INpcGetter npc, string name) {
        return npc.GetScript(name) is not null;
    }
    
    public static bool HasFaction(this INpcGetter npc, Predicate<string?> stringCompare) {
        foreach (var rankPlacement in npc.Factions) {
            if (!IRecordTest.LinkCache.TryResolve<IFactionGetter>(rankPlacement.Faction.FormKey, out var faction)) continue;

            if (stringCompare(faction.EditorID)) return true;
        }

        return false;
    }
    
    public static bool HasFaction(this INpcGetter npc, string editorId) {
        return npc.HasFaction(eId => string.Equals(eId, editorId, StringComparison.OrdinalIgnoreCase));
    }
    
    public static bool IsGhost(this INpcGetter npc) {
        return npc.HasKeyword(Skyrim.Keyword.ActorTypeGhost);
    }
    
    public static bool IsUnique(this INpcGetter npc) {
        return npc.Configuration.Flags.HasFlag(NpcConfiguration.Flag.Unique);
    }
}
