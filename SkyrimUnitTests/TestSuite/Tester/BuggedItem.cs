﻿using System;
using System.Collections.Generic;
using System.Text;
namespace SkyrimUnitTests.TestSuite.Tester;

public abstract class BuggedItem : IComparable {
	public List<string> Bugs { get; set; } = new();
	public string BugDescription {
		get {
			var sb = new StringBuilder();
			foreach (var bug in Bugs) {
				sb.AppendLine(bug);
			}

			return sb.ToString();
		}
	}

	public virtual int CompareTo(object? obj) {
		if (obj is not BuggedItem other) return 1;

		return ReferenceEquals(this, other) ? 0 : 1;
	}

	public override string ToString() {
		var str = string.Empty;

		if (!string.IsNullOrEmpty(BugDescription)) {
			str += Environment.NewLine + BugDescription;
		}

		return str;
	}
}
