﻿using System.Collections.Generic;
using System.Windows.Media;
namespace SkyrimUnitTests.TestSuite.Tester;

public enum BugSeverity {
    Error,
    Warning,
    Inconsistency
}

public interface IBug : IEnablable {
    public static readonly Dictionary<BugSeverity, SolidColorBrush> SeverityColors = new() {
        { BugSeverity.Error, Brushes.Tomato },
        { BugSeverity.Warning, Brushes.Orange },
        { BugSeverity.Inconsistency, Brushes.Khaki }
    };
    
    public string Name { get; }
    public string Description { get; }
    public BugSeverity Severity { get; }
    public SolidColorBrush Color { get; }
}

public interface IBug<TBuggedItem> : IBug {
    public HashSet<TBuggedItem> BuggedItems { get; set; }
}

public interface IBug<in TContainerSetter, in TContainerGetter, in TItemSetter, in TItemGetter, TBuggedItem> : IBug<TBuggedItem> {
    public bool Verify(TItemGetter item, TContainerGetter? container = default);
    public void Patch(TItemSetter item, TContainerSetter? container = default);
    public bool HasPatch { get; }
}