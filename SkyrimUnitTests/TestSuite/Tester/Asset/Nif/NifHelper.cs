﻿using System;
using System.Collections.Generic;
using System.IO;
using nifly;
using SkyrimUnitTests.TestSuite.Tester.Record;
namespace SkyrimUnitTests.TestSuite.Tester.Asset.Nif;

public sealed record ExtendedNifFile(NifFile Nif, string Path, string FileName) : IDisposable {
    public void Dispose() => Nif.Dispose();
}
public sealed record ExtendedNiObject(NiObject NiObject, uint BlockID = uint.MaxValue) : IDisposable {
    public void Dispose() => NiObject.Dispose();
}
public sealed record ExtendedBlock<T>(T Block, uint BlockID = uint.MaxValue) : IDisposable
    where T : NiObject {
    public void Dispose() => Block.Dispose();
}

public static class NifHelper {
    public static bool HasCollision(this niflycpp.BlockCache cache, out uint blockId) {
        for (uint id = 0; id < cache.Header.GetNumBlocks(); id++) {
            var block = cache.EditableBlockById<bhkConvexShape>(id);
            if (block != null) {
                blockId = id;
                return true;
            }

            var block2 = cache.EditableBlockById<bhkCompressedMeshShapeData>(id);
            if (block2 != null) {
                blockId = id;
                return true;
            }
        }

        blockId = uint.MaxValue;
        return false;
    }

    public static IEnumerable<T> EnumerateBlocks<T>(this NifFile nif) where T : NiObject {
        var niHeader = nif.GetHeader();
        for (uint blockID = 0; blockID < niHeader.GetNumBlocks(); blockID++) {
            var block = niHeader.GetBlockById(blockID);
            if (block is not T t) continue;

            yield return t;
        }
    }

    public static bool Validate(this Color3 color) {
        return color.r is >= 0 and <= 1
            && color.g is >= 0 and <= 1
            && color.b is >= 0 and <= 1;
    }

    public static bool Validate(this Color4 color) {
        return color.r is >= 0 and <= 1
            && color.g is >= 0 and <= 1
            && color.b is >= 0 and <= 1
            && color.a is >= 0 and <= 1;
    }

    public static bool ValidateColor(this Vector3 color) {
        return color.x is >= 0 and <= 1
            && color.y is >= 0 and <= 1
            && color.z is >= 0 and <= 1;
    }

    public static string? Diffuse(this BSShaderTextureSet textureSet) => textureSet.textures.items()[0].get();
    public static string? Normal(this BSShaderTextureSet textureSet) => textureSet.textures.items()[1].get();
    public static string? Glow(this BSShaderTextureSet textureSet) => textureSet.textures.items()[2].get();
    public static string? Height(this BSShaderTextureSet textureSet) => textureSet.textures.items()[3].get();
    public static string? Cube(this BSShaderTextureSet textureSet) => textureSet.textures.items()[4].get();
    public static string? EnvironmentMask(this BSShaderTextureSet textureSet) => textureSet.textures.items()[5].get();
    public static string? Subsurface(this BSShaderTextureSet textureSet) => textureSet.textures.items()[6].get();
    public static string? Backlight(this BSShaderTextureSet textureSet) => textureSet.textures.items()[7].get();

    public static IEnumerable<T> EnumerateEditableBlocks<T>(this NifFile nif) where T : NiObject {
        using var blockCache = new niflycpp.BlockCache(niflycpp.BlockCache.SafeClone<NiHeader>(nif.GetHeader()));
        return EnumerateEditableBlocks<T>(blockCache);
    }

    public static IEnumerable<T> EnumerateEditableBlocks<T>(this niflycpp.BlockCache blockCache) where T : NiObject {
        for (uint blockID = 0; blockID < blockCache.Header.GetNumBlocks(); blockID++) {
            var block = blockCache.EditableBlockById<T>(blockID);
            if (block is null) continue;

            yield return block;
        }
    }

    public static bool InvalidMaterial(uint material) {
        switch (material) {
            case 131151687:
            case 365420259:
            case 398949039:
            case 428587608:
            case 438912228:
            case 493553910:
            case 500811281:
            case 591247106:
            case 732141076:
            case 781661019:
            case 790784366:
            case 873356572:
            case 899511101:
            case 1024582599:
            case 1060167844:
            case 1264672850:
            case 1286705471:
            case 1288358971:
            case 1305674443:
            case 1461712277:
            case 1486385281:
            case 1550912982:
            case 1560365355:
            case 1570821952:
            case 1607128641:
            case 1803571212:
            case 1848600814:
            case 1885326971:
            case 1886078335:
            case 2022742644:
            case 2025794648:
            case 2168343821:
            case 2229413539:
            case 2518321175:
            case 2617944780:
            case 2632367422:
            case 2892392795:
            case 2965929619:
            case 2974920155:
            case 3049421844:
            case 3070783559:
            case 3074114406:
            case 3106094762:
            case 3424720541:
            case 3448167928:
            case 3589100606:
            case 3702389584:
            case 3708432437:
            case 3725505938:
            case 3739830338:
            case 3741512247:
            case 3839073443:
            case 3969592277:
            case 4283869410:
                return false;
            default:
                return true;
        }
    }
    
    public static bool InvalidLayer(uint layer) => layer is < 1 or > 46;

    public static bool HasTextKey(string file, string controller, string contained) {
        file = Path.Combine(IRecordTest.Environment.DataFolderPath, file);
        if (!File.Exists(file)) return true;

        var nif = new NifFile();
        nif.Load(file);

        if (!nif.IsValid()) return true;

        var blockCache = new niflycpp.BlockCache(niflycpp.BlockCache.SafeClone<NiHeader>(nif.GetHeader()));
        for (uint blockID = 0; blockID < blockCache.Header.GetNumBlocks(); blockID++) {
            var niControllerSequence = blockCache.EditableBlockById<NiControllerSequence>(blockID);
            if (niControllerSequence == null) continue;
            var controllerName = niControllerSequence.name.get();
            if (controllerName.Equals(controller, StringComparison.OrdinalIgnoreCase)) {
                
            }

            var indices = new vectoruint32();
            niControllerSequence.GetChildIndices(indices);

            foreach (var index in indices) {
                var niTextKeyExtraData = blockCache.EditableBlockById<NiTextKeyExtraData>(index);
                if (niTextKeyExtraData == null) continue;

                var keys = new setNiRef();
                niTextKeyExtraData.textKeys.GetPtrs(keys);
                var ind = new vectoruint32();
                niTextKeyExtraData.textKeys.GetChildIndices(ind);
                var child = new setNiRef();
                niTextKeyExtraData.textKeys.GetChildRefs(child);
                var x = new vectorNiStringRefPtr();
                niTextKeyExtraData.textKeys.GetStringRefs(x);
                foreach (var niStringRef in x) {
                    var s = niStringRef.get();
                    if (s.StartsWith(contained, StringComparison.OrdinalIgnoreCase)) {
                        return false;
                    }
                }
            }
        }

        return true;
    }
}
