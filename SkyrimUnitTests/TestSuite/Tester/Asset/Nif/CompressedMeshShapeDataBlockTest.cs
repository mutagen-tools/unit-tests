﻿using System.Collections.Generic;
using nifly;
namespace SkyrimUnitTests.TestSuite.Tester.Asset.Nif;

public class CompressedMeshShapeDataBlockTest : NifBlockTest<bhkCompressedMeshShapeData> {
    public override string Name => "CompressedMeshShapeData";
    
    public override List<INifBug> Bugs { get; } = new() {
        new NifBlockBug<bhkCompressedMeshShapeData>("Invalid Material", "Only a specific subset of numbers are numbers are valid materials",
            BugSeverity.Warning, true, TestMaterial),
        new NifBlockBug<bhkCompressedMeshShapeData>("Invalid Layer", "Only a specific subset of numbers are numbers are valid layers",
            BugSeverity.Warning, true, TestLayer)
    };
    
    private static bool TestMaterial(bhkCompressedMeshShapeData block, ExtendedNifFile nif, List<string> list) {
        var allValid = true;
        
        foreach (var material in block.materials.items()) {
            if (NifHelper.InvalidMaterial(material.material)) {
                list.Add($"{material.material} is an invalid material");
                allValid = false;
            }
        }

        return allValid;
    }
    
    private static bool TestLayer(bhkCompressedMeshShapeData block, ExtendedNifFile nif, List<string> list) {
        var allValid = true;
        
        foreach (var material in block.materials.items()) {
            if (NifHelper.InvalidLayer(material.layer.layer)) {
                list.Add($"{material.layer.layer} is an invalid layer");
                allValid = false;
            }
        }
        
        return allValid;
    }
}