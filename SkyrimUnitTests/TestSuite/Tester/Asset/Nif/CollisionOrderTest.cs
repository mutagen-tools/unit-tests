﻿using System;
using System.Collections.Generic;
using System.Linq;
using nifly;
namespace SkyrimUnitTests.TestSuite.Tester.Asset.Nif;

public class CollisionOrderTest : NifTest {
	public override string Name => "Order";

	public override List<INifBug> Bugs { get; } = new() {
		new NifFileBug("Collision Order", "The collision shape needs to be the first block after all extra data blocks and controller blocks",
			BugSeverity.Error, true, TestCollisionOrder)
	};

	private static bool TestCollisionOrder(ExtendedNifFile nif, List<string> list) {
		var blockCache = new niflycpp.BlockCache(niflycpp.BlockCache.SafeClone<NiHeader>(nif.Nif.GetHeader()));

		if (nif.Path.Contains("BSM_ATBrazier01a", StringComparison.OrdinalIgnoreCase)) {
			Console.WriteLine();
		}

		var fadeNode = blockCache.EditableBlockById<BSFadeNode>(0);
		if (fadeNode == null) return true;

		// Check if there is any collision, but also skip ones where the collision isn't directly under the fade node
		if (fadeNode.collisionRef.index == uint.MaxValue || !blockCache.HasCollision(out var firstCollisionIndex)) return true;

		var lastReferencedIndex = uint.MaxValue;

		//Get last extra data index
		if (fadeNode.extraDataRefs is not null) {
			var extraData = fadeNode.extraDataRefs.GetRefs();

			if (extraData.Count > 0) {
				lastReferencedIndex = extraData.Max(x => x.index);
			}
		}

		//Get last controller index
		if (fadeNode.controllerRef.index != uint.MaxValue) {
			var controller = blockCache.EditableBlockById<NiControllerManager>(fadeNode.controllerRef.index);
			if (controller != null) {
				var childIndices = new vectoruint32();
				controller.GetChildIndices(childIndices);

				if (childIndices.Count > 0) { 
					lastReferencedIndex = childIndices.Max();
				}
			}
		}

		if (lastReferencedIndex == uint.MaxValue) return true;

		//Skip NiNodes
		var niNode = blockCache.EditableBlockById<NiNode>(lastReferencedIndex + 1);
		while (niNode != null) {
			lastReferencedIndex++;
			niNode = blockCache.EditableBlockById<NiNode>(lastReferencedIndex + 1);
		}

		return lastReferencedIndex + 1 == firstCollisionIndex;
	}
}
