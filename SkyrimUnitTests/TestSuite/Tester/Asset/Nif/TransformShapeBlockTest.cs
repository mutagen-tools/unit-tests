﻿using System.Collections.Generic;
using nifly;
namespace SkyrimUnitTests.TestSuite.Tester.Asset.Nif;

public class TransformShapeBlockTest : NifBlockTest<bhkTransformShape> {
    public override string Name => "TransformShape";
    
    public override List<INifBug> Bugs { get; } = new() {
        new NifBlockBug<bhkTransformShape>("Invalid Material", "Only a specific subset of numbers are numbers are valid materials",
            BugSeverity.Warning, true, TestMaterial)
    };
    
    private static bool TestMaterial(bhkTransformShape block, ExtendedNifFile nif, List<string> list) {
        if (!NifHelper.InvalidMaterial(block.material)) return true;
        list.Add($"{block.material} is an invalid material");

        return false;
    }
}