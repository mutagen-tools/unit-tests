﻿using System.Collections.Generic;
using nifly;
namespace SkyrimUnitTests.TestSuite.Tester.Asset.Nif;

public class EffectShaderPropertyTest : NifBlockTest<BSEffectShaderProperty> {
	public override string Name => "Effect Shader Property";

	public override List<INifBug> Bugs { get; } = [
		new NifBlockBug<BSEffectShaderProperty>("Colors out of bounds", "Colors should be between 0 and 1",
			BugSeverity.Warning, true, TestColor),
	];

    private static bool TestColor(BSEffectShaderProperty block, ExtendedNifFile nif, List<string> list) {
        if (!block.baseColor.Validate()) {
            list.Add("Base Color out of bounds");
        }
        
        if (!block.emittanceColor.Validate()) {
            list.Add("Emittance Color out of bounds");
        }
        
        return list.Count == 0;
    }
}
