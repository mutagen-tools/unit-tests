﻿using System.Collections.Generic;
using System.Linq;
using nifly;
namespace SkyrimUnitTests.TestSuite.Tester.Asset.Nif;

public class InterpControllerTest : NifBlockTest<NiInterpController> {
	public override string Name => "Interp Controller";

	public override List<INifBug> Bugs { get; } = [
		new NifBlockBug<NiInterpController>("No Animated Flag", "No animated flag set on BSXFlags",
			BugSeverity.Warning, true, TestAnimatedFlag, PatchAnimatedFlag),
	];

	private static bool TestAnimatedFlag(NiInterpController block, ExtendedNifFile nif, List<string> list) {
		var bsxFlags = nif.Nif.EnumerateBlocks<BSXFlags>().FirstOrDefault();
		if (bsxFlags is null) {
			list.Add("BSXFlags not found");
			return false;
		}

		return ((BSXFlagsEnum) bsxFlags.integerData & BSXFlagsEnum.BSX_ANIMATED) != 0;
	}

	private static void PatchAnimatedFlag(ExtendedBlock<NiInterpController> extendedBlock, ExtendedNifFile nif) {
		var bsxFlags = nif.Nif.EnumerateEditableBlocks<BSXFlags>().FirstOrDefault();
		if (bsxFlags is not null) {
			bsxFlags.integerData |= (int) BSXFlagsEnum.BSX_ANIMATED;
		}
	}
}
