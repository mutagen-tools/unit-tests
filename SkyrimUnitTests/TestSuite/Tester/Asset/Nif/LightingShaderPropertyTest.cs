﻿using System;
using System.Collections.Generic;
using System.Linq;
using nifly;
using Noggog;
namespace SkyrimUnitTests.TestSuite.Tester.Asset.Nif;

public class LightingShaderPropertyTest : NifBlockTest<BSLightingShaderProperty> {
	public override string Name => "Lighting Shader Property";

	public override List<INifBug> Bugs { get; } = [
		new NifBlockBug<BSLightingShaderProperty>("Missing Required Properties", "Textures need a certain shader type and flags to work properly",
			BugSeverity.Warning, true, TestRequiredProperties),
		new NifBlockBug<BSLightingShaderProperty>("Check missing shadow flags", "Cast and receive shadow flags should usually be set",
			BugSeverity.Warning, true, TestShadowFlags, PatchShadowFlags),
		new NifBlockBug<BSLightingShaderProperty>("Colors out of bounds", "Colors should be between 0 and 1",
			BugSeverity.Warning, true, TestColor)
	];

	private static readonly HashSet<string> BlacklistedFileNames = [
		"Rug"
	];

	private static readonly HashSet<string> BlacklistedFilePaths = [
		"fx"
	];

	private static bool TestShadowFlags(BSLightingShaderProperty block, ExtendedNifFile nif, List<string> list) {
		if (BlacklistedFileNames.Any(x => nif.FileName.Contains(x, StringComparison.OrdinalIgnoreCase))) return true;
		if (BlacklistedFilePaths.Any(x => nif.Path.Contains(x, StringComparison.OrdinalIgnoreCase))) return true;

		var flags1 = (SkyrimShaderPropertyFlags1) block.shaderFlags1;
		if ((flags1 & SkyrimShaderPropertyFlags1.SLSF1_CAST_SHADOWS) == 0) {
			list.Add("Cast Shadows missing");
		}
		if ((flags1 & SkyrimShaderPropertyFlags1.SLSF1_RECEIVE_SHADOWS) == 0) {
			list.Add("Receive Shadows missing");
		}

		return list.Count == 0;
	}

	private static void PatchShadowFlags(ExtendedBlock<BSLightingShaderProperty> extendedBlock, ExtendedNifFile nif) {
		foreach (var shader in nif.Nif.EnumerateEditableBlocks<BSLightingShaderProperty>()) {
			shader.shaderFlags1 |= (uint) SkyrimShaderPropertyFlags1.SLSF1_CAST_SHADOWS | (uint) SkyrimShaderPropertyFlags1.SLSF1_RECEIVE_SHADOWS;
		}
	}

	private static bool TestRequiredProperties(BSLightingShaderProperty block, ExtendedNifFile nif, List<string> list) {
		var niHeader = nif.Nif.GetHeader();

		var textureSetRef = niHeader.GetBlockById(block.textureSetRef.index);
		if (textureSetRef is not BSShaderTextureSet textureSet) return true;

		var shaderFlags1 = (SkyrimShaderPropertyFlags1) block.shaderFlags1;
		var shaderFlags2 = (SkyrimShaderPropertyFlags2) block.shaderFlags2;
		var shaderType = (ShaderType) block.GetShaderType();

		var count = textureSet.textures.items().Count;

		if (count == 0) return list.Count == 0;
		if (!textureSet.Diffuse().IsNullOrWhitespace()) {}

		if (count == 1) return list.Count == 0;
		if (!textureSet.Normal().IsNullOrWhitespace()) {
			if ((shaderFlags1 & SkyrimShaderPropertyFlags1.SLSF1_SPECULAR) == 0) {
				list.Add("Has normal map but specular flag is not set in Shader Flags 1");
			}
		}

		if (count == 2) return list.Count == 0;
		if (!textureSet.Glow().IsNullOrWhitespace()) {
			if (shaderType != ShaderType.ST_GlowShader) {
				list.Add("Has glow map but Shader Type is not glow shader");
			}

			if ((shaderFlags1 & SkyrimShaderPropertyFlags1.SLSF1_OWN_EMIT) == 0) {
				list.Add("Has glow map but own emit flag is not set in Shader Flags 1");
			}

			if ((shaderFlags2 & SkyrimShaderPropertyFlags2.SLSF2_GLOW_MAP) == 0) {
				list.Add("Has glow map but glow map flag is not set in Shader Flags 2");
			}

			if ((shaderFlags1 & SkyrimShaderPropertyFlags1.SLSF1_ENVIRONMENT_MAPPING) != 0) {
				list.Add("Has glow map but environment map flag is set in Shader Flags 1 - both can't be enabled at the same time");
			}
		}

		if (count == 3) return list.Count == 0;
		if (!textureSet.Height().IsNullOrWhitespace()) {
			if (shaderType != ShaderType.ST_Heightmap) {
				list.Add("Has cube map but Shader Type is not heightmap");
			}

			if ((shaderFlags1 & SkyrimShaderPropertyFlags1.SLSF1_PARALLAX) == 0) {
				list.Add("Has height map but parallax flag is not set in Shader Flags 1");
			}
		}

		if (count == 4) return list.Count == 0;
		if (!textureSet.Cube().IsNullOrWhitespace()) {
			if (shaderType != ShaderType.ST_EnvironmentMap) {
				list.Add("Has cube map but Shader Type is not environment map");
			}

			if ((shaderFlags1 & SkyrimShaderPropertyFlags1.SLSF1_ENVIRONMENT_MAPPING) == 0) {
				list.Add("Has cube map but environment map flag is not set in Shader Flags 1");
			}

			if ((shaderFlags2 & SkyrimShaderPropertyFlags2.SLSF2_GLOW_MAP) != 0) {
				list.Add("Has cube map but glow map flag is not set in Shader Flags 2 - both can't be enabled at the same time");
			}
		}

		if (count == 5) return list.Count == 0;
		if (!textureSet.EnvironmentMask().IsNullOrWhitespace()) {
			if (shaderType != ShaderType.ST_EnvironmentMap) {
				list.Add("Has an environment mask but Shader Type is not environment map");
			}

			if ((shaderFlags1 & SkyrimShaderPropertyFlags1.SLSF1_ENVIRONMENT_MAPPING) == 0) {
				list.Add("Has an environment mask but environment map flag is not set in Shader Flags 1");
			}
		}

		if (count == 6) return list.Count == 0;
		if (!textureSet.Subsurface().IsNullOrWhitespace()) {
			if (shaderType != ShaderType.ST_SkinTint) {
				list.Add("Has subsurface map but Shader Type is not skin tint");
			}

			if ((shaderFlags1 & SkyrimShaderPropertyFlags1.SLSF1_FACEGEN_RGB_TINT) == 0) {
				list.Add("Has subsurface map but facegen rgb tint flag is not set in Shader Flags 1");
			}
		}

		if (!textureSet.Backlight().IsNullOrWhitespace()) {
			if ((shaderFlags2 & SkyrimShaderPropertyFlags2.SLSF2_BACK_LIGHTING) == 0) {
				list.Add("Has backlight map but back lighting flag is not set in Shader Flags 2");
			}
		}

		return list.Count == 0;
	}

    private static bool TestColor(BSLightingShaderProperty block, ExtendedNifFile nif, List<string> list) {
        if (!block.emissiveColor.ValidateColor()) {
            list.Add("Emissive Color out of bounds");
        }

        if (!block.specularColor.ValidateColor()) {
            list.Add("Specular Color out of bounds");
        }

        if (!block.subsurfaceColor.Validate()) {
            list.Add("Subsurface Color out of bounds");
        }

        if (!block.hairTintColor.ValidateColor()) {
            list.Add("Hair Tint Color out of bounds");
        }

        if (!block.skinTintColor.ValidateColor()) {
            list.Add("Skin Tint Color out of bounds");
        }

        return list.Count == 0;
    }
}
