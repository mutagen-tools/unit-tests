﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using nifly;
namespace SkyrimUnitTests.TestSuite.Tester.Asset.Nif;

public class MissingBlocksTest : NifTest {
	public override string Name => "Missing Blocks";

	public override List<INifBug> Bugs { get; } = new() {
		new NifFileBug("No BSX Block", "A mesh without BSX can't be selected in the Creation Kit",
			BugSeverity.Warning, true, TestBSX)
	};

	private static readonly List<string> BlacklistedBSXPaths = new() {
		Path.Combine("FaceGenData", "FaceGeom"),
		"Grass",
		"LOD"
	};

	private static bool TestBSX(ExtendedNifFile nif, List<string> list) {
		if (BlacklistedBSXPaths.Any(p => nif.Path.Contains(p, StringComparison.OrdinalIgnoreCase))) return true;

		var bsxFlags = nif.Nif.EnumerateBlocks<BSXFlags>().FirstOrDefault();
		return bsxFlags is not null;
	}
}
