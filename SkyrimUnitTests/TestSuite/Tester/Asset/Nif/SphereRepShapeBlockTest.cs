﻿using System.Collections.Generic;
using nifly;
namespace SkyrimUnitTests.TestSuite.Tester.Asset.Nif;

public class SphereRepShapeBlockTest : NifBlockTest<bhkSphereRepShape> {
    public override string Name => "SphereRepShape";
    
    public override List<INifBug> Bugs { get; } = new() {
        new NifBlockBug<bhkSphereRepShape>("Invalid Material", "Only a specific subset of numbers are numbers are valid materials",
            BugSeverity.Warning, true, TestMaterial)
    };
    
    private static bool TestMaterial(bhkSphereRepShape block, ExtendedNifFile nif, List<string> list) {
        if (!NifHelper.InvalidMaterial(block.GetMaterial())) return true;
        list.Add($"{block.GetMaterial()} is an invalid material");

        return false;
    }
}