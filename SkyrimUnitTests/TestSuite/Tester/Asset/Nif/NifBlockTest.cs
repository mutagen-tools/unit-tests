﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using nifly;
namespace SkyrimUnitTests.TestSuite.Tester.Asset.Nif; 

public abstract class NifBlockTest<TBlock> : INifTest where TBlock : NiObject {
    public bool Enabled { get; set; } = true;
    public bool DefaultEnabled { get; set; }

    public virtual string Name => string.Empty;
    public virtual IEnumerable<INifBug> Bugs { get; } = new List<INifBug>();
    
    public event IProgressReporter.ProgressReportHandler? Progress;
    
    public bool Verify(string container, INifBug? bug) {
        if (!Enabled || string.IsNullOrWhiteSpace(container)) return false;
        
        var noBugs = true;
        var files = Directory.EnumerateFiles(container, "*.nif*", SearchOption.AllDirectories).ToList();
        var total = files.Count / 100.0f;

        for (var i = 0; i < files.Count; i++) {
            var file = files[i];
            Progress?.Invoke(file, i / total);
            
            //Load nif file
            if (!File.Exists(file)) continue;

            var fileName = Path.GetFileName(file);
            using var nif = new NifFile();
            nif.Load(file);
        
            if (!nif.IsValid()) continue;
        
            using var blockCache = new niflycpp.BlockCache(niflycpp.BlockCache.SafeClone<NiHeader>(nif.GetHeader()));
            for (uint blockID = 0; blockID<blockCache.Header.GetNumBlocks(); blockID++) {
                using var block = blockCache.EditableBlockById<TBlock>(blockID);
                if (block == null) continue;

                var extendedNiObject = new ExtendedNiObject(block, blockID);
                var extendedNifFile = new ExtendedNifFile(nif, file, fileName);
                var bugs = Bugs.Where(b => b.Enabled && !b.Verify(extendedNiObject, extendedNifFile));
                if (bugs.Any()) noBugs = false;
            }
        }

        return noBugs;
    }
    
    public string GetErrorOutput() {
        var output = new StringBuilder();
        foreach (var bug in Bugs) {
            foreach (var asset in bug.BuggedItems) output.AppendLine(asset.Path);
        }
        return output.ToString();
    }
    
    public void Patch(string container, INifBug bug) {
        if (!bug.Enabled || !bug.HasPatch) return;

        var i = 0;
        var nifFiles = bug.BuggedItems.GroupBy(x => x.FullPath).ToList();
        var total = nifFiles.Count / 100.0f;
        foreach (var nifFile in nifFiles) {
            Progress?.Invoke(nifFile.Key, i++ / total);
            PatchMany(container, bug, nifFile.ToList());
        }
    }

    public void PatchMany(string container, INifBug bug, List<BuggedNif> buggedBlocks) {
        if (!bug.Enabled || !bug.HasPatch) return;
        
        //Load nif file
        var path = buggedBlocks[0].FullPath;
        if (!File.Exists(path)) return;

        var fileName = Path.GetFileName(path);
        using var nif = new NifFile();
        nif.Load(path);
        
        if (!nif.IsValid()) return;
        
        //Patch bugs and save nif
        using var blockCache = new niflycpp.BlockCache(niflycpp.BlockCache.SafeClone<NiHeader>(nif.GetHeader()));
        foreach (var buggedBlock in buggedBlocks) {
            using var block = blockCache.EditableBlockById<TBlock>(buggedBlock.BlockID);
            bug.Patch(new ExtendedNiObject(block, buggedBlock.BlockID), new ExtendedNifFile(nif, path, fileName));
            blockCache.Header.ReplaceBlock(buggedBlock.BlockID, block);
        }

        nif.Save(path);
    }

    public void Patch(string container, INifBug bug, BuggedNif item) {
        if (!bug.Enabled || !bug.HasPatch) return;
        
        //Load nif file
        if (!File.Exists(item.FullPath)) return;

        using var nif = new NifFile();
        nif.Load(item.FullPath);
        
        if (!nif.IsValid()) return;
        
        //Patch bugs and save nif
        using var blockCache = new niflycpp.BlockCache(niflycpp.BlockCache.SafeClone<NiHeader>(nif.GetHeader()));
        using var block = blockCache.EditableBlockById<TBlock>(item.BlockID);
        bug.Patch(new ExtendedNiObject(block, item.BlockID), new ExtendedNifFile(nif, item.FullPath, Path.GetFileName(item.Path)));
        blockCache.Header.ReplaceBlock(item.BlockID, block);

        nif.Save(item.FullPath);
    }
}