﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using nifly;
using Noggog;
namespace SkyrimUnitTests.TestSuite.Tester.Asset.Nif;

public interface INifTest : ITest<string, string, BuggedNif, INifBug> {
    public static IEnumerable<INifTest> Tests { get; } = typeof(INifTest).GetSubclassesOf().NotNull()
        .Select(subclass => (INifTest?) System.Activator.CreateInstance(subclass)).NotNull();
}

// This was never tested and will probably not work correctly
public abstract class NifTest : INifTest {
    public bool Enabled { get; set; } = true;
    public bool DefaultEnabled { get; set; }

    public virtual string Name => string.Empty;
    public virtual IEnumerable<INifBug> Bugs { get; } = new List<INifBug>();
    
    public event IProgressReporter.ProgressReportHandler? Progress;
    
    public bool Verify(string container, INifBug? bug) {
        if (!Enabled || string.IsNullOrWhiteSpace(container)) return false;
        
        var noBugs = true;
        var files = Directory.EnumerateFiles(container, "*.nif*", SearchOption.AllDirectories).ToList();
        var total = files.Count / 100.0f;

        for (var i = 0; i < files.Count; i++) {
            var file = files[i];
            Progress?.Invoke(file, i / total);
            
            //Load nif file
            if (!File.Exists(file)) continue;

            var nif = new NifFile();
            nif.Load(file);
        
            if (!nif.IsValid()) continue;
        
            var bugs = Bugs.Where(b => b.Enabled && !b.Verify(null, new ExtendedNifFile(nif, file, Path.GetFileName(file))));
            if (bugs.Any()) noBugs = false;
        }

        return noBugs;
    }
    
    public string GetErrorOutput() {
        var output = new StringBuilder();
        foreach (var bug in Bugs) {
            foreach (var asset in bug.BuggedItems) output.AppendLine(asset.Path);
        }
        return output.ToString();
    }
    
    public void Patch(string container, INifBug bug) {
        if (!bug.Enabled || !bug.HasPatch) return;

        var i = 0;
        var total = bug.BuggedItems.Count / 100.0f;
        foreach (var file in bug.BuggedItems) {
            Progress?.Invoke(file.Path, i++ / total);
            Patch(container, bug, file);
        }
    }
    
    public void Patch(string container, INifBug bug, BuggedNif item) {
        if (!bug.Enabled || !bug.HasPatch) return;
        
        //Load nif file
        if (!File.Exists(item.FullPath)) return;

        var nif = new NifFile();
        nif.Load(item.FullPath);
        
        if (!nif.IsValid()) return;
        
        //Patch bugs and save nif
        bug.Patch(null, new ExtendedNifFile(nif, item.FullPath, Path.GetFileName(item.FullPath)));

        nif.Save(item.FullPath);
    }
}

