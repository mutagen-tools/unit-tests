﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using nifly;
namespace SkyrimUnitTests.TestSuite.Tester.Asset.Nif;

public interface INifBug : IBug<ExtendedNifFile, ExtendedNifFile, ExtendedNiObject, ExtendedNiObject, BuggedNif> {}

public sealed class NifFileBug : INifBug {
    public bool Enabled { get; set; }
    public bool DefaultEnabled { get; set; }
    public string Name { get; }
    public string Description { get; }
    public BugSeverity Severity { get; }
    public SolidColorBrush Color { get; set; }
    public HashSet<BuggedNif> BuggedItems { get; set; } = new();
    public bool HasPatch => _patch != null;

    private readonly Func<ExtendedNifFile, List<string>, bool>? _test;
    private readonly Action<ExtendedNifFile>? _patch;
    
    public NifFileBug(string name, string description, BugSeverity severity, bool defaultEnabled = true,
        Func<ExtendedNifFile, List<string>, bool>? test = null, Action<ExtendedNifFile>? patch = null) {
        Name = name;
        Description = description;
        Severity = severity;
        Enabled = DefaultEnabled = defaultEnabled;
        _test = test;
        _patch = patch;
        Color = IBug.SeverityColors[Severity];
    }

    public bool Verify(ExtendedNiObject item, ExtendedNifFile? container = default) => Verify(container);
    public bool Verify(ExtendedNifFile? container = default) {
        if (container == null) return false;

        var asset = new BuggedNif(container.Path);
        if (_test == null || BuggedItems.Contains(asset)) return false;

        var bugList = new List<string>();
        if (_test(container, bugList)) return true;

        asset.Bugs.AddRange(bugList);
        bugList.Clear();
        BuggedItems.Add(asset);

        return false;
    }

    public void Patch(ExtendedNiObject item, ExtendedNifFile? container = default) => Patch(container);
    public void Patch(ExtendedNifFile? container = default) {
        if (container is null || _patch is null) return;

        _patch(container);

        BuggedItems.Remove(new BuggedNif(container.Path));
    }
}

public sealed class NifBlockBug<TBlockType> : INifBug where TBlockType : NiObject {
    public bool Enabled { get; set; }
    public bool DefaultEnabled { get; set; }
    public string Name { get; }
    public string Description { get; }
    public BugSeverity Severity { get; }
    public SolidColorBrush Color { get; set; }
    public HashSet<BuggedNif> BuggedItems { get; set; } = new();
    public bool HasPatch => _patch != null;
    
    private readonly Func<TBlockType, ExtendedNifFile, List<string>, bool>? _test;
    private readonly Action<ExtendedBlock<TBlockType>, ExtendedNifFile>? _patch;
    
    public NifBlockBug(string name, string description, BugSeverity severity, bool defaultEnabled = true,
        Func<TBlockType, ExtendedNifFile, List<string>, bool>? test = null, Action<ExtendedBlock<TBlockType>, ExtendedNifFile>? patch = null) {
        Name = name;
        Description = description;
        Severity = severity;
        Enabled = DefaultEnabled = defaultEnabled;
        _test = test;
        _patch = patch;
        Color = IBug.SeverityColors[Severity];
    }
    
    public bool Verify(ExtendedNiObject item, ExtendedNifFile? container) {
        if (container == null) return false;
        
        var asset = new BuggedNif(container.Path, item.NiObject.GetBlockName(), item.BlockID);
        if (_test == null || BuggedItems.Contains(asset)) return false;
        
        var bugList = new List<string>();
        if (item.NiObject is TBlockType tGetter) {
            if (_test(tGetter, container, bugList)) return true;
            
            asset.Bugs.AddRange(bugList);
            bugList.Clear();
            BuggedItems.Add(asset);
        }
        
        return false;
    }
    
    public void Patch(ExtendedNiObject item, ExtendedNifFile? container = default) {
        if (container == null) return;
        
        if (_patch == null || item.NiObject is not TBlockType tSetter) return;

        _patch(new ExtendedBlock<TBlockType>(tSetter, item.BlockID), container);

        BuggedItems.Remove(new BuggedNif(container.Path, tSetter.GetBlockName(), item.BlockID));
    }
}