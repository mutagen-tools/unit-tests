﻿using System;
namespace SkyrimUnitTests.TestSuite.Tester.Asset.Nif;

// Copied from https://github.com/niftools/nifskope
public enum ShaderType : uint {
	ST_Default,
	ST_EnvironmentMap,
	ST_GlowShader,
	ST_Heightmap,
	ST_FaceTint,
	ST_SkinTint,
	ST_HairTint,
	ST_ParallaxOccMaterial,
	ST_WorldMultitexture,
	ST_WorldMap1,
	ST_Unknown10,
	ST_MultiLayerParallax,
	ST_Unknown12,
	ST_WorldMap2,
	ST_SparkleSnow,
	ST_WorldMap3,
	ST_EyeEnvmap,
	ST_Unknown17,
	ST_WorldMap4,
	ST_WorldLODMultitexture
};

// Copied from https://github.com/ousnius/nifly

// Animations
[Flags]
public enum InterpBlendFlags : byte { INTERP_BLEND_NONE = 0x00, INTERP_BLEND_MANAGER_CONTROLLED = 0x01 };
[Flags]
public enum PathFlags : ushort {
	PATH_NONE = 0x0000,
	PATH_CVDATANEEDSUPDATE = 0x0001,
	PATH_CURVETYPEOPEN = 0x0002,
	PATH_ALLOWFLIP = 0x0004,
	PATH_BANK = 0x0008,
	PATH_CONSTANTVELOCITY = 0x0016,
	PATH_FOLLOW = 0x0032,
	PATH_FLIP = 0x0064
};
[Flags]
public enum LookAtFlags : ushort {
	LOOK_X_AXIS = 0x0000,
	LOOK_FLIP = 0x0001,
	LOOK_Y_AXIS = 0x0002,
	LOOK_Z_AXIS = 0x0004
};
[Flags]
public enum GeomMorpherFlags : ushort { GM_UPDATE_NORMALS_DISABLED, GM_UPDATE_NORMALS_ENABLED };
public enum TargetColor : ushort { TC_AMBIENT, TC_DIFFUSE, TC_SPECULAR, TC_SELF_ILLUM };
public enum TexType : uint {
	BASE_MAP,
	DARK_MAP,
	DETAIL_MAP,
	GLOSS_MAP,
	GLOW_MAP,
	BUMP_MAP,
	NORMAL_MAP,
	PARALLAX_MAP,
	DECAL_0_MAP,
	DECAL_1_MAP,
	DECAL_2_MAP,
	DECAL_3_MAP
};
public enum TexTransformType : uint { TT_TRANSLATE_U, TT_TRANSLATE_V, TT_ROTATE, TT_SCALE_U, TT_SCALE_V };
public enum CycleType : uint { CYCLE_LOOP, CYCLE_REVERSE, CYCLE_CLAMP };
public enum AnimNoteType : uint { ANT_INVALID, ANT_GRABIK, ANT_LOOKIK };
public enum BSShaderType {
	SHADER_TALL_GRASS,
	SHADER_DEFAULT,
	SHADER_SKY = 10,
	SHADER_SKIN = 14,
	SHADER_WATER = 17,
	SHADER_LIGHTING30 = 29,
	SHADER_TILE = 32,
	SHADER_NOLIGHTING
};
public enum NiKeyType : uint { NO_INTERP, LINEAR_KEY, QUADRATIC_KEY, TBC_KEY, XYZ_ROTATION_KEY, CONST_KEY };
public enum ConsistencyType : ushort { CT_MUTABLE = 0x0000, CT_STATIC = 0x4000, CT_VOLATILE = 0x8000 };

// Extra Data
[Flags]
public enum BSXFlagsEnum : uint {
	BSX_ANIMATED = 1 << 0,
	BSX_HAVOK = 1 << 1,
	BSX_RAGDOLL = 1 << 2,
	BSX_COMPLEX = 1 << 3,
	BSX_ADDON = 1 << 4,
	BSX_EDITOR_MARKER = 1 << 5,
	BSX_DYNAMIC = 1 << 6,
	BSX_ARTICULATED = 1 << 7,
	BSX_NEEDS_TRANSFORM_UPDATES = 1 << 8,
	BSX_EXTERNAL_EMITTANCE = 1 << 9
};

// Nodes
[Flags]
public enum BSValueNodeFlags : byte {
	BSVN_NONE = 0x0,
	BSVN_BILLBOARD_WORLD_Z = 0x1,
	BSVN_USE_PLAYER_ADJUST = 0x2
};
public enum BSCPCullingType : uint {
	BSCP_CULL_NORMAL,
	BSCP_CULL_ALLPASS,
	BSCP_CULL_ALLFAIL,
	BSCP_CULL_IGNOREMULTIBOUNDS,
	BSCP_CULL_FORCEMULTIBOUNDSNOUPDATE
};
public enum BillboardMode : ushort {
	ALWAYS_FACE_CAMERA,
	ROTATE_ABOUT_UP,
	RIGID_FACE_CAMERA,
	ALWAYS_FACE_CENTER,
	RIGID_FACE_CENTER,
	BSROTATE_ABOUT_UP,
	ROTATE_ABOUT_UP2 = 9
};
[Flags]
public enum NiSwitchFlags : ushort { UPDATE_ONLY_ACTIVE_CHILD, UPDATE_CONTROLLERS };
public enum SortingMode { SORTING_INHERIT, SORTING_OFF };

// Objects
public enum PixelFormat : uint {
	PX_FMT_RGB8,
	PX_FMT_RGBA8,
	PX_FMT_PAL8,
	PX_FMT_DXT1 = 4,
	PX_FMT_DXT5 = 5,
	PX_FMT_DXT5_ALT = 6,
};
public enum PixelTiling : uint { PX_TILE_NONE, PX_TILE_XENON, PX_TILE_WII, PX_TILE_NV_SWIZZLED };
public enum PixelComponent : uint {
	PX_COMP_RED,
	PX_COMP_GREEN,
	PX_COMP_BLUE,
	PX_COMP_ALPHA,
	PX_COMP_COMPRESSED,
	PX_COMP_OFFSET_U,
	PX_COMP_OFFSET_V,
	PX_COMP_OFFSET_W,
	PX_COMP_OFFSET_Q,
	PX_COMP_LUMA,
	PX_COMP_HEIGHT,
	PX_COMP_VECTOR_X,
	PX_COMP_VECTOR_Y,
	PX_COMP_VECTOR_Z,
	PX_COMP_PADDING,
	PX_COMP_INTENSITY,
	PX_COMP_INDEX,
	PX_COMP_DEPTH,
	PX_COMP_STENCIL,
	PX_COMP_EMPTY
};
public enum PixelRepresentation : uint {
	PX_REP_NORM_INT,
	PX_REP_HALF,
	PX_REP_FLOAT,
	PX_REP_INDEX,
	PX_REP_COMPRESSED,
	PX_REP_UNKNOWN,
	PX_REP_INT
};
public enum PlatformID : uint { PLAT_ANY, PLAT_XENON, PLAT_PS3, PLAT_DX9, PLAT_WII, PLAT_D3D10 };
public enum PixelLayout : uint {
	PX_LAY_PALETTIZED_8,
	PX_LAY_HIGH_COLOR_16,
	PX_LAY_TRUE_COLOR_32,
	PX_LAY_COMPRESSED,
	PX_LAY_BUMPMAP,
	PX_LAY_PALETTIZED_4,
	PX_LAY_DEFAULT,
	PX_LAY_SINGLE_COLOR_8,
	PX_LAY_SINGLE_COLOR_16,
	PX_LAY_SINGLE_COLOR_32,
	PX_LAY_DOUBLE_COLOR_32,
	PX_LAY_DOUBLE_COLOR_64,
	PX_LAY_FLOAT_COLOR_32,
	PX_LAY_FLOAT_COLOR_64,
	PX_LAY_FLOAT_COLOR_128,
	PX_LAY_SINGLE_COLOR_4,
	PX_LAY_DEPTH_24_X8,
};
public enum MipMapFormat : uint { MIP_FMT_NO, MIP_FMT_YES, MIP_FMT_DEFAULT };
public enum AlphaFormat : uint { ALPHA_NONE, ALPHA_BINARY, ALPHA_SMOOTH, ALPHA_DEFAULT };
public enum TexFilterMode : uint {
	FILTER_NEAREST,
	FILTER_BILERP,
	FILTER_TRILERP,
	FILTER_NEAREST_MIPNEAREST,
	FILTER_NEAREST_MIPLERP,
	FILTER_BILERP_MIPNEAREST
};
public enum TexClampMode : uint { CLAMP_S_CLAMP_T, CLAMP_S_WRAP_T, WRAP_S_CLAMP_T, WRAP_S_WRAP_T };
public enum EffectType : uint {
	EFFECT_PROJECTED_LIGHT,
	EFFECT_PROJECTED_SHADOW,
	EFFECT_ENVIRONMENT_MAP,
	EFFECT_FOG_MAP
};
public enum CoordGenType : uint {
	CG_WORLD_PARALLEL,
	CG_WORLD_PERSPECTIVE,
	CG_SPHERE_MAP,
	CG_SPECULAR_CUBE_MAP,
	CG_DIFFUSE_CUBE_MAP
};

// Particles
public enum ForceType : uint { FORCE_PLANAR, FORCE_SPHERICAL, FORCE_UNKNOWN };
public enum DecayType : uint { DECAY_NONE, DECAY_LINEAR, DECAY_EXPONENTIAL };
public enum SymmetryType : uint { SYMMETRY_SPHERICAL, SYMMETRY_CYLINDRICAL, SYMMETRY_PLANAR };
public enum VelocityType : uint { VELOCITY_USE_NORMALS, VELOCITY_USE_RANDOM, VELOCITY_USE_DIRECTION };
public enum EmitFrom : uint {
	EMIT_FROM_VERTICES,
	EMIT_FROM_FACE_CENTER,
	EMIT_FROM_EDGE_CENTER,
	EMIT_FROM_FACE_SURFACE,
	EMIT_FROM_EDGE_SURFACE
};

// Shaders
public enum BSLightingShaderPropertyShaderType : uint {
	BSLSP_DEFAULT,
	BSLSP_ENVMAP,
	BSLSP_GLOWMAP,
	BSLSP_PARALLAX,
	BSLSP_FACE,
	BSLSP_SKINTINT,
	BSLSP_HAIRTINT,
	BSLSP_PARALLAXOCC,
	BSLSP_MULTITEXTURELANDSCAPE,
	BSLSP_LODLANDSCAPE,
	BSLSP_SNOW,
	BSLSP_MULTILAYERPARALLAX,
	BSLSP_TREEANIM,
	BSLSP_LODOBJECTS,
	BSLSP_MULTIINDEXSNOW,
	BSLSP_LODOBJECTSHD,
	BSLSP_EYE,
	BSLSP_CLOUD,
	BSLSP_LODLANDSCAPENOISE,
	BSLSP_MULTITEXTURELANDSCAPELODBLEND,
	BSLSP_DISMEMBERMENT,
	BSLSP_LAST = BSLSP_DISMEMBERMENT
};
[Flags]
public enum SkyrimShaderPropertyFlags1 : uint {
	SLSF1_SPECULAR = 1 << 0, // Enables specularity
	SLSF1_SKINNED = 1 << 1, // Required for skinned meshes
	SLSF1_TEMP_REFRACTION = 1 << 2,
	SLSF1_VERTEX_ALPHA = 1 << 3, // Enables using alpha component of vertex colors
	SLSF1_GREYSCALETOPALETTE_COLOR = 1 << 4, // For effect shader property
	SLSF1_GREYSCALETOPALETTE_ALPHA = 1 << 5, // For effect shader property
	SLSF1_USE_FALLOFF = 1 << 6, // Use falloff value in effect shader property
	SLSF1_ENVIRONMENT_MAPPING = 1 << 7, // Enables environment mapping (uses environment map scale)
	SLSF1_RECEIVE_SHADOWS = 1 << 8, // Can receive shadows
	SLSF1_CAST_SHADOWS = 1 << 9, // Can cast shadows
	SLSF1_FACEGEN_DETAIL_MAP = 1 << 10, // Use a face detail map in the fourth texture slot
	SLSF1_PARALLAX = 1 << 11,
	SLSF1_MODEL_SPACE_NORMALS = 1 << 12, // Use model space normals and an external specular map
	SLSF1_NON_PROJECTIVE_SHADOWS = 1 << 13,
	SLSF1_LANDSCAPE = 1 << 14,
	SLSF1_REFRACTION = 1 << 15, // Use normal map for refraction effect
	SLSF1_FIRE_REFRACTION = 1 << 16,
	SLSF1_EYE_ENVIRONMENT_MAPPING = 1 << 17, // Enables eye environment mapping (must use the eye shader and the model must be skinned)
	SLSF1_HAIR_SOFT_LIGHTING = 1 << 18, // Keeps from going too bright under lights (hair shader only)
	SLSF1_SCREENDOOR_ALPHA_FADE = 1 << 19,
	SLSF1_LOCALMAP_HIDE_SECRET = 1 << 20, // Object and anything it is positioned above will not render on local map view
	SLSF1_FACEGEN_RGB_TINT = 1 << 21, // Use tint mask for face
	SLSF1_OWN_EMIT = 1 << 22, // Provides its own emittance color (will not absorb light/ambient color?)
	SLSF1_PROJECTED_UV = 1 << 23, // Used for decalling?
	SLSF1_MULTIPLE_TEXTURES = 1 << 24,
	SLSF1_REMAPPABLE_TEXTURES = 1 << 25,
	SLSF1_DECAL = 1 << 26,
	SLSF1_DYNAMIC_DECAL = 1 << 27,
	SLSF1_PARALLAX_OCCLUSION = 1 << 28,
	SLSF1_EXTERNAL_EMITTANCE = 1 << 29,
	SLSF1_SOFT_EFFECT = 1 << 30,
	SLSF1_ZBUFFER_TEST = 1u << 31 // Enables Z-Buffer testing
};
[Flags]
public enum SkyrimShaderPropertyFlags2 : uint {
	SLSF2_ZBUFFER_WRITE = 1 << 0, // Enables writing to the Z-Buffer
	SLSF2_LOD_LANDSCAPE = 1 << 1,
	SLSF2_LOD_OBJECTS = 1 << 2,
	SLSF2_NO_FADE = 1 << 3,
	SLSF2_DOUBLE_SIDED = 1 << 4, // Enables double-sided rendering
	SLSF2_VERTEX_COLORS = 1 << 5, // Enables vertex color rendering
	SLSF2_GLOW_MAP = 1 << 6, // Use glow map in the third texture slot
	SLSF2_ASSUME_SHADOWMASK = 1 << 7,
	SLSF2_PACKED_TANGENT = 1 << 8,
	SLSF2_MULTI_INDEX_SNOW = 1 << 9,
	SLSF2_VERTEX_LIGHTING = 1 << 10,
	SLSF2_UNIFORM_SCALE = 1 << 11,
	SLSF2_FIT_SLOPE = 1 << 12,
	SLSF2_BILLBOARD = 1 << 13,
	SLSF2_NO_LOD_LAND_BLEND = 1 << 14,
	SLSF2_ENVMAP_LIGHT_FADE = 1 << 15,
	SLSF2_WIREFRAME = 1 << 16,
	SLSF2_WEAPON_BLODD = 1 << 17, // Used for blood decals on weapons
	SLSF2_HIDE_ON_LOCAL_MAP = 1 << 18, // Similar to hide secret, but only for self?
	SLSF2_PREMULT_ALPHA = 1 << 19, // Has premultiplied alpha
	SLSF2_CLOUD_LOD = 1 << 20,
	SLSF2_ANISOTROPIC_LIGHTING = 1 << 21, // Hair only?
	SLSF2_NO_TRANSPARENCY_MULTISAMPLING = 1 << 22,
	SLSF2_UNUSED01 = 1 << 23,
	SLSF2_MULTI_LAYER_PARALLAX = 1 << 24, // Use multilayer (inner-layer) map
	SLSF2_SOFT_LIGHTING = 1 << 25, // Use soft lighting map
	SLSF2_RIM_LIGHTING = 1 << 26, // Use rim lighting map
	SLSF2_BACK_LIGHTING = 1 << 27, // Use back lighting map
	SLSF2_UNUSED02 = 1 << 28,
	SLSF2_TREE_ANIM = 1 << 29, // Enables vertex animation, flutter animation
	SLSF2_EFFECT_LIGHTING = 1 << 30,
	SLSF2_HD_LOD_OBJECTS = 1u << 31
};
[Flags]
public enum Fallout4ShaderPropertyFlags1 : uint {
	F4SF1_SPECULAR = 1 << 0, // Enables specularity
	F4SF1_SKINNED = 1 << 1, // Required for skinned meshes
	F4SF1_TEMP_REFRACTION = 1 << 2,
	F4SF1_VERTEX_ALPHA = 1 << 3, // Enables using alpha component of vertex colors
	F4SF1_GREYSCALETOPALETTE_COLOR = 1 << 4, // For effect shader property
	F4SF1_GREYSCALETOPALETTE_ALPHA = 1 << 5, // For effect shader property
	F4SF1_USE_FALLOFF = 1 << 6, // Use falloff value in effect shader property
	F4SF1_ENVIRONMENT_MAPPING = 1 << 7, // Enables environment mapping (uses environment map scale)
	F4SF1_RGB_FALLOFF = 1 << 8,
	F4SF1_CAST_SHADOWS = 1 << 9, // Can cast shadows
	F4SF1_FACE = 1 << 10,
	F4SF1_UI_MASK_RECTS = 1 << 11,
	F4SF1_MODEL_SPACE_NORMALS = 1 << 12,
	F4SF1_NON_PROJECTIVE_SHADOWS = 1 << 13,
	F4SF1_LANDSCAPE = 1 << 14,
	F4SF1_REFRACTION = 1 << 15,
	F4SF1_FIRE_REFRACTION = 1 << 16,
	F4SF1_EYE_ENVIRONMENT_MAPPING = 1 << 17,
	F4SF1_HAIR = 1 << 18,
	F4SF1_SCREENDOOR_ALPHA_FADE = 1 << 19,
	F4SF1_LOCALMAP_HIDE_SECRET = 1 << 20,
	F4SF1_SKIN_TINT = 1 << 21,
	F4SF1_OWN_EMIT = 1 << 22,
	F4SF1_PROJECTED_UV = 1 << 23, // Used for decalling?
	F4SF1_MULTIPLE_TEXTURES = 1 << 24,
	F4SF1_TESSELLATE = 1 << 25,
	F4SF1_DECAL = 1 << 26,
	F4SF1_DYNAMIC_DECAL = 1 << 27,
	F4SF1_CHARACTER_LIGHTING = 1 << 28,
	F4SF1_EXTERNAL_EMITTANCE = 1 << 29,
	F4SF1_SOFT_EFFECT = 1 << 30,
	F4SF1_ZBUFFER_TEST = 1u << 31 // Enables Z-Buffer testing
};
[Flags]
public enum Fallout4ShaderPropertyFlags2 : uint {
	F4SF2_ZBUFFER_WRITE = 1 << 0, // Enables writing to the Z-Buffer
	F4SF2_LOD_LANDSCAPE = 1 << 1,
	F4SF2_LOD_OBJECTS = 1 << 2,
	F4SF2_NO_FADE = 1 << 3,
	F4SF2_DOUBLE_SIDED = 1 << 4, // Enables double-sided rendering
	F4SF2_VERTEX_COLORS = 1 << 5, // Enables vertex color rendering
	F4SF2_GLOW_MAP = 1 << 6,
	F4SF2_TRANSFORM_CHANGED = 1 << 7,
	F4SF2_DISMEMBERMENT_MEATCUFF = 1 << 8,
	F4SF2_TINT = 1 << 9,
	F4SF2_GRASS_VERTEX_LIGHTING = 1 << 10,
	F4SF2_GRASS_UNIFORM_SCALE = 1 << 11,
	F4SF2_GRASS_FIT_SLOPE = 1 << 12,
	F4SF2_GRASS_BILLBOARD = 1 << 13,
	F4SF2_NO_LOD_LAND_BLEND = 1 << 14,
	F4SF2_DISMEMBERMENT = 1 << 15,
	F4SF2_WIREFRAME = 1 << 16,
	F4SF2_WEAPON_BLODD = 1 << 17,
	F4SF2_HIDE_ON_LOCAL_MAP = 1 << 18,
	F4SF2_PREMULT_ALPHA = 1 << 19,
	F4SF2_VATS_TARGET = 1 << 20,
	F4SF2_ANISOTROPIC_LIGHTING = 1 << 21,
	F4SF2_SKEW_SPECULAR_ALPHA = 1 << 22,
	F4SF2_MENU_SCREEN = 1 << 23,
	F4SF2_MULTI_LAYER_PARALLAX = 1 << 24,
	F4SF2_ALPHA_TEST = 1 << 25,
	F4SF2_GRADIENT_REMAP = 1 << 26,
	F4SF2_VATS_TARGET_DRAW_ALL = 1 << 27,
	F4SF2_PIPBOY_SCREEN = 1 << 28,
	F4SF2_TREE_ANIM = 1 << 29,
	F4SF2_EFFECT_LIGHTING = 1 << 30,
	F4SF2_REFRACTION_WRITES_DEPTH = 1u << 31
};
public enum SkyObjectType : uint {
	BSSM_SKY_TEXTURE,
	BSSM_SKY_SUNGLARE,
	BSSM_SKY,
	BSSM_SKY_CLOUDS,
	BSSM_SKY_STARS = 5,
	BSSM_SKY_MOON_STARS_MASK = 7
};
public enum StencilMasks {
	ENABLE_MASK = 0x0001,
	FAIL_MASK = 0x000E,
	FAIL_POS = 1,
	ZFAIL_MASK = 0x0070,
	ZFAIL_POS = 4,
	ZPASS_MASK = 0x0380,
	ZPASS_POS = 7,
	DRAW_MASK = 0x0C00,
	DRAW_POS = 10,
	TEST_MASK = 0x7000,
	TEST_POS = 12
};
public enum DrawMode { DRAW_CCW_OR_BOTH, DRAW_CCW, DRAW_CW, DRAW_BOTH, DRAW_MAX };

// Skin
[Flags]
public enum PartitionFlags : ushort { PF_NONE = 0, PF_EDITOR_VISIBLE = 1 << 0, PF_START_NET_BONESET = 1 << 8 };

// Copied from https://raw.githubusercontent.com/libcs/game-estates/master/src/Formats/Nif/src/Game.Format.Nif2/Gen/Enums.cs
public enum SkyrimHavokMaterial : uint {
	SKY_HAV_MAT_BROKEN_STONE = 131151687, /*!< Broken Stone */
	SKY_HAV_MAT_LIGHT_WOOD = 365420259, /*!< Light Wood */
	SKY_HAV_MAT_SNOW = 398949039, /*!< Snow */
	SKY_HAV_MAT_GRAVEL = 428587608, /*!< Gravel */
	SKY_HAV_MAT_MATERIAL_CHAIN_METAL = 438912228, /*!< Material Chain Metal */
	SKY_HAV_MAT_BOTTLE = 493553910, /*!< Bottle */
	SKY_HAV_MAT_WOOD = 500811281, /*!< Wood */
	SKY_HAV_MAT_SKIN = 591247106, /*!< Skin */
	SKY_HAV_MAT_UNKNOWN_617099282 = 617099282, /*!< Unknown in Creation Kit v1.9.32.0. Found in Dawnguard DLC in meshes\dlc01\clutter\dlc01deerskin.nif. */
	SKY_HAV_MAT_BARREL = 732141076, /*!< Barrel */
	SKY_HAV_MAT_MATERIAL_CERAMIC_MEDIUM = 781661019, /*!< Material Ceramic Medium */
	SKY_HAV_MAT_MATERIAL_BASKET = 790784366, /*!< Material Basket */
	SKY_HAV_MAT_ICE = 873356572, /*!< Ice */
	SKY_HAV_MAT_STAIRS_STONE = 899511101, /*!< Stairs Stone */
	SKY_HAV_MAT_WATER = 1024582599, /*!< Water */
	SKY_HAV_MAT_UNKNOWN_1028101969 = 1028101969, /*!< Unknown in Creation Kit v1.6.89.0. Found in actors\draugr\character assets\skeletons.nif. */
	SKY_HAV_MAT_MATERIAL_BLADE_1HAND = 1060167844, /*!< Material Blade 1 Hand */
	SKY_HAV_MAT_MATERIAL_BOOK = 1264672850, /*!< Material Book */
	SKY_HAV_MAT_MATERIAL_CARPET = 1286705471, /*!< Material Carpet */
	SKY_HAV_MAT_SOLID_METAL = 1288358971, /*!< Solid Metal */
	SKY_HAV_MAT_MATERIAL_AXE_1HAND = 1305674443, /*!< Material Axe 1Hand */
	SKY_HAV_MAT_UNKNOWN_1440721808 = 1440721808, /*!< Unknown in Creation Kit v1.6.89.0. Found in armor\draugr\draugrbootsfemale_go.nif or armor\amuletsandrings\amuletgnd.nif. */
	SKY_HAV_MAT_STAIRS_WOOD = 1461712277, /*!< Stairs Wood */
	SKY_HAV_MAT_MUD = 1486385281, /*!< Mud */
	SKY_HAV_MAT_MATERIAL_BOULDER_SMALL = 1550912982, /*!< Material Boulder Small */
	SKY_HAV_MAT_STAIRS_SNOW = 1560365355, /*!< Stairs Snow */
	SKY_HAV_MAT_HEAVY_STONE = 1570821952, /*!< Heavy Stone */
	SKY_HAV_MAT_UNKNOWN_1574477864 = 1574477864, /*!< Unknown in Creation Kit v1.6.89.0. Found in actors\dragon\character assets\skeleton.nif. */
	SKY_HAV_MAT_UNKNOWN_1591009235 = 1591009235, /*!< Unknown in Creation Kit v1.6.89.0. Found in trap objects or clutter\displaycases\displaycaselgangled01.nif or actors\deer\character assets\skeleton.nif. */
	SKY_HAV_MAT_MATERIAL_BOWS_STAVES = 1607128641, /*!< Material Bows Staves */
	SKY_HAV_MAT_MATERIAL_WOOD_AS_STAIRS = 1803571212, /*!< Material Wood As Stairs */
	SKY_HAV_MAT_GRASS = 1848600814, /*!< Grass */
	SKY_HAV_MAT_MATERIAL_BOULDER_LARGE = 1885326971, /*!< Material Boulder Large */
	SKY_HAV_MAT_MATERIAL_STONE_AS_STAIRS = 1886078335, /*!< Material Stone As Stairs */
	SKY_HAV_MAT_MATERIAL_BLADE_2HAND = 2022742644, /*!< Material Blade 2Hand */
	SKY_HAV_MAT_MATERIAL_BOTTLE_SMALL = 2025794648, /*!< Material Bottle Small */
	SKY_HAV_MAT_SAND = 2168343821, /*!< Sand */
	SKY_HAV_MAT_HEAVY_METAL = 2229413539, /*!< Heavy Metal */
	SKY_HAV_MAT_UNKNOWN_2290050264 = 2290050264, /*!< Unknown in Creation Kit v1.9.32.0. Found in Dawnguard DLC in meshes\dlc01\clutter\dlc01sabrecatpelt.nif. */
	SKY_HAV_MAT_DRAGON = 2518321175, /*!< Dragon */
	SKY_HAV_MAT_MATERIAL_BLADE_1HAND_SMALL = 2617944780, /*!< Material Blade 1Hand Small */
	SKY_HAV_MAT_MATERIAL_SKIN_SMALL = 2632367422, /*!< Material Skin Small */
	SKY_HAV_MAT_STAIRS_BROKEN_STONE = 2892392795, /*!< Stairs Broken Stone */
	SKY_HAV_MAT_MATERIAL_SKIN_LARGE = 2965929619, /*!< Material Skin Large */
	SKY_HAV_MAT_ORGANIC = 2974920155, /*!< Organic */
	SKY_HAV_MAT_MATERIAL_BONE = 3049421844, /*!< Material Bone */
	SKY_HAV_MAT_HEAVY_WOOD = 3070783559, /*!< Heavy Wood */
	SKY_HAV_MAT_MATERIAL_CHAIN = 3074114406, /*!< Material Chain */
	SKY_HAV_MAT_DIRT = 3106094762, /*!< Dirt */
	SKY_HAV_MAT_MATERIAL_ARMOR_LIGHT = 3424720541, /*!< Material Armor Light */
	SKY_HAV_MAT_MATERIAL_SHIELD_LIGHT = 3448167928, /*!< Material Shield Light */
	SKY_HAV_MAT_MATERIAL_COIN = 3589100606, /*!< Material Coin */
	SKY_HAV_MAT_MATERIAL_SHIELD_HEAVY = 3702389584, /*!< Material Shield Heavy */
	SKY_HAV_MAT_MATERIAL_ARMOR_HEAVY = 3708432437, /*!< Material Armor Heavy */
	SKY_HAV_MAT_MATERIAL_ARROW = 3725505938, /*!< Material Arrow */
	SKY_HAV_MAT_GLASS = 3739830338, /*!< Glass */
	SKY_HAV_MAT_STONE = 3741512247, /*!< Stone */
	SKY_HAV_MAT_CLOTH = 3839073443, /*!< Cloth */
	SKY_HAV_MAT_MATERIAL_BLUNT_2HAND = 3969592277, /*!< Material Blunt 2Hand */
	SKY_HAV_MAT_UNKNOWN_4239621792 = 4239621792, /*!< Unknown in Creation Kit v1.9.32.0. Found in Dawnguard DLC in meshes\dlc01\prototype\dlc1protoswingingbridge.nif. */
	SKY_HAV_MAT_MATERIAL_BOULDER_MEDIUM = 4283869410, /*!< Material Boulder Medium */
}
