﻿using System.Collections.Generic;
using nifly;
namespace SkyrimUnitTests.TestSuite.Tester.Asset.Nif;

public class ConvexListShapeBlockTest : NifBlockTest<bhkConvexListShape> {
    public override string Name => "ConvexListShape";
    
    public override List<INifBug> Bugs { get; } =[
        new NifBlockBug<bhkConvexListShape>("Invalid Material", "Only a specific subset of numbers are numbers are valid materials",
            BugSeverity.Warning, true, TestMaterial)
    ];
    
    private static bool TestMaterial(bhkConvexListShape block, ExtendedNifFile nif, List<string> list) {
        if (!NifHelper.InvalidMaterial(block.material)) return true;
        list.Add($"{block.material} is an invalid material");

        return false;
    }
}