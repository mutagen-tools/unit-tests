﻿using System.Collections.Generic;
using System.Linq;
using nifly;
namespace SkyrimUnitTests.TestSuite.Tester.Asset.Nif;


public class ControllerManagerTest : NifBlockTest<NiControllerManager> {
	public override string Name => "Controller Manager";

	public override List<INifBug> Bugs { get; } = [
		new NifBlockBug<NiControllerManager>("No Animated Flag", "No animated flag set on BSXFlags",
			BugSeverity.Warning, true, TestAnimatedFlag, PatchAnimatedFlag),
	];

	private static bool TestAnimatedFlag(NiControllerManager block, ExtendedNifFile nif, List<string> list) {
		var bsxFlags = nif.Nif.EnumerateBlocks<BSXFlags>().FirstOrDefault();
		if (bsxFlags is null) {
			list.Add("BSXFlags not found");
			return false;
		}

		return ((BSXFlagsEnum) bsxFlags.integerData & BSXFlagsEnum.BSX_ANIMATED) != 0;
	}

	private static void PatchAnimatedFlag(ExtendedBlock<NiControllerManager> extendedBlock, ExtendedNifFile nif) {
		var bsxFlags = nif.Nif.EnumerateEditableBlocks<BSXFlags>().FirstOrDefault();
		if (bsxFlags is not null) {
			bsxFlags.integerData |= (int) BSXFlagsEnum.BSX_ANIMATED;
		}
	}
}
