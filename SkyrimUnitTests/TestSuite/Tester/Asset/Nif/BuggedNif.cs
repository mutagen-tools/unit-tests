﻿using System;
using SkyrimUnitTests.UI.Asset.Nif;
namespace SkyrimUnitTests.TestSuite.Tester.Asset.Nif;

public sealed class BuggedNif(string path, string blockName = "", uint blockID = uint.MaxValue) : BuggedItem {
	public string FullPath { get; } = path;
	public string Path => System.IO.Path.GetRelativePath(NifUnitTestViewModel.Instance.Directory, FullPath);
	public string BlockName { get; } = blockName;
	public uint BlockID { get; } = blockID;

	public override int GetHashCode() => HashCode.Combine(FullPath, BlockID);
	public override int CompareTo(object? obj) {
		if (obj is not BuggedNif other) return 1;

		if (ReferenceEquals(this, other)) return 0;

		var pathCompare = string.Compare(FullPath, other.FullPath, StringComparison.OrdinalIgnoreCase);
		if (pathCompare == 0) return BlockID.CompareTo(other.BlockID);

		return pathCompare;
	}

	public override string ToString() {
		var str = FullPath;
		if (BlockID != uint.MaxValue || BlockName != "") {
			str += " Block ";
			if (BlockID != uint.MaxValue) str += BlockID;
			if (BlockName != "") str += ' ' + BlockName;
		}

		if (!string.IsNullOrEmpty(BugDescription)) {
			str += Environment.NewLine + BugDescription;
		}

		return str;
	}
}
