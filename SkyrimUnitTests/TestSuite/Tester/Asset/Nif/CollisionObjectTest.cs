﻿using System.Collections.Generic;
using System.Linq;
using nifly;
namespace SkyrimUnitTests.TestSuite.Tester.Asset.Nif;

public class CollisionObjectTest : NifBlockTest<bhkCollisionObject> {
	public override string Name => "Collision Object";

	public override List<INifBug> Bugs { get; } = new() {
		new NifBlockBug<bhkCollisionObject>("Invalid Target", "The target is not pointing to the direct parent which can cause issues",
			BugSeverity.Warning, true, TestTarget, PatchTarget),
	};

	private static bool TestTarget(bhkCollisionObject block, ExtendedNifFile nif, List<string> list) {
		var targetRefIndex = block.targetRef.index;

		// If the collision block is not attached to anything, we can't do anything about it
		if (targetRefIndex == uint.MaxValue) return true;

		// Check if the collision ref entry of the target object is pointing to our collision object
		var targetObject = nif.Nif.GetHeader().GetBlockById(targetRefIndex);
		if (targetObject is not NiAVObject niAvObject) return false;

		var collisionId = nif.Nif.GetBlockID(block);
		return niAvObject.collisionRef.index != collisionId;
	}

	private static void PatchTarget(ExtendedBlock<bhkCollisionObject> extendedBlock, ExtendedNifFile nif) {
		// Search for node with collision ref pointing to our collision object
		var niAvObject = nif.Nif
			.EnumerateBlocks<NiAVObject>()
			.FirstOrDefault(niAvObject => niAvObject.collisionRef.index == extendedBlock.BlockID);
		if (niAvObject is null) return;

		// Set the target ref to the parent
		var parentId = nif.Nif.GetBlockID(niAvObject);
		extendedBlock.Block.targetRef = new NiBlockRefNiAVObject(parentId);
		extendedBlock.Block.targetRef.index = parentId;
	}
}
