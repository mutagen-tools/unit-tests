﻿using System.Collections.Generic;
namespace SkyrimUnitTests.TestSuite.Tester; 

public interface ITest : IProgressReporter, IEnablable {
    /// Name of Test
    public string Name { get; }
}

public interface ITest<out TBug> : ITest {
    /// Bugs that are tested and patched
    public IEnumerable<TBug> Bugs { get; }
}
    
    
public interface ITest<in TContainerSetter, in TContainerGetter, in TBuggedItem, TBug> : ITest<TBug>
    where TBuggedItem : BuggedItem
    where TBug : IBug {
    /// Verify bugs
    public bool Verify(TContainerGetter container, TBug? bug = default);
    
    public string GetErrorOutput();

    /// Generate patch
    public void Patch(TContainerSetter container) {
        foreach (var bug in Bugs) Patch(container, bug);
    }
    
    public void Patch(TContainerSetter container, TBug bug);
    public void Patch(TContainerSetter container, TBug bug, TBuggedItem item);
}