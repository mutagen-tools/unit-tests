﻿using System.Collections.Generic;
using System.Linq;
namespace SkyrimUnitTests.TestSuite.Tester;

public static class InvalidString {
    public static Dictionary<string, string> InvalidStrings { get; } = new() {
        { "’", "'" },
        { "`", "'" },
        { "”", "\"" },
        { "“", "\"" },
        { "…", "..." },
        { "—", "-" },
        { "  ", " " },
    };

    public static Dictionary<string, string> InvalidStringsOneLiner { get; } = InvalidStrings
        .Concat(new Dictionary<string, string> { { "\r", "" }, { "\n", "" } })
        .ToDictionary();
}
