﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mutagen.Bethesda;
using Mutagen.Bethesda.Environments;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Plugins.Cache;
using Mutagen.Bethesda.Plugins.Records;
using Mutagen.Bethesda.Skyrim;
using MutagenLibrary.Assets.Managers;
using MutagenLibrary.ReferenceCache;
using Noggog;
namespace SkyrimUnitTests.TestSuite.Tester.Record;

public interface IRecordTest : ITest<ISkyrimMod, ISkyrimModGetter, BuggedRecord, IRecordBug> {
    public static IEnumerable<IRecordTest> Tests { get; } = typeof(IRecordTest).GetSubclassesOf().NotNull()
        .Select(subclass => (IRecordTest?) System.Activator.CreateInstance(subclass)).NotNull();
    
    public static readonly IGameEnvironment<ISkyrimMod, ISkyrimModGetter> Environment = GameEnvironment.Typical.Skyrim(SkyrimRelease.SkyrimSE);
    public static readonly ILinkCache<ISkyrimMod, ISkyrimModGetter> LinkCache = Environment.LinkCache;
    public static readonly ISkyrimMod PatchMod = new SkyrimMod(new ModKey("UnitTestPatch", ModType.Plugin), SkyrimRelease.SkyrimSE);
    public static readonly ILinkCache PatchLinkCache = PatchMod.ToMutableLinkCache();
    private static readonly PluginManager PluginManagerInternal = new(Environment, PatchMod.ModKey.FileName);
    private static bool _parsedAssets;
    public static PluginManager PluginManager {
        get {
            if (!_parsedAssets) {
                PluginManagerInternal.ParseAssets();
                _parsedAssets = true;
            }
            return PluginManagerInternal;
        }
    }

    private static ReferenceQuery? _referenceQueryInternal;
    public static ReferenceQuery ReferenceQuery => _referenceQueryInternal ??= new ReferenceQuery(LinkCache.ListedOrder);
}

public abstract class RecordTest<TMajorRecord, TMajorRecordGetter, TMajorParentRecordGetter> : IRecordTest
    where TMajorParentRecordGetter : class, IMajorRecordGetter
    where TMajorRecordGetter : class, IMajorRecordGetter
    where TMajorRecord : class, TMajorRecordGetter, IMajorRecord {
    private static readonly Dictionary<FormKey, IModContext<ISkyrimMod, ISkyrimModGetter, TMajorRecord, TMajorRecordGetter>> OverrideCache = new();
    public bool Enabled { get; set; } = true;
    public bool DefaultEnabled { get; set; }

    public virtual string Name => string.Empty;
    public virtual IEnumerable<IRecordBug> Bugs { get; } = new List<RecordBug<TMajorRecord, TMajorRecordGetter>>();
    
    public event IProgressReporter.ProgressReportHandler? Progress;
    
    /// Verifies if one or any bugs occur in a mod
    public bool Verify(ISkyrimModGetter container, IRecordBug? bug) {
        if (!Enabled || bug is { Enabled: false }) return false;
        
        var noBugs = true;
        var records = container.EnumerateMajorRecords<TMajorParentRecordGetter>().ToList();
        var total = records.Count / 100.0f;
        
        for (var i = 0; i < records.Count; i++) {
            var record = records[i];
            Progress?.Invoke(record.FormKey.ToString(), i / total);
            
            foreach (var relevantRecord in GetTestRelevantRecords(record)) {
                if (bug == null) {
                    var bugs = Bugs.Where(b => b.Enabled && !b.Verify(relevantRecord));
                    if (bugs.Any()) noBugs = false;
                } else {
                    if (!bug.Verify(relevantRecord)) noBugs = false;
                }
            }
        }
        
        return noBugs;
    }

    //Allows further details on what records to test
    protected abstract IEnumerable<TMajorRecordGetter> GetTestRelevantRecords(TMajorParentRecordGetter record);

    /// Unsuccessful forms output
    public string GetErrorOutput() {
        var output = new StringBuilder();
        foreach (var bug in Bugs) {
            foreach (var record in bug.BuggedItems) output.AppendLine($"{record.FormKey} {record.EditorID}");
        }
        return output.ToString();
    }
    
    /// Patch unsuccessful forms
    public void Patch(ISkyrimMod container, IRecordBug bug) {
        if (!bug.Enabled || !bug.HasPatch) return;

        var i = 0;
        var total = bug.BuggedItems.Count / 100.0f;
        foreach (var record in bug.BuggedItems) {
            Progress?.Invoke(record.FormKey.ToString(), i++ / total);
            Patch(container, bug, record);
        }
    }
    public void Patch(ISkyrimMod container, IRecordBug bug, BuggedRecord item) {
        if (!bug.Enabled || !bug.HasPatch) return;
        
        //Try to get get context from cache
        IModContext<ISkyrimMod, ISkyrimModGetter, TMajorRecord, TMajorRecordGetter> context;
        if (OverrideCache.TryGetValue(item.FormKey, out var value)) {
            context = value;
        } else {
            context = IRecordTest.LinkCache.ResolveContext<TMajorRecord, TMajorRecordGetter>(item.FormKey);
            OverrideCache.Add(item.FormKey, context);
        }
        
        //Add to patch
        var patchRecord = context.GetOrAddAsOverride(IRecordTest.PatchMod);
        if (context.Record is IDialogTopicGetter dialogTopicGetter) {
            foreach (var response in dialogTopicGetter.Responses) {
                var responseContext = IRecordTest.LinkCache.ResolveContext<IDialogResponses, IDialogResponsesGetter>(response.FormKey);
                responseContext.GetOrAddAsOverride(IRecordTest.PatchMod);
            }
        }
        
        //Patch bugs and keep record if unsuccessful
        bug.Patch(patchRecord, container);
    }
}