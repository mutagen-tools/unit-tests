﻿using System.Collections.Generic;
using System.Linq;
using Mutagen.Bethesda.FormKeys.SkyrimSE;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Skyrim;
namespace SkyrimUnitTests.TestSuite.Tester.Record; 

public class SpellRecordTest : RecordTest<ISpell, ISpellGetter, ISpellGetter> {
    public override string Name => "Spell";
    public override List<RecordBug<ISpell, ISpellGetter>> Bugs { get; } = [
        new RecordBug<ISpell, ISpellGetter>("Effect List Empty", "Spells should have at least one effect",
            BugSeverity.Warning, true, TestEffectList),
        new RecordBug<ISpell, ISpellGetter>("Empty Menu Display", "Spells should have an inventory symbol, MagicHatMarker if not filled",
            BugSeverity.Inconsistency, true, TestEmptyMenuDisplay, PatchEmptyMenuDisplay)
    ];

    protected override IEnumerable<ISpellGetter> GetTestRelevantRecords(ISpellGetter record) {
        yield return record;
    }

    //Effect List Empty
    private static bool TestEffectList(ISpellGetter spell, List<string> list) {
        return spell.Effects.Any();
    }
    
    //Empty Menu Display
    private static bool TestEmptyMenuDisplay(ISpellGetter spell, List<string> list) {
        return !spell.MenuDisplayObject.IsNull;
    }

    private static void PatchEmptyMenuDisplay(ISpell spell, ISkyrimMod patchMod) {
        spell.MenuDisplayObject = new FormLinkNullable<IStaticGetter>(Skyrim.Static.MagicHatMarker.FormKey);
    }
}