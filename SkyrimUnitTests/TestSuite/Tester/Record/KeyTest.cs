﻿using System.Collections.Generic;
using System.Linq;
using Mutagen.Bethesda.FormKeys.SkyrimSE;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Skyrim;
using Noggog;
namespace SkyrimUnitTests.TestSuite.Tester.Record; 

public class KeyRecordTest : RecordTest<IKey, IKeyGetter, IKeyGetter> {
    public override string Name => "Key";
    public override List<RecordBug<IKey, IKeyGetter>> Bugs { get; } = [
        new RecordBug<IKey, IKeyGetter>("No Sounds", "Keys should have pickup/put down sounds",
            BugSeverity.Inconsistency, true, TestSounds, PatchSounds),
        new RecordBug<IKey, IKeyGetter>("No Vendor Keyword", "Key should have VendorItemKey keyword",
            BugSeverity.Inconsistency, true, TestVendorKeyword, PatchVendorKeyword),
        new RecordBug<IKey, IKeyGetter>("Weight/Value not 0", "Weight/value should be 0 for consistency",
            BugSeverity.Inconsistency, true, TestWeightValue, PatchWeightValue)
    ];

    protected override IEnumerable<IKeyGetter> GetTestRelevantRecords(IKeyGetter record) {
        yield return record;
    }

    //No Sounds
    private static bool TestSounds(IKeyGetter key, List<string> list) {
        return !key.PickUpSound.IsNull && !key.PutDownSound.IsNull;
    }
    
    private static void PatchSounds(IKey key, ISkyrimMod patchFile) {
        if (key.PickUpSound.IsNull) key.PickUpSound = new FormLinkNullable<ISoundDescriptorGetter>(Skyrim.SoundDescriptor.ITMKeyUpSD.FormKey);
        if (key.PutDownSound.IsNull) key.PutDownSound = new FormLinkNullable<ISoundDescriptorGetter>(Skyrim.SoundDescriptor.ITMKeyDownSD.FormKey);
    }
    
    //Vendor Keyword
    private static bool TestVendorKeyword(IKeyGetter key, List<string> list) {
        return key.Keywords != null && key.Keywords.Any(k => k.FormKey == Skyrim.Keyword.VendorItemKey.FormKey);
    }
    
    private static void PatchVendorKeyword(IKey key, ISkyrimMod patchFile) {
        key.Keywords ??= new ExtendedList<IFormLinkGetter<IKeywordGetter>>();
        key.Keywords.Add(new FormLink<IKeywordGetter>(Skyrim.Keyword.VendorItemKey.FormKey));
    }
    
    //Weight/Value not 0
    private static bool TestWeightValue(IKeyGetter key, List<string> list) {
        return key is { Weight: 0, Value: 0 };
    }
    
    private static void PatchWeightValue(IKey key, ISkyrimMod patchFile) {
        key.Weight = key.Value = 0;
    }
}