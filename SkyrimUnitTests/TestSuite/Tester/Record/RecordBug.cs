﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using Mutagen.Bethesda.Plugins.Records;
using Mutagen.Bethesda.Skyrim;
namespace SkyrimUnitTests.TestSuite.Tester.Record;

public interface IRecordBug : IBug<ISkyrimMod, ISkyrimModGetter, IMajorRecord, IMajorRecordGetter, BuggedRecord> {
    
}

public class RecordBug<TMajorRecord, TMajorRecordGetter> : IRecordBug {
    public bool Enabled { get; set; }
    public bool DefaultEnabled { get; set; }
    public string Name { get; }
    public string Description { get; }
    public BugSeverity Severity { get; }
    public HashSet<BuggedRecord> BuggedItems { get; set; } = [];
    public SolidColorBrush Color { get; set; }
    public bool HasPatch => _patch != null;
    
    private readonly Func<TMajorRecordGetter, List<string>, bool>? _test;
    private readonly Action<TMajorRecord, ISkyrimMod>? _patch;
        
    public RecordBug(string name, string description, BugSeverity severity, bool defaultEnabled = true,
        Func<TMajorRecordGetter, List<string>, bool>? test = null, Action<TMajorRecord, ISkyrimMod>? patch = null) {
        Name = name;
        Description = description;
        Severity = severity;
        Enabled = DefaultEnabled = defaultEnabled;
        _test = test;
        _patch = patch;
        Color = IBug.SeverityColors[Severity];
    }
    
    public bool Verify(IMajorRecordGetter item, ISkyrimModGetter? container = default) {
        if (item.IsDeleted) return true;

        var id = new BuggedRecord(item);
        if (_test == null || BuggedItems.Contains(id)) return false;
        
        var bugList = new List<string>();
        if (item is TMajorRecordGetter tGetter) {
            if (_test(tGetter, bugList)) return true;
            
            id.Bugs.AddRange(bugList);
            bugList.Clear();
            BuggedItems.Add(id);
        }
        
        return false;
    }

    public void Patch(IMajorRecord item, ISkyrimMod? container) {
        if (_patch == null || item is not TMajorRecord tSetter) return;

        _patch(tSetter, IRecordTest.PatchMod);

        BuggedItems.Remove(new BuggedRecord(item));
    }
}