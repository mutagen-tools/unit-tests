﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mutagen.Bethesda.FormKeys.SkyrimSE;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Skyrim;
using Noggog;
namespace SkyrimUnitTests.TestSuite.Tester.Record; 

public class WeaponRecordTest : RecordTest<IWeapon, IWeaponGetter, IWeaponGetter> {
    public override string Name => "Weapon";
    public override List<RecordBug<IWeapon, IWeaponGetter>> Bugs { get; } = [
        new RecordBug<IWeapon, IWeaponGetter>("Missing Vendor Keyword", "Should have VendorItemWeapon keyword",
            BugSeverity.Inconsistency, true, TestVendorKeyword, PatchVendorKeyword),
        new RecordBug<IWeapon, IWeaponGetter>("Keyword/Animation don't match", "WeapType keywords should be set based on animation type",
            BugSeverity.Inconsistency, true, TestKeywordAnimation, PatchKeywordAnimation),
        new RecordBug<IWeapon, IWeaponGetter>("Skill/Animation don't match", "Skill should match weapon animation type",
            BugSeverity.Inconsistency, true, TestSkillAnimation, PatchSkillAnimation),
        new RecordBug<IWeapon, IWeaponGetter>("Sound(s) not set", "Attack (fail), and (un)equip sound should be set",
            BugSeverity.Warning, true, TestNoSound),
        new RecordBug<IWeapon, IWeaponGetter>("Missing Models", "Should have model and usually 1st Person model object",
            BugSeverity.Warning, true, TestMissingModels),
    ];

    private static readonly List<FormLink<IKeywordGetter>> WeaponTypeKeywords = [
        Skyrim.Keyword.WeapTypeWarhammer,
        Skyrim.Keyword.WeapTypeBattleaxe,
        Skyrim.Keyword.WeapTypeBow,
        Skyrim.Keyword.WeapTypeDagger,
        Skyrim.Keyword.WeapTypeGreatsword,
        Skyrim.Keyword.WeapTypeMace,
        Skyrim.Keyword.WeapTypeStaff,
        Skyrim.Keyword.WeapTypeSword,
        Skyrim.Keyword.WeapTypeWarAxe,
    ];

    protected override IEnumerable<IWeaponGetter> GetTestRelevantRecords(IWeaponGetter record) {
        yield return record;
    }

    //Missing Vendor Keyword
    private static bool TestVendorKeyword(IWeaponGetter weapon, List<string> list) {
        return weapon.Keywords != null
         && weapon.Keywords.Contains(weapon.Data?.AnimationType == WeaponAnimationType.Staff ? Skyrim.Keyword.VendorItemStaff : Skyrim.Keyword.VendorItemWeapon);
    }
    
    private static void PatchVendorKeyword(IWeapon weapon, ISkyrimMod patchFile) {
        weapon.Keywords ??= new ExtendedList<IFormLinkGetter<IKeywordGetter>>();
        weapon.Keywords.Add(weapon.Data?.AnimationType switch {
            WeaponAnimationType.Staff => Skyrim.Keyword.VendorItemStaff,
            _ => Skyrim.Keyword.VendorItemWeapon
        });
    }
    
    //Keyword/Animation don't match
    private static bool TestKeywordAnimation(IWeaponGetter weapon, List<string> list) {
        return weapon.Data?.AnimationType switch {
            WeaponAnimationType.OneHandSword when HasOtherWeaponTypes(weapon, Skyrim.Keyword.WeapTypeSword) => false,
            WeaponAnimationType.OneHandDagger when HasOtherWeaponTypes(weapon, Skyrim.Keyword.WeapTypeDagger) => false,
            WeaponAnimationType.OneHandAxe when HasOtherWeaponTypes(weapon, Skyrim.Keyword.WeapTypeWarAxe) => false,
            WeaponAnimationType.OneHandMace when HasOtherWeaponTypes(weapon, Skyrim.Keyword.WeapTypeMace) => false,
            WeaponAnimationType.TwoHandSword when HasOtherWeaponTypes(weapon, Skyrim.Keyword.WeapTypeGreatsword) => false,
            WeaponAnimationType.TwoHandAxe when HasOtherWeaponTypes(weapon, Skyrim.Keyword.WeapTypeBattleaxe, Skyrim.Keyword.WeapTypeWarhammer) => false,
            WeaponAnimationType.Bow when HasOtherWeaponTypes(weapon, Skyrim.Keyword.WeapTypeBow) => false,
            WeaponAnimationType.Crossbow when HasOtherWeaponTypes(weapon, Skyrim.Keyword.WeapTypeBow) => false,
            WeaponAnimationType.Staff when HasOtherWeaponTypes(weapon, Skyrim.Keyword.WeapTypeStaff) => false,
            _ => true
        };
    }

    private static bool HasOtherWeaponTypes(IWeaponGetter weapon, FormLink<IKeywordGetter> weaponType, FormLink<IKeywordGetter>? weaponType2 = null) {
        return weapon.Keywords == null
         || !weapon.Keywords.Contains(weaponType) && (weaponType2 == null || !weapon.Keywords.Contains(weaponType2))
         || weapon.Keywords.Any(keyword => !keyword.Equals(weaponType) && !keyword.Equals(weaponType2) && WeaponTypeKeywords.Contains(keyword));
    }

    private static void PatchKeywordAnimation(IWeapon weapon, ISkyrimMod patchFile) {
        var newKeyword = weapon.Data?.AnimationType switch {
            WeaponAnimationType.OneHandSword when HasOtherWeaponTypes(weapon, Skyrim.Keyword.WeapTypeSword) => Skyrim.Keyword.WeapTypeSword,
            WeaponAnimationType.OneHandDagger when HasOtherWeaponTypes(weapon, Skyrim.Keyword.WeapTypeDagger) => Skyrim.Keyword.WeapTypeDagger,
            WeaponAnimationType.OneHandAxe when HasOtherWeaponTypes(weapon, Skyrim.Keyword.WeapTypeWarAxe) => Skyrim.Keyword.WeapTypeWarAxe,
            WeaponAnimationType.OneHandMace when HasOtherWeaponTypes(weapon, Skyrim.Keyword.WeapTypeMace) => Skyrim.Keyword.WeapTypeMace,
            WeaponAnimationType.TwoHandSword when HasOtherWeaponTypes(weapon, Skyrim.Keyword.WeapTypeGreatsword) => Skyrim.Keyword.WeapTypeGreatsword,
            WeaponAnimationType.TwoHandAxe when weapon.Name?.String != null && weapon.Name.String.Contains("axe", StringComparison.OrdinalIgnoreCase)
             && HasOtherWeaponTypes(weapon, Skyrim.Keyword.WeapTypeBattleaxe) => Skyrim.Keyword.WeapTypeBattleaxe,
            WeaponAnimationType.TwoHandAxe when weapon.Name?.String != null && weapon.Name.String.Contains("warhammer", StringComparison.OrdinalIgnoreCase)
             && HasOtherWeaponTypes(weapon, Skyrim.Keyword.WeapTypeWarhammer) => Skyrim.Keyword.WeapTypeWarhammer,
            WeaponAnimationType.Bow when HasOtherWeaponTypes(weapon, Skyrim.Keyword.WeapTypeBow) => Skyrim.Keyword.WeapTypeBow,
            WeaponAnimationType.Crossbow when HasOtherWeaponTypes(weapon, Skyrim.Keyword.WeapTypeBow) => Skyrim.Keyword.WeapTypeBow,
            WeaponAnimationType.Staff when HasOtherWeaponTypes(weapon, Skyrim.Keyword.WeapTypeStaff) => Skyrim.Keyword.WeapTypeStaff,
            _ => null
        };

        if (newKeyword != null) {
            weapon.Keywords ??= new ExtendedList<IFormLinkGetter<IKeywordGetter>>();
            foreach (var keyword in WeaponTypeKeywords) weapon.Keywords.Remove(keyword);
            weapon.Keywords.Add(newKeyword);
        }
    }

    //Skill/Animation don't match
    private static bool TestSkillAnimation(IWeaponGetter weapon, List<string> list) {
        return weapon.Data?.AnimationType switch {
            WeaponAnimationType.OneHandSword when weapon.Data.Skill != Skill.OneHanded => false,
            WeaponAnimationType.OneHandDagger when weapon.Data.Skill != Skill.OneHanded => false,
            WeaponAnimationType.OneHandAxe when weapon.Data.Skill != Skill.OneHanded => false,
            WeaponAnimationType.OneHandMace when weapon.Data.Skill != Skill.OneHanded => false,
            WeaponAnimationType.TwoHandSword when weapon.Data.Skill != Skill.TwoHanded => false,
            WeaponAnimationType.TwoHandAxe when weapon.Data.Skill != Skill.TwoHanded => false,
            WeaponAnimationType.Bow when weapon.Data.Skill != Skill.Archery => false,
            WeaponAnimationType.Crossbow when weapon.Data.Skill != Skill.Archery => false,
            WeaponAnimationType.Staff when weapon.Data.Skill != null && weapon.Data.Skill != Skill.Alteration && weapon.Data.Skill != Skill.Conjuration
             && weapon.Data.Skill != Skill.Destruction && weapon.Data.Skill != Skill.Illusion  && weapon.Data.Skill != Skill.Restoration => false,
            _ => true
        };
    }

    private static void PatchSkillAnimation(IWeapon weapon, ISkyrimMod patchFile) {
        var newSkill = weapon.Data?.AnimationType switch {
            WeaponAnimationType.OneHandSword when weapon.Data.Skill != Skill.OneHanded => Skill.OneHanded,
            WeaponAnimationType.OneHandDagger when weapon.Data.Skill != Skill.OneHanded => Skill.OneHanded,
            WeaponAnimationType.OneHandAxe when weapon.Data.Skill != Skill.OneHanded => Skill.OneHanded,
            WeaponAnimationType.OneHandMace when weapon.Data.Skill != Skill.OneHanded => Skill.OneHanded,
            WeaponAnimationType.TwoHandSword when weapon.Data.Skill != Skill.TwoHanded => Skill.TwoHanded,
            WeaponAnimationType.TwoHandAxe when weapon.Data.Skill != Skill.TwoHanded => Skill.TwoHanded,
            WeaponAnimationType.Bow when weapon.Data.Skill != Skill.Archery => Skill.Archery,
            WeaponAnimationType.Crossbow when weapon.Data.Skill != Skill.Archery => Skill.Archery,
            _ => Skill.Alchemy
        };

        if (newSkill != Skill.Alchemy) {
            weapon.Data ??= new WeaponData();
            weapon.Data.Skill = newSkill;
        }
    }
    
    //Sound(s) not set
    private static bool TestNoSound(IWeaponGetter weapon, List<string> list) {
        return !weapon.EquipSound.IsNull && !weapon.UnequipSound.IsNull && (!weapon.AttackFailSound.IsNull || weapon.Data?.AnimationType is WeaponAnimationType.Bow or WeaponAnimationType.Crossbow);
    }
    
    //Missing Models
    private static bool TestMissingModels(IWeaponGetter weapon, List<string> list) {
        return weapon.Model != null && !weapon.Model.File.IsNullOrEmpty() && !weapon.FirstPersonModel.IsNull;
    }
}