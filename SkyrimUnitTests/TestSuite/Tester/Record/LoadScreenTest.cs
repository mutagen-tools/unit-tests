﻿using System.Collections.Generic;
using Mutagen.Bethesda.Skyrim;
using Noggog;
namespace SkyrimUnitTests.TestSuite.Tester.Record; 

public class LoadScreenRecordTest : RecordTest<ILoadScreen, ILoadScreenGetter, ILoadScreenGetter> {
    public override string Name => "LoadScreen";
    public override List<RecordBug<ILoadScreen, ILoadScreenGetter>> Bugs { get; } = [
        new RecordBug<ILoadScreen, ILoadScreenGetter>("No Description", "Every load screen should have a description",
            BugSeverity.Warning, true, TestDescription),
        new RecordBug<ILoadScreen, ILoadScreenGetter>("No 3D Model", "Every load screen should have a 3D model",
            BugSeverity.Warning, true, Test3DModel)
    ];

    protected override IEnumerable<ILoadScreenGetter> GetTestRelevantRecords(ILoadScreenGetter record) {
        yield return record;
    }

    //No Description
    private static bool TestDescription(ILoadScreenGetter loadScreen, List<string> list) {
        return loadScreen.Description.String != null && !loadScreen.Description.String.IsNullOrEmpty();
    }

    //No 3D Model
    private static bool Test3DModel(ILoadScreenGetter loadScreen, List<string> list) {
        return !loadScreen.LoadingScreenNif.IsNull;
    }
}