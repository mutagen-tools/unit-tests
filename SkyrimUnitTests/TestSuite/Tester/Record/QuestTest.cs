﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Skyrim;
using Noggog;
namespace SkyrimUnitTests.TestSuite.Tester.Record; 

public class QuestRecordTest : RecordTest<IQuest, IQuestGetter, IQuestGetter> {
    public override string Name => "Quest";
    public override List<RecordBug<IQuest, IQuestGetter>> Bugs { get; } = [
        new RecordBug<IQuest, IQuestGetter>("Story Manager Quest not assigned", "Story Manager Quest should be assigned to a story manager quest node",
            BugSeverity.Warning, true, TestStoryManagerQuestAssigned),
        new RecordBug<IQuest, IQuestGetter>("No Object Window Filter", "Quest has no Object Window Filter",
            BugSeverity.Inconsistency, true, TestNoObjectWindowFilter),
        new RecordBug<IQuest, IQuestGetter>("Invalid Actor Dialogue quest setup", "Actor Dialogue quest need a special setup",
            BugSeverity.Warning, true, TestInvalidActorDialogueQuestSetup),
        new RecordBug<IQuest, IQuestGetter>("Invalid Dialogue Alias", "Alias that is forced to none, no additional dialogue formlist/npc and has dialogue conditioned to it doesn't get any lines for VA exported",
            BugSeverity.Warning, true, TestInvalidDialogueAlias)
    ];

    protected override IEnumerable<IQuestGetter> GetTestRelevantRecords(IQuestGetter record) {
        yield return record;
    }

    //Story Manager Quest not assigned
    private static HashSet<FormKey>? _questNodes;
    
    private static bool TestStoryManagerQuestAssigned(IQuestGetter quest, List<string> list) {
        if (!quest.Event.HasValue) return true;

        if (_questNodes == null) {
            _questNodes = new HashSet<FormKey>();
            foreach (var mod in IRecordTest.Environment.LoadOrder.Select(l => l.Value.Mod).NotNull()) {
                foreach (var questNode in mod.StoryManagerQuestNodes) {
                    foreach (var questFormKey in questNode.Quests.Select(n => n.Quest.FormKey)) {
                        _questNodes.Add(questFormKey);
                    }
                }
            }
        }

        return _questNodes.Contains(quest.FormKey);
    }

    //No Object Window Filter
    private static bool TestNoObjectWindowFilter(IQuestGetter quest, List<string> list) {
        return !quest.Filter.IsNullOrWhitespace();
    }

    //Invalid Actor Dialogue quest setup
    private static bool TestInvalidActorDialogueQuestSetup(IQuestGetter quest, List<string> list) {
        if (!quest.Event.HasValue) return true;
        if (quest.Event.Value != "ADIA") return true;

        if (quest.Aliases.Count == 0) {
            list.Add("Quest has no aliases");
            return false;
        }

        if (quest.Aliases.Count % 2 != 0) {
            list.Add("Quest has an odd number of aliases");
            return false;
        }

        var firstHalf = quest.Aliases.Take(quest.Aliases.Count / 2).ToList();
        var secondHalf = quest.Aliases.Skip(quest.Aliases.Count / 2).ToList();

        var startsWithEventAlias = quest.Aliases[0].FindMatchingRefFromEvent is not null;
        var eventAliases = startsWithEventAlias ? firstHalf : secondHalf;
        for (var i = 0; i < eventAliases.Count; i++) {
            var eventAlias = eventAliases[i];
            if (i < 2) {
                // Event filled alias
                if (eventAlias.FindMatchingRefFromEvent is null) {
                    list.Add($"Alias {eventAlias.Name} isn't Find Matching Reference From Event");
                }
                if (eventAlias.Conditions.Count != eventAliases.Count) {
                    list.Add($"Alias {eventAlias.Name} doesn't have the same number of conditions as the number of npcs in the scene");
                }
                if (eventAlias.Conditions.Any(condition => condition.Data is not IGetIsIDConditionDataGetter)) {
                    list.Add($"Alias {eventAlias.Name} has a condition that isn't GetIsID");
                }
            } else {
                // Distance filled alias
                if (eventAlias.Conditions.Count != eventAliases.Count + 1) {
                    list.Add($"Alias {eventAlias.Name} doesn't have the same number of conditions as the number of npcs in the scene");
                }
                // All but one of the conditions should be GetIsID, the other one should be GetDistance
                if (eventAlias.Conditions.Count(condition => condition.Data is IGetIsIDConditionDataGetter) != eventAliases.Count) {
                    list.Add($"Alias {eventAlias.Name} doesn't have the same number of GetIsID conditions as the number of npcs in the scene");
                }
                if (eventAlias.Conditions.Count(condition => condition.Data is IGetDistanceConditionDataGetter) != 1) {
                    list.Add($"Alias {eventAlias.Name} doesn't have exactly one GetDistance condition");
                }
            }
            // if ((eventAlias.Flags & QuestAlias.Flag.AllowReserved) == 0) {
            //     list.Add($"Alias {eventAlias.Name} doesn't have Allow Reserved flag");
            // }
            
        }

        var npcAliases = startsWithEventAlias ? secondHalf : firstHalf;
        foreach (var npcAlias in npcAliases) {
            if (npcAlias.UniqueActor.IsNull) {
                list.Add($"Alias {npcAlias.Name} doesn't have a Unique Actor");
            }
            // if ((npcAlias.Flags & QuestAlias.Flag.AllowReserved) == 0) {
            //     list.Add($"Alias {npcAlias.Name} doesn't have Allow Reserved flag");
            // }
        }
        
        foreach (var alias in secondHalf) {
            if ((alias.Flags & QuestAlias.Flag.AllowReuseInQuest) == 0) {
                list.Add($"Alias {alias.Name} doesn't have Allow Reuse In Quest flag");
            }
        }

        return list.Count == 0;
    }
    
    //Invalid Dialogue Alias
    private static Dictionary<FormKey, Dictionary<int, List<FormKey>>>? _referencedAliases;

    private static bool TestInvalidDialogueAlias(IQuestGetter quest, List<string> list) {
        var relevantAliases = (from alias in quest.Aliases where alias.ForcedReference.IsNull && !alias.Conditions.Any() && alias.SpecificLocation.IsNull && alias.UniqueActor.IsNull && alias.VoiceTypes.IsNull
         && alias.External == null && alias.CreateReferenceToObject == null && alias.FindMatchingRefFromEvent == null && alias.FindMatchingRefNearAlias == null select (int) alias.ID).ToList();
        if (!relevantAliases.Any()) return true;

        if (_referencedAliases == null) {
            _referencedAliases = new Dictionary<FormKey, Dictionary<int, List<FormKey>>>();
            foreach (var mod in IRecordTest.Environment.LoadOrder.Select(l => l.Value.Mod).NotNull()) {
                foreach (var topic in mod.DialogTopics) {
                    foreach (var response in topic.Responses) {
                        if (!response.Speaker.IsNull) continue; //TODO enhance with voice type parse function

                        foreach (var condition in response.Conditions) {
                            if (condition.Data is not IGetIsAliasRefConditionDataGetter { ReferenceAliasIndex: var aliasIndex }) continue;
                            if (condition is IConditionFloatGetter { ComparisonValue: 0 }) continue;

                            if (_referencedAliases.ContainsKey(topic.Quest.FormKey)) {
                                if (_referencedAliases[topic.Quest.FormKey].TryGetValue(aliasIndex, out var value)) {
                                    value.Add(response.FormKey);
                                } else {
                                    _referencedAliases[topic.Quest.FormKey].Add(aliasIndex, [response.FormKey]);
                                }
                            } else {
                                _referencedAliases.Add(topic.Quest.FormKey, new Dictionary<int, List<FormKey>> {{ aliasIndex, [response.FormKey] }});
                            }
                        }
                    }
                }
            }
        }

        foreach (var alias in relevantAliases) {
            if (_referencedAliases.ContainsKey(quest.FormKey) && _referencedAliases[quest.FormKey].ContainsKey(alias)) {
                var sb = new StringBuilder();
                sb.AppendLine($"Alias {alias.ToString()}:");
                foreach (var response in _referencedAliases[quest.FormKey][alias]) {
                    sb.AppendLine(response.ToString());
                }
                list.Add(sb.ToString());
            }
        }

        return !list.Any();
    }
}