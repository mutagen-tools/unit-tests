﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mutagen.Bethesda.FormKeys.SkyrimSE;
using Mutagen.Bethesda.Skyrim;
namespace SkyrimUnitTests.TestSuite.Tester.Record;

public class PlacedObjectRecordTest : RecordTest<IPlacedObject, IPlacedObjectGetter, ICellGetter> {
    public override string Name => "Placed Object";
    public override List<RecordBug<IPlacedObject, IPlacedObjectGetter>> Bugs { get; } = [
        new RecordBug<IPlacedObject, IPlacedObjectGetter>("Scaling should be limited", "Scaling objects too much looks strange and inconsistent",
            BugSeverity.Inconsistency, true, TestScale),
        new RecordBug<IPlacedObject, IPlacedObjectGetter>("MultiBound Markers should not be used", "MultiBound Markers are not used in Skyrim, seemingly don't work and should not be used",
            BugSeverity.Warning, true, TestExteriorMultiBoundMarkers),
    ];

    protected override IEnumerable<IPlacedObjectGetter> GetTestRelevantRecords(ICellGetter record) {
        if (record.EditorID != null) {
            if (record.EditorID.StartsWith("z", StringComparison.OrdinalIgnoreCase)) yield break;
            if (record.EditorID.EndsWith("old", StringComparison.OrdinalIgnoreCase)) yield break;
        }

        foreach (var placedObject in record.Temporary.Concat(record.Persistent).OfType<IPlacedObjectGetter>()) {
            if (placedObject.IsDeleted) continue;

            yield return placedObject;
        }
    }

    //Scaling should be limited
    private static bool TestScale(IPlacedObjectGetter placedObject, List<string> list) {
        if (placedObject.Scale is null or >= 0.5f and <= 1.5f) return true;

        // Whitelisted objects
        if (placedObject.Base.FormKey == Skyrim.Static.BlackPlane01.FormKey) return true;
        if (placedObject.Base.FormKey == Skyrim.Door.AutoLoadDoor01.FormKey) return true;
        if (placedObject.Base.FormKey == Skyrim.Door.AutoLoadDoorMinUse01.FormKey) return true;
        if (placedObject.Base.FormKey == Skyrim.Door.AutoLoadDoorHiddenMinUse01.FormKey) return true;

        var baseObject = placedObject.Base.TryResolve(IRecordTest.LinkCache);
        var baseObjectEditorID = baseObject?.EditorID;
        if (baseObjectEditorID is null) return false;

        // Whitelisted editor ids
        if (baseObjectEditorID.StartsWith("dwe", StringComparison.OrdinalIgnoreCase)) return true;
        if (baseObjectEditorID.Contains("mine", StringComparison.OrdinalIgnoreCase)) return true;
        if (baseObjectEditorID.Contains("cave", StringComparison.OrdinalIgnoreCase)) return true;
        if (baseObjectEditorID.Contains("mountain", StringComparison.OrdinalIgnoreCase)) return true;
        if (baseObjectEditorID.Contains("rock", StringComparison.OrdinalIgnoreCase)) return true;
        if (baseObjectEditorID.Contains("water", StringComparison.OrdinalIgnoreCase)) return true;
        if (baseObjectEditorID.Contains("fx", StringComparison.OrdinalIgnoreCase)) return true;
        if (baseObjectEditorID.Contains("web", StringComparison.OrdinalIgnoreCase)) return true;
        if (baseObjectEditorID.Contains("bsdev", StringComparison.OrdinalIgnoreCase)) return true;

        // Specific type filter
        switch (baseObject) {
            case IActivatorGetter activator:
                break;
            case IContainerGetter container:
                break;
            case IDoorGetter door:
                break;
            case IFloraGetter flora:
                if (placedObject.Scale is null or >= 0.1f and <= 3f) return true;

                break;
            case IFurnitureGetter furniture:
                break;
            case IIngestibleGetter ingestible:
                break;
            case IMoveableStaticGetter moveableStatic:
                if (placedObject.Scale is null or >= 0.2f and <= 3f) return true;

                break;
            case IStaticGetter @static:
                if (placedObject.Scale is null or >= 0.2f and <= 2.5f) return true;

                break;
            case ITreeGetter tree:
                if (placedObject.Scale is null or >= 0.1f and <= 3f) return true;

                break;
            case ILightGetter light:
            case IIdleMarkerGetter:
            case ITextureSetGetter:
            case ISoundMarkerGetter:
            case ISpellGetter:
                return true;
        }

        Console.WriteLine($"{baseObjectEditorID} placed by {placedObject.FormKey} with scale {placedObject.Scale}");

        return false;
    }

    // MultiBound Markers should not be in exteriors
    private static bool TestExteriorMultiBoundMarkers(IPlacedObjectGetter placedObject, List<string> list) {
        return placedObject.Base.FormKey != Skyrim.Static.RoomMarker.FormKey;
    }
}
