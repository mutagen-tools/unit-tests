﻿using System.Collections.Generic;
using Mutagen.Bethesda.Skyrim;
namespace SkyrimUnitTests.TestSuite.Tester.Record; 

public class MagicEffectRecordTest : RecordTest<IMagicEffect, IMagicEffectGetter, IMagicEffectGetter> {
    public override string Name => "MagicEffect";
    public override List<RecordBug<IMagicEffect, IMagicEffectGetter>> Bugs { get; } = [
        new RecordBug<IMagicEffect, IMagicEffectGetter>("Empty Menu Display", "Spells should have an inventory symbol, MagicHatMarker if not filled",
            BugSeverity.Inconsistency, true, TestThreeWords)
    ];
    
    protected override IEnumerable<IMagicEffectGetter> GetTestRelevantRecords(IMagicEffectGetter record) {
        yield return record;
    }

    //Not Three Words
    private static bool TestThreeWords(IMagicEffectGetter magicEffect, List<string> list) {
        return !magicEffect.MenuDisplayObject.IsNull;
    }
}