﻿using System.Collections.Generic;
using Mutagen.Bethesda.Skyrim;
namespace SkyrimUnitTests.TestSuite.Tester.Record; 

public class IngredientRecordTest : RecordTest<IIngredient, IIngredientGetter, IIngredientGetter> {
    public override string Name => "Ingredient";
    public override List<RecordBug<IIngredient, IIngredientGetter>> Bugs { get; } = [
        new RecordBug<IIngredient, IIngredientGetter>("No Pickup Sounds", "Ingredients should have at least a pickup sound",
            BugSeverity.Inconsistency, true, TestSounds),
        new RecordBug<IIngredient, IIngredientGetter>("Not Four Effects", "Ingredients should have four effects",
            BugSeverity.Inconsistency, true, TestNoEffects)
    ];

    protected override IEnumerable<IIngredientGetter> GetTestRelevantRecords(IIngredientGetter record) {
        yield return record;
    }

    //No Pickup Sounds
    private static bool TestSounds(IIngredientGetter ingredient, List<string> list) {
        return !ingredient.PickUpSound.IsNull;
    }

    //Not Four Effects
    private static bool TestNoEffects(IIngredientGetter ingredient, List<string> list) {
        return ingredient.Effects.Count == 4;
    }
}