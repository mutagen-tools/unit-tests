﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Skyrim;
namespace SkyrimUnitTests.TestSuite.Tester.Record;

public record DuplicateLookup(IPlacementGetter Placement, float Scale, FormKey BaseFormKey) {
    public DuplicateLookup(IPlacedObjectGetter placedObject) : this(placedObject.Placement ?? new Placement(), placedObject.Scale ?? 1.0f, placedObject.Base.FormKey) {}
    
    public virtual bool Equals(DuplicateLookup? other) {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;

        return Placement.Equals(other.Placement) && Scale.Equals(other.Scale) && BaseFormKey.Equals(other.BaseFormKey);
    }
    
    public override int GetHashCode() {
        return HashCode.Combine(Placement, Scale);
    }
}

public class CellRecordTest : RecordTest<ICell, ICellGetter, ICellGetter> {
    public override string Name => "Cell";
    public override List<RecordBug<ICell, ICellGetter>> Bugs { get; } = [
        new RecordBug<ICell, ICellGetter>("Duplicate References", "Remove duplicate references (detected by identical placement and same base object)",
            BugSeverity.Error, true, TestDuplicates, PatchDuplicates)
    ];

    private static readonly Dictionary<DuplicateLookup, IPlacedObjectGetter> Placements = new();
    private static readonly Dictionary<FormKey, HashSet<FormKey>> Duplicates = new();

    protected override IEnumerable<ICellGetter> GetTestRelevantRecords(ICellGetter record) {
        yield return record;
    }
    
    //Duplicate References
    private static bool TestDuplicates(ICellGetter cell, List<string> list) {
        HashSet<FormKey> duplicates;
        if (Duplicates.TryGetValue(cell.FormKey, out var dup)) {
            duplicates = dup;
        } else {
            duplicates = new HashSet<FormKey>();
            Duplicates.Add(cell.FormKey, duplicates);
        }
        
        foreach (var placed in cell.Temporary.Concat(cell.Persistent)) {
            if (placed.IsDeleted) continue;
            if (placed.Placement == null) continue;
            if (placed is not IPlacedObjectGetter placedObject) continue;

            var duplicateLookup = new DuplicateLookup(placedObject);
            if (!Placements.TryAdd(duplicateLookup, placedObject)) {
                var duplicate = Placements[duplicateLookup];
                var sb = new StringBuilder();
                sb.AppendLine($"Conflict on: {placedObject.FormKey} {duplicate.FormKey}");
                
                var canDeleteCurrent = CanDelete(placedObject);
                if (canDeleteCurrent || CanDelete(duplicate)) {
                    var deleteFormKey = (canDeleteCurrent ? placedObject : duplicate).FormKey;
                    duplicates.Add(deleteFormKey);
                    sb.AppendLine($"Suggest to delete {deleteFormKey}");
                }
                list.Add(sb.ToString());
            }
        }
        Placements.Clear();
        
        return duplicates.Count == 0;
    }
    
    private static void PatchDuplicates(ICell cell, ISkyrimMod patchFile) {
        if (!Duplicates.ContainsKey(cell.FormKey)) return;

        foreach (var duplicateFormKey in Duplicates[cell.FormKey]) {
            var context = IRecordTest.LinkCache.ResolveContext<IPlacedObject, IPlacedObjectGetter>(duplicateFormKey);
            var placedObject = context.GetOrAddAsOverride(patchFile);
            placedObject.IsDeleted = true;
        }
    }

    private static bool CanDelete(IPlacedObjectGetter placedObject) {
        return placedObject.EditorID == null && placedObject.VirtualMachineAdapter == null && placedObject.EnableParent == null && placedObject.TeleportDestination == null && placedObject.LocationRefTypes == null
         && placedObject.ActivateParents == null && placedObject.Patrol == null && !placedObject.LinkedReferences.Any() && placedObject.AttachRef.IsNull;
    }
}