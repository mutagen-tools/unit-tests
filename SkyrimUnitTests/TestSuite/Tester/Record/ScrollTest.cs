﻿using System.Collections.Generic;
using System.Linq;
using Mutagen.Bethesda.FormKeys.SkyrimSE;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Skyrim;
using Noggog;
namespace SkyrimUnitTests.TestSuite.Tester.Record; 

public class ScrollRecordTest : RecordTest<IScroll, IScrollGetter, IScrollGetter> {
    public override string Name => "Scroll";
    public override List<RecordBug<IScroll, IScrollGetter>> Bugs { get; } = [
        new RecordBug<IScroll, IScrollGetter>("Effect List Empty", "Scrolls should have at least one effect",
            BugSeverity.Warning, true, TestEffectList),
        new RecordBug<IScroll, IScrollGetter>("Missing Vendor Keyword", "Scrolls should have VendorItemScroll keyword",
            BugSeverity.Inconsistency, true, TestVendorKeyword, PatchVendorKeyword)
    ];

    protected override IEnumerable<IScrollGetter> GetTestRelevantRecords(IScrollGetter record) {
        yield return record;
    }

    //Effect List Empty
    private static bool TestEffectList(IScrollGetter scroll, List<string> list) {
        return scroll.Effects.Any();
    }
    
    //Missing Vendor Keyword
    private static bool TestVendorKeyword(IScrollGetter scroll, List<string> list) {
        return scroll.Keywords != null && scroll.Keywords.Any(k => k.FormKey == Skyrim.Keyword.VendorItemScroll.FormKey);
    }

    private static void PatchVendorKeyword(IScroll scroll, ISkyrimMod patchMod) {
        scroll.Keywords ??= new ExtendedList<IFormLinkGetter<IKeywordGetter>>();
        scroll.Keywords.Add(new FormLink<IKeywordGetter>(Skyrim.Keyword.VendorItemScroll.FormKey));
    }
}