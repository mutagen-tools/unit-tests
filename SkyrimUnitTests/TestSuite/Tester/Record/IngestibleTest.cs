﻿using System.Collections.Generic;
using System.Linq;
using Mutagen.Bethesda.Skyrim;
namespace SkyrimUnitTests.TestSuite.Tester.Record; 

public class IngestibleRecordTest : RecordTest<IIngestible, IIngestibleGetter, IIngestibleGetter> {
    public override string Name => "Ingestible";
    public override List<RecordBug<IIngestible, IIngestibleGetter>> Bugs { get; } = [
        new RecordBug<IIngestible, IIngestibleGetter>("Effect List Empty", "Ingestibles should have at least one effect",
            BugSeverity.Warning, true, TestEffectList),
        new RecordBug<IIngestible, IIngestibleGetter>("No Consume Sound", "Ingestibles should have a consume sound",
            BugSeverity.Inconsistency, true, TestNoConsumeSound)
    ];

    protected override IEnumerable<IIngestibleGetter> GetTestRelevantRecords(IIngestibleGetter record) {
        yield return record;
    }

    //Effect List Empty
    private static bool TestEffectList(IIngestibleGetter ingestible, List<string> list) {
        return ingestible.Effects.Any();
    }
    
    //No Consume Sound
    private static bool TestNoConsumeSound(IIngestibleGetter ingestible, List<string> list) {
        return !ingestible.ConsumeSound.IsNull;
    }
}