﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mutagen.Bethesda.FormKeys.SkyrimSE;
using Mutagen.Bethesda.Skyrim;
namespace SkyrimUnitTests.TestSuite.Tester.Record;

public class PersistentPlacedObjectTest : RecordTest<IPlaced, IPlacedGetter, ICellGetter> {
	public override string Name => "Persistent Placed";
	public override List<RecordBug<IPlaced, IPlacedGetter>> Bugs { get; } = [
		new RecordBug<IPlaced, IPlacedGetter>("Minimal Persistence", "Persistence should be used as little as possible to reduce overhead on the system",
			BugSeverity.Warning, true, TestPersistence, PatchPersistence),
	];

	protected override IEnumerable<IPlacedGetter> GetTestRelevantRecords(ICellGetter record) {
		if (record.EditorID != null) {
			if (record.EditorID.StartsWith("z", StringComparison.OrdinalIgnoreCase)) yield break;
			if (record.EditorID.EndsWith("old", StringComparison.OrdinalIgnoreCase)) yield break;
		}

		foreach (var placedObject in record.Persistent) {
			if (placedObject.IsDeleted) continue;

			yield return placedObject;
		}
	}

	// Minimal Persistence
	private static bool TestPersistence(IPlacedGetter placed, List<string> list) {
		if ((placed.MajorRecordFlagsRaw & 0x10000) != 0) return true;

		// Objects that are allowed to be persistent
		if (placed.FormKey == Skyrim.Static.COCMarkerHeading.FormKey) return true;
		if (placed.FormKey == Skyrim.Static.PlaneMarker.FormKey) return true;
		if (placed.FormKey == Skyrim.Static.RoomMarker.FormKey) return true;
		if (placed.FormKey == Skyrim.Static.PortalMarker.FormKey) return true;
		if (placed.FormKey == Skyrim.Static.MultiBoundMarker.FormKey) return true;

		var references = IRecordTest.ReferenceQuery.GetReferences(placed.FormKey);
		if (references.Any()) return true;

		switch (placed) {
			case IPlacedObjectGetter placedObjectGetter:
				if (placedObjectGetter.MapMarker is not null) return true;
				if (placedObjectGetter.VirtualMachineAdapter is not null) return true;
				// if (placedObjectGetter.LocationRefTypes is not null) return true;
				if (placedObjectGetter.LinkedReferences.Any()) return true;

				// Base types that are allowed to be persistent
				if (IRecordTest.LinkCache.TryResolve<IDoorGetter>(placedObjectGetter.Base.FormKey, out _)) return true;
				if (IRecordTest.LinkCache.TryResolve<ITextureSetGetter>(placedObjectGetter.Base.FormKey, out _)) return true;

				break;
			case IPlacedNpcGetter placedNpcGetter:
				if (placedNpcGetter.VirtualMachineAdapter is not null) return true;
				// if (placedNpcGetter.LocationRefTypes is not null) return true;

				var npc = placedNpcGetter.Base.TryResolve(IRecordTest.LinkCache);
				if (npc is not null) {
					Console.WriteLine("Removing actor persistence: " + (npc.Name?.String ?? npc.EditorID ?? npc.FormKey.ToString()));
				}

				break;
			case IAPlacedTrapGetter placedTrapGetter:
				if (placedTrapGetter.VirtualMachineAdapter is not null) return true;
				// if (placedTrapGetter.LocationRefTypes is not null) return true;

				break;
		}

		return false;
	}

	private static void PatchPersistence(IPlaced placed, ISkyrimMod mod) {
		// Remove flag 16
		placed.MajorRecordFlagsRaw &= ~0x400;
		
		if (IRecordTest.PatchLinkCache.TryResolveSimpleContext<PlacedObject>(placed.FormKey, out var placedObject)) {
			if (placedObject.Parent?.Record is ICell cell) {
				cell.Persistent.Remove(placed);
				cell.Temporary.Add(placed);
			}
		} else  if (IRecordTest.PatchLinkCache.TryResolveSimpleContext<PlacedNpc>(placed.FormKey, out var placedNpc)) {
			if (placedNpc.Parent?.Record is ICell cell) {
				cell.Persistent.Remove(placed);
				cell.Temporary.Add(placed);
			}
		}
	}
}
