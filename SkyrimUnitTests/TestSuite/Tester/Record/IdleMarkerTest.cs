﻿using System.Collections.Generic;
using System.Linq;
using Mutagen.Bethesda.Skyrim;
namespace SkyrimUnitTests.TestSuite.Tester.Record; 

public class IdleMarkerRecordTest : RecordTest<IIdleMarker, IIdleMarkerGetter, IIdleMarkerGetter> {
    public override string Name => "IdleMarker";
    public override List<RecordBug<IIdleMarker, IIdleMarkerGetter>> Bugs { get; } = [
        new RecordBug<IIdleMarker, IIdleMarkerGetter>("No Idles", "Idle Markers should have at least one idle, otherwise they're useless",
            BugSeverity.Warning, true, TestThreeWords)
    ];

    protected override IEnumerable<IIdleMarkerGetter> GetTestRelevantRecords(IIdleMarkerGetter record) {
        yield return record;
    }

    //No Idles
    private static bool TestThreeWords(IIdleMarkerGetter idleMarker, List<string> list) {
        return idleMarker.Animations != null && idleMarker.Animations.Any();
    }
}