﻿using System.Collections.Generic;
using System.IO;
using Mutagen.Bethesda.Skyrim;
using SkyrimUnitTests.TestSuite.Tester.Asset.Nif;
namespace SkyrimUnitTests.TestSuite.Tester.Record; 

public class DoorRecordTest : RecordTest<IDoor, IDoorGetter, IDoorGetter> {
    public override string Name => "Door";
    public override List<RecordBug<IDoor, IDoorGetter>> Bugs { get; } = [
        new RecordBug<IDoor, IDoorGetter>("No Door Sounds", "Doors should have open/close sound",
            BugSeverity.Inconsistency, true, TestSounds)
    ];

    protected override IEnumerable<IDoorGetter> GetTestRelevantRecords(IDoorGetter record) {
        yield return record;
    }

    //Door Sounds
    private static bool TestSounds(IDoorGetter door, List<string> list) {
        var file = door.Model?.File.DataRelativePath;

        var noOpen = door.OpenSound.IsNull && (file == null || NifHelper.HasTextKey(file, "Open", "Sound:"));
        var noClose = door.CloseSound.IsNull && (file == null || NifHelper.HasTextKey(file, "Close", "Sound:"));

        return !noOpen && !noClose;
    }
}