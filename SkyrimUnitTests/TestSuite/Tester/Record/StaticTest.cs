﻿using System.Collections.Generic;
using Mutagen.Bethesda.Skyrim;
namespace SkyrimUnitTests.TestSuite.Tester.Record; 

public class StaticRecordTest : RecordTest<IStatic, IStaticGetter, IStaticGetter> {
    public override string Name => "Static";
    public override List<RecordBug<IStatic, IStaticGetter>> Bugs { get; } = [
        new RecordBug<IStatic, IStaticGetter>("Missing LOD", "Needs LOD entries when LOD flag is enabled",
            BugSeverity.Warning, true, TestMissingLOD)
    ];

    protected override IEnumerable<IStaticGetter> GetTestRelevantRecords(IStaticGetter record) {
        yield return record;
    }

    //Missing LOD
    private static bool TestMissingLOD(IStaticGetter @static, List<string> list) {
        return (@static.MajorFlags & Static.MajorFlag.HasDistantLOD) == 0 || @static.Lod != null;
    }
}