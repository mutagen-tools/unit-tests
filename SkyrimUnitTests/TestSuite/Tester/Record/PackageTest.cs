﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Mutagen.Bethesda.Skyrim;
namespace SkyrimUnitTests.TestSuite.Tester.Record;

public class PackageTest : RecordTest<IPackage, IPackageGetter, IPackageGetter> {
    public override string Name => "Package";
    public override List<RecordBug<IPackage, IPackageGetter>> Bugs { get; } = [
        new RecordBug<IPackage, IPackageGetter>("Inconsistent Timeframe", "The timeframe in the package data and the editor id should be the same",
            BugSeverity.Inconsistency, true, TestInconsistentTimeframe, PatchInconsistentTimeframe),
    ];

    protected override IEnumerable<IPackageGetter> GetTestRelevantRecords(IPackageGetter record) {
        yield return record;
    }

    private static readonly Regex TimeframeRegex = new(@"[^\d\s]*(\d+)x(\d+)");

    //Inconsistent Timeframe
    private static bool TryGetEditorIDTimeframe(IPackageGetter package, out (int hour, int duration) duration) {
        if (package.EditorID == null) {
            duration = (0, 0);
            return false;
        }

        var match = TimeframeRegex.Match(package.EditorID);
        if (!match.Success || match.Groups.Count < 3) {
            duration = (0, 0);
            return false;
        }

        var startingHour = Convert.ToInt32(match.Groups[1].Value);
        var durationHour = Convert.ToInt32(match.Groups[2].Value);
        
        duration = (startingHour, durationHour);
        return true;
    }
    
    private static bool TestInconsistentTimeframe(IPackageGetter package, List<string> list) {
        if (!TryGetEditorIDTimeframe(package, out var timeframe)) return true;

        if (package.ScheduleHour == timeframe.hour % 24
         && package.ScheduleDurationInMinutes / 60 == timeframe.duration) return true;

        if (package.ScheduleHour == -1 && package.ScheduleDurationInMinutes == 0
            && timeframe.hour == 0 && timeframe.duration % 24 == 0) return true;

        list.Add(package.ScheduleHour < 0
            ? $"The package runs all day, but should be {timeframe.hour}x{timeframe.duration}" 
            : $"Actually {package.ScheduleHour}x{package.ScheduleDurationInMinutes / 60}, but should be {timeframe.hour}x{timeframe.duration}");

        return false;
    }

    private static void PatchInconsistentTimeframe(IPackage package, ISkyrimMod patchFile) {
        if (!TryGetEditorIDTimeframe(package, out var timeframe)) return;

        package.ScheduleHour = (sbyte) timeframe.hour;
        package.ScheduleDurationInMinutes = timeframe.duration * 60;
    }
}
