﻿using System.Collections.Generic;
using Mutagen.Bethesda.Skyrim;
namespace SkyrimUnitTests.TestSuite.Tester.Record; 

public class MineOreRefRecordTest : RecordTest<IPlacedObject, IPlacedObjectGetter, IPlacedObjectGetter> {
    public override string Name => "Mine Ore Ref";
    public override List<RecordBug<IPlacedObject, IPlacedObjectGetter>> Bugs { get; } = [
        new RecordBug<IPlacedObject, IPlacedObjectGetter>("Furniture Linked", "Mining furniture linked",
            BugSeverity.Error, true, TestFurnitureLinked)
    ];

    protected override IEnumerable<IPlacedObjectGetter> GetTestRelevantRecords(IPlacedObjectGetter record) {
        if (record.IsDeleted) yield break;
        if (!IRecordTest.LinkCache.TryResolve<IActivatorGetter>(record.Base.FormKey, out var mine)) yield break;
        if (mine.EditorID != null && (mine.EditorID.Contains("MineOre") || mine.EditorID.Contains("MineGem"))) yield return record;
    }

    //Furniture Linked
    private static bool TestFurnitureLinked(IPlacedObjectGetter placedObject, List<string> list) {
        foreach (var linkedRef in placedObject.LinkedReferences) {
            if (!IRecordTest.LinkCache.TryResolve<IPlacedObjectGetter>(linkedRef.Reference.FormKey, out var linkedObject)) continue;
            if (!IRecordTest.LinkCache.TryResolve<IFurnitureGetter>(linkedObject.Base.FormKey, out var furniture)) continue;

            if (furniture.EditorID is "PickaxeMiningTableMarker" or "PickaxeMiningFloorMarker" or "PickaxeMiningWallMarker" or "PickaxeMiningTableMarkerNonPlayer") return true;
        }

        return false;
    }
}