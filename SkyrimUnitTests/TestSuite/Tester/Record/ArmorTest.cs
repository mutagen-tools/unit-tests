﻿using System.Collections.Generic;
using System.Linq;
using Mutagen.Bethesda.FormKeys.SkyrimSE;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Skyrim;
using Noggog;
namespace SkyrimUnitTests.TestSuite.Tester.Record;

public class ArmorRecordTest : RecordTest<IArmor, IArmorGetter, IArmorGetter> {
    public override string Name => "Armor";
    public override List<RecordBug<IArmor, IArmorGetter>> Bugs { get; } = [
        new RecordBug<IArmor, IArmorGetter>("Missing Vendor Keyword", "Should have VendorItemWeapon keyword",
            BugSeverity.Inconsistency, true, TestVendorKeyword, PatchVendorKeyword),
        new RecordBug<IArmor, IArmorGetter>("Missing Race Addons", "Playable armor should have armor addons for all default player races",
            BugSeverity.Warning, true, TestMissingRaceAddons),
        new RecordBug<IArmor, IArmorGetter>("Keyword/Slots don't match", "Armor keyword should match biped object slot",
            BugSeverity.Inconsistency, true, TestKeywordSlots, PatchKeywordSlots),
        new RecordBug<IArmor, IArmorGetter>("Keyword/ArmorType don't match", "Armor keyword should match the armor type",
            BugSeverity.Inconsistency, true, TestKeywordArmorType, PatchKeywordArmorType),
        new RecordBug<IArmor, IArmorGetter>("Missing Models", "Should 1st Person model for gauntlets",
            BugSeverity.Error, true, TestMissingModels),
        new RecordBug<IArmor, IArmorGetter>("Footstep/ArmorType don't match", "Footsteps should match the armor type",
            BugSeverity.Inconsistency, true, TestFootstepArmorType)
    ];
    
    protected override IEnumerable<IArmorGetter> GetTestRelevantRecords(IArmorGetter record) {
        yield return record;
    }

    private static readonly List<IFormLinkGetter<IRaceGetter>> DefaultPlayerRaces = new() {
        Skyrim.Race.ArgonianRace, Skyrim.Race.ArgonianRaceVampire,
        Skyrim.Race.BretonRace, Skyrim.Race.BretonRaceVampire,
        Skyrim.Race.DarkElfRace, Skyrim.Race.DarkElfRaceVampire,
        Skyrim.Race.HighElfRace, Skyrim.Race.HighElfRaceVampire,
        Skyrim.Race.ImperialRace, Skyrim.Race.ImperialRaceVampire,
        Skyrim.Race.KhajiitRace, Skyrim.Race.KhajiitRaceVampire,
        Skyrim.Race.NordRace, Skyrim.Race.NordRaceVampire,
        Skyrim.Race.OrcRace, Skyrim.Race.OrcRaceVampire,
        Skyrim.Race.RedguardRace, Skyrim.Race.RedguardRaceVampire,
        Skyrim.Race.WoodElfRace, Skyrim.Race.WoodElfRaceVampire,
    };

    //Missing Vendor Keyword
    private static readonly List<FormLink<IKeywordGetter>> ArmorTypeKeywords = new() {
        Skyrim.Keyword.VendorItemArmor,
        Skyrim.Keyword.VendorItemJewelry,
        Skyrim.Keyword.VendorItemClothing
    };
    
    private static bool TestVendorKeyword(IArmorGetter armor, List<string> list) {
        if (!armor.TemplateArmor.IsNull || armor.Keywords == null || (armor.MajorFlags & Armor.MajorFlag.NonPlayable) != 0) return true;

        if (armor.Keywords.Contains(Skyrim.Keyword.VendorItemDaedricArtifact) || armor.Keywords.Contains(Skyrim.Keyword.VendorNoSale)) return true;

        if (armor.BodyTemplate?.ArmorType is ArmorType.Clothing or ArmorType.HeavyArmor or ArmorType.LightArmor
         && armor.Keywords.Contains(Skyrim.Keyword.VendorItemArmor)) return true;
        
        
        return armor.BodyTemplate?.ArmorType switch {
            ArmorType.Clothing or ArmorType.LightArmor when 
                armor.Keywords.Contains(Skyrim.Keyword.ClothingCirclet)
             || armor.Keywords.Contains(Skyrim.Keyword.ClothingNecklace)
             || armor.Keywords.Contains(Skyrim.Keyword.ClothingRing)
             || armor.Keywords.Contains(Skyrim.Keyword.ArmorJewelry)
             || armor.Keywords.Contains(Skyrim.Keyword.JewelryExpensive) => armor.Keywords.Contains(Skyrim.Keyword.VendorItemJewelry),
            ArmorType.Clothing => armor.Keywords.Contains(Skyrim.Keyword.VendorItemClothing) || armor.Keywords.Contains(Skyrim.Keyword.VendorItemJewelry),
            _ => true
        };
    }
    
    private static void PatchVendorKeyword(IArmor armor, ISkyrimMod patchFile) {
        if (armor.BodyTemplate == null) return;
        
        armor.Keywords ??= new ExtendedList<IFormLinkGetter<IKeywordGetter>>();
        armor.Keywords.RemoveWhere(k => ArmorTypeKeywords.Contains(k));
        switch (armor.BodyTemplate.ArmorType) {
            case ArmorType.LightArmor:
            case ArmorType.HeavyArmor:
                armor.Keywords.Add(Skyrim.Keyword.VendorItemArmor);
                break;
            case ArmorType.Clothing:
                if (armor.Keywords.Contains(Skyrim.Keyword.ClothingCirclet)
                 || armor.Keywords.Contains(Skyrim.Keyword.ClothingNecklace)
                 || armor.Keywords.Contains(Skyrim.Keyword.ClothingRing)) {
                    armor.Keywords.Add(Skyrim.Keyword.VendorItemJewelry);
                } else {
                    armor.Keywords.Add(Skyrim.Keyword.VendorItemClothing);
                }
                break;
        }
    }
    
    //Missing Race Addons
    private static bool TestMissingRaceAddons(IArmorGetter armor, List<string> list) {
        if (!armor.TemplateArmor.IsNull || (armor.MajorFlags & Armor.MajorFlag.NonPlayable) != 0) return true;

        //Exclude non player race armor
        if (armor.Race.FormKey != Skyrim.Race.DefaultRace.FormKey && !DefaultPlayerRaces.Select(r => r.FormKey).Contains(armor.Race.FormKey)) {
            return true;
        }
        
        //Build list of all races in armor addons
        var missingRaces = new List<IFormLinkGetter<IRaceGetter>>(DefaultPlayerRaces);
        foreach (var armorAddon in armor.Armature) {
            var addon = armorAddon.TryResolve(IRecordTest.LinkCache);
            if (addon == null) continue;
            
            foreach (var additionalRace in addon.AdditionalRaces) {
                missingRaces.Remove(additionalRace);
            }
            missingRaces.Remove(addon.Race);
        }
        
        foreach (var race in missingRaces) {
            var r = race.TryResolve(IRecordTest.LinkCache);

            if (r?.EditorID != null) list.Add(r.EditorID);
        }

        return missingRaces.Count == 0;
    }
    
    //Keyword/Slots don't match
    private static bool TestKeywordSlots(IArmorGetter armor, List<string> list) {
        if (!armor.TemplateArmor.IsNull || armor.BodyTemplate == null || armor.Keywords == null) return true;
        
        var isArmor = armor.BodyTemplate.ArmorType is ArmorType.HeavyArmor or ArmorType.LightArmor;
        var isClothing = armor.BodyTemplate.ArmorType is ArmorType.Clothing;

        if ((armor.BodyTemplate.FirstPersonFlags & BipedObjectFlag.Body) != 0) {
            if (isArmor) return armor.Keywords.Contains(Skyrim.Keyword.ArmorCuirass);
            if (isClothing) return armor.Keywords.Contains(Skyrim.Keyword.ClothingBody);
        }

        if ((armor.BodyTemplate.FirstPersonFlags & BipedObjectFlag.Feet) != 0) {
            if (isArmor) return armor.Keywords.Contains(Skyrim.Keyword.ArmorBoots);
            if (isClothing) return armor.Keywords.Contains(Skyrim.Keyword.ClothingFeet);
        }
        
        if ((armor.BodyTemplate.FirstPersonFlags & BipedObjectFlag.Hands) != 0) {
            if (isArmor) return armor.Keywords.Contains(Skyrim.Keyword.ArmorGauntlets);
            if (isClothing) return armor.Keywords.Contains(Skyrim.Keyword.ClothingHands);
        }
        
        if ((armor.BodyTemplate.FirstPersonFlags & (BipedObjectFlag.Head | BipedObjectFlag.Hair)) != 0) {
            if (isArmor) return armor.Keywords.Contains(Skyrim.Keyword.ArmorHelmet);
            if (isClothing) return armor.Keywords.Contains(Skyrim.Keyword.ClothingHead);
        }
        
        if ((armor.BodyTemplate.FirstPersonFlags & BipedObjectFlag.Shield) != 0) return armor.Keywords.Contains(Skyrim.Keyword.ArmorShield);
        if ((armor.BodyTemplate.FirstPersonFlags & BipedObjectFlag.Circlet) != 0) return armor.Keywords.Contains(Skyrim.Keyword.ClothingCirclet);
        if ((armor.BodyTemplate.FirstPersonFlags & BipedObjectFlag.Amulet) != 0) return armor.Keywords.Contains(Skyrim.Keyword.ClothingNecklace);
        if ((armor.BodyTemplate.FirstPersonFlags & BipedObjectFlag.Ring) != 0) return armor.Keywords.Contains(Skyrim.Keyword.ClothingRing);

        return true;
    }
    
    private static void PatchKeywordSlots(IArmor armor, ISkyrimMod patchFile) {
        if (armor.BodyTemplate == null || armor.Keywords == null) return;
        armor.Keywords ??= new ExtendedList<IFormLinkGetter<IKeywordGetter>>();

        var isArmor = armor.BodyTemplate.ArmorType is ArmorType.HeavyArmor or ArmorType.LightArmor;
        var isClothing = armor.BodyTemplate.ArmorType is ArmorType.Clothing;
        
        if ((armor.BodyTemplate.FirstPersonFlags & BipedObjectFlag.Body) != 0) {
            if (isArmor) armor.Keywords.Add(Skyrim.Keyword.ArmorCuirass);
            else if (isClothing) armor.Keywords.Add(Skyrim.Keyword.ClothingBody);
        } else if ((armor.BodyTemplate.FirstPersonFlags & BipedObjectFlag.Feet) != 0) {
            if (isArmor) armor.Keywords.Add(Skyrim.Keyword.ArmorBoots);
            else if (isClothing) armor.Keywords.Add(Skyrim.Keyword.ClothingFeet);
        } else if ((armor.BodyTemplate.FirstPersonFlags & BipedObjectFlag.Hands) != 0) {
            if (isArmor) armor.Keywords.Add(Skyrim.Keyword.ArmorGauntlets);
            else if (isClothing) armor.Keywords.Add(Skyrim.Keyword.ClothingHands);
        } else if ((armor.BodyTemplate.FirstPersonFlags & (BipedObjectFlag.Head | BipedObjectFlag.Hair)) != 0) {
            if (isArmor) armor.Keywords.Add(Skyrim.Keyword.ArmorHelmet);
            else if (isClothing) armor.Keywords.Add(Skyrim.Keyword.ClothingHead);
        } else if ((armor.BodyTemplate.FirstPersonFlags & BipedObjectFlag.Shield) != 0) {
            armor.Keywords.Add(Skyrim.Keyword.ArmorShield);
        } else if ((armor.BodyTemplate.FirstPersonFlags & BipedObjectFlag.Circlet) != 0) {
            armor.Keywords.Add(Skyrim.Keyword.ClothingCirclet);
        } else if ((armor.BodyTemplate.FirstPersonFlags & BipedObjectFlag.Amulet) != 0) {
            armor.Keywords.Add(Skyrim.Keyword.ClothingNecklace);
        } else if ((armor.BodyTemplate.FirstPersonFlags & BipedObjectFlag.Ring) != 0) {
            armor.Keywords.Add(Skyrim.Keyword.ClothingRing);
        }
    }
    
    //Keyword/Armor Type don't match
    private static readonly List<FormLink<IKeywordGetter>> ArmorTypes = new() {
        Skyrim.Keyword.ArmorLight,
        Skyrim.Keyword.ArmorHeavy,
        Skyrim.Keyword.ArmorClothing,
        Skyrim.Keyword.ArmorJewelry,
    }; 
    
    private static bool TestKeywordArmorType(IArmorGetter armor, List<string> list) {
        if (!armor.TemplateArmor.IsNull || armor.BodyTemplate == null || armor.Keywords == null) return true;
        if ((armor.BodyTemplate.FirstPersonFlags & BipedObjectFlag.Shield) != 0) return true;

        return armor.BodyTemplate.ArmorType switch {
            ArmorType.LightArmor => armor.Keywords.Contains(Skyrim.Keyword.ArmorLight),
            ArmorType.HeavyArmor => armor.Keywords.Contains(Skyrim.Keyword.ArmorHeavy),
            ArmorType.Clothing => armor.Keywords.Contains(Skyrim.Keyword.ArmorClothing) || armor.Keywords.Contains(Skyrim.Keyword.ArmorJewelry),
            _ => true
        };
    }
    
    private static void PatchKeywordArmorType(IArmor armor, ISkyrimMod patchFile) {
        if (armor.BodyTemplate == null || armor.Keywords == null) return;
        armor.Keywords ??= new ExtendedList<IFormLinkGetter<IKeywordGetter>>();
        
        foreach (var armorType in ArmorTypes) armor.Keywords.Remove(armorType);
        
        switch (armor.BodyTemplate.ArmorType) {
            case ArmorType.LightArmor:
                armor.Keywords.Add(Skyrim.Keyword.ArmorLight);
                break;
            case ArmorType.HeavyArmor:
                armor.Keywords.Add(Skyrim.Keyword.ArmorHeavy);
                break;
            case ArmorType.Clothing:
                if (armor.Keywords.Contains(Skyrim.Keyword.ClothingCirclet) || armor.Keywords.Contains(Skyrim.Keyword.ClothingRing) || armor.Keywords.Contains(Skyrim.Keyword.ClothingNecklace)) {
                    armor.Keywords.Add(Skyrim.Keyword.ArmorJewelry);
                } else {
                    armor.Keywords.Add(Skyrim.Keyword.ArmorClothing);
                }
                
                break;
        }
    }
    
    //Missing Models
    private static bool TestMissingModels(IArmorGetter armor, List<string> list) {
        if (!armor.TemplateArmor.IsNull || armor.BodyTemplate == null || (armor.BodyTemplate.FirstPersonFlags & (BipedObjectFlag.Hands | BipedObjectFlag.Body)) == 0) return true; 
        
        foreach (var armorAddon in armor.Armature) {
            var addon = armorAddon.TryResolve(IRecordTest.LinkCache);
            if (addon == null) continue;
            
            if (addon.FirstPersonModel?.Male == null || !addon.FirstPersonModel.Male.File.IsNullOrEmpty()) return true;
        }

        return false;
    }
    
    //Footstep/ArmorType don't match
    private static bool TestFootstepArmorType(IArmorGetter armor, List<string> list) {
        if (!armor.TemplateArmor.IsNull || armor.BodyTemplate == null || (armor.BodyTemplate.FirstPersonFlags & BipedObjectFlag.Feet) == 0) return true;
        
        foreach (var armorAddon in armor.Armature) {
            var addon = armorAddon.TryResolve(IRecordTest.LinkCache);
            if (addon == null || addon.FootstepSound.IsNull) continue;
            
            if (addon.FootstepSound.FormKey == Skyrim.FootstepSet.FSTArmorLightFootstepSet.FormKey) {
                return armor.BodyTemplate.ArmorType == ArmorType.LightArmor;
            }
            
            if (addon.FootstepSound.FormKey == Skyrim.FootstepSet.FSTArmorHeavyFootstepSet.FormKey) {
                return armor.BodyTemplate.ArmorType == ArmorType.HeavyArmor;
            }
            
            if (addon.FootstepSound.FormKey == Skyrim.FootstepSet.FSTBarefootFootstepSet.FormKey) {
                return armor.BodyTemplate.ArmorType == ArmorType.Clothing;
            }

            return true;
        }

        return true;
    }
}