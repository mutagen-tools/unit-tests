﻿using System.Collections.Generic;
using System.Linq;
using Mutagen.Bethesda.FormKeys.SkyrimSE;
using Mutagen.Bethesda.Skyrim;
namespace SkyrimUnitTests.TestSuite.Tester.Record; 

public class LocationRecordTest : RecordTest<ILocation, ILocationGetter, ILocationGetter> {
    public override string Name => "Location";
    public override List<RecordBug<ILocation, ILocationGetter>> Bugs { get; } = [
        new RecordBug<ILocation, ILocationGetter>("No Parent Location", "Not always a bug but most locations should have a parent location - only provinces, debug cells or magical places should be a top level location",
            BugSeverity.Inconsistency, true, TestHasParentLocation),
        new RecordBug<ILocation, ILocationGetter>("No HouseContainerRefType LocTypeRef", "Settlement houses should usually have one HouseContainerRefType in every location",
            BugSeverity.Inconsistency, true, TestAnyHouseContainerRefType),
        new RecordBug<ILocation, ILocationGetter>("No Boss and BossContainer in Dungeon", "Dungeons should usually have a boss or boss container",
            BugSeverity.Inconsistency, true, TestDungeonBoss)
    ];

    protected override IEnumerable<ILocationGetter> GetTestRelevantRecords(ILocationGetter record) {
        yield return record;
    }

    //Has Parent Location
    private static bool TestHasParentLocation(ILocationGetter location, List<string> list) {
        return !location.ParentLocation.IsNull;
    }
    
    //No HouseContainerRefType LocTypeRef
    private static bool TestAnyHouseContainerRefType(ILocationGetter location, List<string> list) {
        if (!location.IsSettlementLocation()) return true;
        if (!location.IsSettlementHouseLocationNotPlayerHome()) return true;
        if (!location.IsLocationAppliedToInterior()) return true;

        IEnumerable<ILocationCellStaticReferenceGetter> refs;
        // Which of these is used depends on either master/non-master plugin (?) or defining plugin and override plugin
        // Should be abstracted in extension function
        if (location.LocationCellStaticReferences is null) {
            if (location.ActorCellStaticReferences is null) return false;

            refs = location.ActorCellStaticReferences;
        } else {
            refs = location.LocationCellStaticReferences;
            if (location.ActorCellStaticReferences is not null) {
                refs = refs.Concat(location.ActorCellStaticReferences);
            }
        }

        return refs.Any(staticRef =>
            staticRef.LocationRefType.FormKey == Skyrim.LocationReferenceType.HouseContainerRefType.FormKey
            || staticRef.LocationRefType.FormKey == Skyrim.LocationReferenceType.TGRWealthMarker01.FormKey
            || staticRef.LocationRefType.FormKey == Skyrim.LocationReferenceType.TGRWealthMarker02.FormKey
            || staticRef.LocationRefType.FormKey == Skyrim.LocationReferenceType.TGRWealthMarker03.FormKey
            || staticRef.LocationRefType.FormKey == Skyrim.LocationReferenceType.TGRWealthyHomeChest.FormKey
            || staticRef.LocationRefType.FormKey == Skyrim.LocationReferenceType.TGRLedger.FormKey
            || staticRef.LocationRefType.FormKey == Skyrim.LocationReferenceType.TGRSLStrongbox.FormKey);
    }

    //No Boss and BossContainer in Dungeon
    private static bool TestDungeonBoss(ILocationGetter location, List<string> list) {
        if (location.Keywords == null || location.Keywords.All(k => k.FormKey != Skyrim.Keyword.LocTypeDungeon.FormKey)) return true;

        return location.LocationCellStaticReferences != null && location.LocationCellStaticReferences.Any(k =>
            k.LocationRefType.FormKey == Skyrim.LocationReferenceType.Boss.FormKey || k.LocationRefType.FormKey == Skyrim.LocationReferenceType.BossContainer.FormKey);
    }
}