﻿using System.Collections.Generic;
using Mutagen.Bethesda.Skyrim;
namespace SkyrimUnitTests.TestSuite.Tester.Record; 

public class FactionRecordTest : RecordTest<IFaction, IFactionGetter, IFactionGetter> {
    public override string Name => "Faction";
    public override List<RecordBug<IFaction, IFactionGetter>> Bugs { get; } = [
        new RecordBug<IFaction, IFactionGetter>("Vendor Hour order", "Start vendor hour needs to be smaller than end vendor hour",
            BugSeverity.Error, true, TestVendorHourOrder),
    ];

    protected override IEnumerable<IFactionGetter> GetTestRelevantRecords(IFactionGetter record) {
        yield return record;
    }

    //Vendor Hour order
    private static bool TestVendorHourOrder(IFactionGetter faction, List<string> list) {
        if (!faction.IsVendor()) return true;
        if (faction.VendorValues is null) return true;
        
        return faction.VendorValues.StartHour < faction.VendorValues.EndHour;
    }
}