﻿using System;
using System.Collections.Generic;
using Mutagen.Bethesda.Skyrim;
namespace SkyrimUnitTests.TestSuite.Tester.Record; 

public class DialogTopicRecordTest : RecordTest<IDialogTopic, IDialogTopicGetter, IDialogTopicGetter> {
    private const int DialogTopicLengthLimit = 80;
    private const int DefaultDialogTopicPriority = 50;

    public override string Name => "Dialog Topic";
    public override List<RecordBug<IDialogTopic, IDialogTopicGetter>> Bugs { get; } = [ 
        new RecordBug<IDialogTopic, IDialogTopicGetter>("Topic Too Long", $"Responses shouldn't be longer than {DialogTopicLengthLimit} characters",
            BugSeverity.Warning, true, TestResponseLength),
        new RecordBug<IDialogTopic, IDialogTopicGetter>("No Quest", "Topic isn't assigned to quest",
            BugSeverity.Error, true, TestNoQuest),
        new RecordBug<IDialogTopic, IDialogTopicGetter>("No Branch", "Topic isn't assigned to branch or it is a scene dialog",
            BugSeverity.Error, true, TestNoBranch),
        new RecordBug<IDialogTopic, IDialogTopicGetter>("Not Default Priority", "Topics that aren't the starting topic don't use their priority for anything. Remove to avoid confusion that this might have any meaning.",
            BugSeverity.Inconsistency, true, TestNotDefaultPriority, PatchNotDefaultPriority),
    ];

    protected override IEnumerable<IDialogTopicGetter> GetTestRelevantRecords(IDialogTopicGetter record) {
        yield return record;
    }

    //Topic Too Long
    private static bool TestResponseLength(IDialogTopicGetter dialogTopic, List<string> list) {
        return dialogTopic.Name?.String is not { Length: > DialogTopicLengthLimit };
    }
    
    //No Quest
    private static bool TestNoQuest(IDialogTopicGetter dialogTopic, List<string> list) {
        return !dialogTopic.Quest.IsNull;
    }
    
    //No Branch
    private static bool TestNoBranch(IDialogTopicGetter dialogTopic, List<string> list) {
        return dialogTopic.Subtype is not DialogTopic.SubtypeEnum.Rumors or DialogTopic.SubtypeEnum.ForceGreet or DialogTopic.SubtypeEnum.Custom || !dialogTopic.Branch.IsNull;
    }

    //Not Default Priority
    private static bool TestNotDefaultPriority(IDialogTopicGetter dialogTopic, List<string> list) {
        // Only check custom topics
        if (dialogTopic.Subtype != DialogTopic.SubtypeEnum.Custom) return true;

        // Skip if this topic is a starting topic
        var branch = dialogTopic.Branch.TryResolve(IRecordTest.LinkCache);
        if (branch is not null && branch.StartingTopic.FormKey == dialogTopic.FormKey) return true;

        return Math.Abs(dialogTopic.Priority - DefaultDialogTopicPriority) < 0.001;
    }

    private static void PatchNotDefaultPriority(IDialogTopic dialogTopic, ISkyrimMod mod) {
        dialogTopic.Priority = DefaultDialogTopicPriority;
    }
}