﻿using System.Collections.Generic;
using Mutagen.Bethesda.Skyrim;
namespace SkyrimUnitTests.TestSuite.Tester.Record; 

public class FloraRecordTest : RecordTest<IFlora, IFloraGetter, IFloraGetter> {
    public override string Name => "Flora";
    public override List<RecordBug<IFlora, IFloraGetter>> Bugs { get; } = [
        new RecordBug<IFlora, IFloraGetter>("No Flora Sounds", "Floras should have harvest sound",
            BugSeverity.Warning, true, TestSounds),
        new RecordBug<IFlora, IFloraGetter>("No Harvest Ingredient", "Floras should have harvestable ingredient",
            BugSeverity.Inconsistency, true, TestIngredient)
    ];

    protected override IEnumerable<IFloraGetter> GetTestRelevantRecords(IFloraGetter record) {
        yield return record;
    }

    //Flora Sounds
    private static bool TestSounds(IFloraGetter flora, List<string> list) {
        return !flora.HarvestSound.IsNull;
    }
    
    //Harvest Ingredient
    private static bool TestIngredient(IFloraGetter flora, List<string> list) {
        return !flora.Ingredient.IsNull;
    }
}