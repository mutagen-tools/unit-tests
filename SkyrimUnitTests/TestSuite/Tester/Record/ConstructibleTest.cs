﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using Mutagen.Bethesda.FormKeys.SkyrimSE;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Skyrim;
namespace SkyrimUnitTests.TestSuite.Tester.Record; 

public class ConstructibleRecordTest : RecordTest<IConstructibleObject, IConstructibleObjectGetter, IConstructibleObjectGetter> {
    public override string Name => "ConstructibleObject";
    public override List<RecordBug<IConstructibleObject, IConstructibleObjectGetter>> Bugs { get; } = [
        new RecordBug<IConstructibleObject, IConstructibleObjectGetter>("Duplicate Recipe", "Duplicated recipes should be merged or removed",
            BugSeverity.Warning, true, TestDuplicateRecipe),
        new RecordBug<IConstructibleObject, IConstructibleObjectGetter>("Missing Created Object", "Constructibles need to create something",
            BugSeverity.Error, true, TestMissingCreatedObject),
        new RecordBug<IConstructibleObject, IConstructibleObjectGetter>("Missing Conditions for Enchanted Tempering", "Temper recipes need to have EPTemperingItemIsEnchanted = 0 or ArcaneBlacksmith perk conditions",
            BugSeverity.Inconsistency, true, TestMissingPerkEnchantedWeaponArmor)
    ];

    protected override IEnumerable<IConstructibleObjectGetter> GetTestRelevantRecords(IConstructibleObjectGetter record) {
        yield return record;
    }

    //Duplicate Recipe
    private record Constructible(IReadOnlyList<IConditionGetter> Conditions, IReadOnlyList<IContainerEntryGetter>? Items, IFormLinkNullableGetter<IConstructibleGetter> CreatedObject, IFormLinkNullableGetter<IKeywordGetter> WorkbenchKeyword, ushort? CreatedObjectCount) {
        public virtual bool Equals(Constructible? other) {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return Conditions.Equals(other.Conditions) && Equals(Items, other.Items) && CreatedObject.Equals(other.CreatedObject) && WorkbenchKeyword.Equals(other.WorkbenchKeyword) && CreatedObjectCount == other.CreatedObjectCount;
        }
        
        public override int GetHashCode() {
            return HashCode.Combine(Conditions, Items, CreatedObject, WorkbenchKeyword, CreatedObjectCount);
        }
    }
    
    private static readonly ConcurrentDictionary<Constructible, List<FormKey>> DuplicateRecipes = new();
    
    private static bool TestDuplicateRecipe(IConstructibleObjectGetter constructibleObject, List<string> list) {
        if (constructibleObject.CreatedObject.IsNull) return true;

        var c = new Constructible(constructibleObject.Conditions, constructibleObject.Items, constructibleObject.CreatedObject, constructibleObject.WorkbenchKeyword, constructibleObject.CreatedObjectCount);
        if (DuplicateRecipes.TryGetValue(c, out var formKeys)) {
            var sb = new StringBuilder();
            foreach (var formKey in formKeys) sb.AppendLine(formKey.ToString());
            list.Add(sb.ToString());
            formKeys.Add(constructibleObject.FormKey);
            
            return false;
        }
        
        DuplicateRecipes.TryAdd(c, [constructibleObject.FormKey]);
        return true;
    }
    
    //Missing Created Object
    private static bool TestMissingCreatedObject(IConstructibleObjectGetter constructibleObject, List<string> list) {
        return !constructibleObject.CreatedObject.IsNull;
    }
    
    //Missing Perk for Enchanted Weapon/Armor
    private static bool TestMissingPerkEnchantedWeaponArmor(IConstructibleObjectGetter constructibleObject, List<string> list) {
        if (constructibleObject.WorkbenchKeyword.FormKey != Skyrim.Keyword.CraftingSmithingSharpeningWheel.FormKey && constructibleObject.WorkbenchKeyword.FormKey != Skyrim.Keyword.CraftingSmithingArmorTable.FormKey) return true;

        var perkCondition = false;
        var isEnchanted = false;
        foreach (var condition in constructibleObject.Conditions) {
            switch (condition.Data) {
                case IEPTemperingItemIsEnchantedConditionDataGetter: {
                    isEnchanted = true;
                    if (perkCondition) return true;

                    break;
                }
                case IHasPerkConditionDataGetter hasPerk when hasPerk.Perk.Link.FormKey == Skyrim.Perk.ArcaneBlacksmith.FormKey: {
                    perkCondition = true;
                    if (isEnchanted) return true;

                    break;
                }
            }

            if ((condition.Flags & Condition.Flag.OR) == 0) {
                perkCondition = isEnchanted = false;
            }
        }

        return false;
    }
}