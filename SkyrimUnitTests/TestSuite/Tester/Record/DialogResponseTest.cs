﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mutagen.Bethesda.FormKeys.SkyrimSE;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Skyrim;
namespace SkyrimUnitTests.TestSuite.Tester.Record; 

public class DialogResponseRecordTest : RecordTest<IDialogResponses, IDialogResponsesGetter, IDialogResponsesGetter> {
    private const int DialogResponseLengthLimit = 149;
    private static readonly FormLink<ISkyrimMajorRecordGetter> AmuletOfArticulationFormList = new(Skyrim.FormList.TGAmuletofArticulationList.FormKey);
    private static readonly Condition AmuletCondition;

    static DialogResponseRecordTest() {
        var data = new GetEquippedConditionData {
            RunOnType = Condition.RunOnType.Reference,
            Reference = Skyrim.PlayerRef,
        };
        data.ItemOrList.Link.SetTo(AmuletOfArticulationFormList.FormKey);
        AmuletCondition = new ConditionFloat {
            Data = data,
            CompareOperator = CompareOperator.EqualTo,
            ComparisonValue = 1
        };
    }

    public override string Name => "Dialog Response";
    public override List<RecordBug<IDialogResponses, IDialogResponsesGetter>> Bugs { get; } = [
        new RecordBug<IDialogResponses, IDialogResponsesGetter>("Amulet of Articulation",
            "Persuasion speech checks should always let you auto pass with Amulet of Articulation equipped",
            BugSeverity.Inconsistency, true, TestAmulet, PatchAmulet),
        new RecordBug<IDialogResponses, IDialogResponsesGetter>("Non-Player Skill Check", "Skill checks that are not checked on the player",
            BugSeverity.Warning, true, TestNonPlayerSkillCheck, PatchNonPlayerSkillCheck),
        new RecordBug<IDialogResponses, IDialogResponsesGetter>("Non-Global Skill Check", "Skill checks doesn't use global to evaluate skill check",
            BugSeverity.Inconsistency, true, TestNonGlobalSkillCheck),
        new RecordBug<IDialogResponses, IDialogResponsesGetter>("Not Trimmed", "Whitespaces at the beginning or end of lines",
            BugSeverity.Warning, true, TestNotTrimmed, PatchNotTrimmed),
        new RecordBug<IDialogResponses, IDialogResponsesGetter>("Invalid Char", "Chars that Skyrim can't represent",
            BugSeverity.Error, true, TestInvalidChar, PatchInvalidChar),
        new RecordBug<IDialogResponses, IDialogResponsesGetter>("Response Too Long", $"Responses shouldn't be longer than {DialogResponseLengthLimit} characters",
            BugSeverity.Warning, true, TestResponseLength),
        new RecordBug<IDialogResponses, IDialogResponsesGetter>("Circular Previous Dialog", "Circular previous dialog",
            BugSeverity.Warning, true, TestCircularPreviousDialog),
        new RecordBug<IDialogResponses, IDialogResponsesGetter>("Invalid SayOnce", "SayOnce is not working for quest that are not Start Game Enabled",
            BugSeverity.Warning, true, TestInvalidSayOnce),
        new RecordBug<IDialogResponses, IDialogResponsesGetter>("Remaining Implementation Notes", "Implementation notes should not remain in the dialog and be implemented properly",
            BugSeverity.Warning, true, TestImplementationNotes),
    ];
    

    protected override IEnumerable<IDialogResponsesGetter> GetTestRelevantRecords(IDialogResponsesGetter record) {
        yield return record;
    }

    //Amulet of Articulation
    private static bool TestAmulet(IDialogResponsesGetter dialogResponses, List<string> list) {
        var isPersuade = false;
        var hasAmuletOfArticulation = false;
        foreach (var conditionGetter in dialogResponses.Conditions) {
            switch (conditionGetter.Data) {
                case IGetActorValueConditionDataGetter { ActorValue: ActorValue.Speech }:
                    isPersuade = true;
                    break;
                case IGetEquippedConditionDataGetter { RunOnType: Condition.RunOnType.Reference, } getEquipped
                    when getEquipped.Reference.Equals(Skyrim.PlayerRef)
                 && getEquipped.ItemOrList.Link.FormKey.Equals(AmuletOfArticulationFormList.FormKey)
                 && conditionGetter.CompareOperator == CompareOperator.EqualTo
                 && Math.Abs(((IConditionFloatGetter) conditionGetter).ComparisonValue - 1) < float.Epsilon:
                    hasAmuletOfArticulation = true;
                    break;
            }
        }

        return !isPersuade || hasAmuletOfArticulation;
    }

    private static void PatchAmulet(IDialogResponses dialogResponses, ISkyrimMod patchFile) {
        for (var i = 0; i < dialogResponses.Conditions.Count; i++) {
            var conditionData = dialogResponses.Conditions[i].Data;
            if (conditionData is not IGetActorValueConditionDataGetter { ActorValue: ActorValue.Speech }) continue;

            //Set persuasion condition to or
            var or = (dialogResponses.Conditions[i].Flags & Condition.Flag.OR) != 0;
            dialogResponses.Conditions[i].Flags |= Condition.Flag.OR;

            //Add amulet condition, if persuasion had or flag, add it to amulet condition
            dialogResponses.Conditions.Insert(i + 1, AmuletCondition);
            if (or) {
                dialogResponses.Conditions[i + 1].Flags |= Condition.Flag.OR;
            }
            return;
        }
    }

    //Non-Player Skill Checks
    private static bool TestNonPlayerSkillCheck(IDialogResponsesGetter dialogResponses, List<string> list) {
        return !dialogResponses.Conditions
            .Any(c => c.Data is IGetActorValueConditionDataGetter getActorValue
             && (int) getActorValue.ActorValue is >= 6 and <= 23
             && !(getActorValue.RunOnType == Condition.RunOnType.Reference && Equals(getActorValue.Reference, Skyrim.PlayerRef))
             && getActorValue.RunOnType != Condition.RunOnType.Target);
    }

    private static void PatchNonPlayerSkillCheck(IDialogResponses dialogResponses, ISkyrimMod patchFile) {
        foreach (var condition in dialogResponses.Conditions
            .Where(c => c.Data is IGetActorValueConditionDataGetter getActorValue
             && getActorValue.ActorValue.IsSkill()
             && !(getActorValue.RunOnType == Condition.RunOnType.Reference && Equals(getActorValue.Reference, Skyrim.PlayerRef))
             && getActorValue.RunOnType != Condition.RunOnType.Target)) {
            condition.Data.RunOnType = Condition.RunOnType.Target;
            condition.Data.Reference.SetTo(Skyrim.PlayerRef.FormKey);
        }
    }

    //Non-Global Skill Checks
    private static bool TestNonGlobalSkillCheck(IDialogResponsesGetter dialogResponses, List<string> list) {
        foreach (var condition in dialogResponses.Conditions) {
            if (condition is not IConditionGlobalGetter
             && condition.Data is IGetActorValueConditionDataGetter getActorValue
             && getActorValue.ActorValue.IsSkill()
             && condition.Data.RunOnType == Condition.RunOnType.Reference
             && condition.Data.Reference.FormKey == Skyrim.PlayerRef.FormKey)
                return false;
        }

        return true;
    }

    // private static void PatchNonGlobalSkillCheck(IDialogResponses dialogResponses, ISkyrimMod patchFile) {
    //     foreach (var condition in dialogResponses.Conditions
    //         .Where(c => c is not IConditionGlobalGetter
    //          && c.Data is IGetActorValueConditionDataGetter getActorValue
    //          && getActorValue.ActorValue.IsSkill())) {
    //         
    //     }
    // }

    //Not Trimmed
    private static bool TestNotTrimmed(IDialogResponsesGetter dialogResponses, List<string> list) {
        return dialogResponses.Responses.Select(dialogResponse => dialogResponse.Text.String)
            .All(text => text != null && !text.StartsWith(' ') && !text.EndsWith(' '));
    }

    private static void PatchNotTrimmed(IDialogResponses dialogResponses, ISkyrimMod patchFile) {
        foreach (var responses in dialogResponses.Responses) {
            responses.Text.String = responses.Text.String?.Trim();
        }
    }

    //Invalid Char
    private static bool TestInvalidChar(IDialogResponsesGetter dialogResponses, List<string> list) {
        return dialogResponses.Responses.Select(dialogResponse => dialogResponse.Text.String)
            .All(text => !InvalidString.InvalidStringsOneLiner.Any(invalid => text != null && text.Contains(invalid.Key)));
    }

    private static void PatchInvalidChar(IDialogResponses dialogResponses, ISkyrimMod patchFile) {
        foreach (var responses in dialogResponses.Responses) {
            if (responses.Text.String is null) continue;

            foreach (var (invalid, replacement) in InvalidString.InvalidStringsOneLiner) {
                responses.Text.String = responses.Text.String.Replace(invalid, replacement);
            }
        }
    }

    //Response Too Long
    private static bool TestResponseLength(IDialogResponsesGetter dialogResponses, List<string> list) {
        return dialogResponses.Responses.All(response =>
            response.Text.String is not { Length: > DialogResponseLengthLimit });
    }

    //Circular Previous Dialogue
    private static bool TestCircularPreviousDialog(IDialogResponsesGetter dialogResponses, List<string> list) {
        var dialogCache = new HashSet<FormKey>();
            
        var previousDialog = dialogResponses.PreviousDialog.TryResolve(IRecordTest.LinkCache);
        while (previousDialog?.PreviousDialog != null) {
            if (!dialogCache.Add(previousDialog.FormKey)) {
                return false;
            }

            previousDialog = previousDialog.PreviousDialog.TryResolve(IRecordTest.LinkCache);
        }

        return true;
    }
    
    //Invalid SayOnce
    private static bool TestInvalidSayOnce(IDialogResponsesGetter dialogResponses, List<string> list) {
        if (dialogResponses.Flags == null || (dialogResponses.Flags.Flags & DialogResponses.Flag.SayOnce) == 0) return true;
        
        var context = IRecordTest.LinkCache.ResolveContext<IDialogResponses, IDialogResponsesGetter>(dialogResponses.FormKey);
        if (context.Parent?.Record is not IDialogTopicGetter topic) return true;

        var quest = topic.Quest.TryResolve(IRecordTest.LinkCache);
        return quest != null && (quest.Flags & Quest.Flag.StartGameEnabled) != 0;
    }

    //Remaining Implementation Notes
    private static bool TestImplementationNotes(IDialogResponsesGetter dialogResponses, List<string> list) {
        return dialogResponses.Responses.All(response => {
            if (response.Text.String is null) return true;

            return !response.Text.String.Contains('[') && !response.Text.String.Contains(']');
        });
    }
}