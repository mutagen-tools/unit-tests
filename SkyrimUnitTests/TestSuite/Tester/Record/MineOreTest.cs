﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mutagen.Bethesda;
using Mutagen.Bethesda.Skyrim;
namespace SkyrimUnitTests.TestSuite.Tester.Record; 

public class MineOreRecordTest : RecordTest<IActivator, IActivatorGetter, IActivatorGetter> {
    public override string Name => "Mine Ore";
    public override List<RecordBug<IActivator, IActivatorGetter>> Bugs { get; } = [
        new RecordBug<IActivator, IActivatorGetter>("No MineOreScript", "MineOres should have mine ore script attached",
            BugSeverity.Warning, true, TestScript),
        new RecordBug<IActivator, IActivatorGetter>("Correct Vein/Ore", "MineOres should have matching ores in their MineOre script",
            BugSeverity.Warning, true, TestVeinOre)
    ];
    
    private const StringComparison StringComparison = System.StringComparison.OrdinalIgnoreCase;

    protected override IEnumerable<IActivatorGetter> GetTestRelevantRecords(IActivatorGetter record) {
        if (record.EditorID != null && (record.EditorID.Contains("MineOre") || record.EditorID.Contains("MineGem"))) yield return record;
    }

    //No MineOreScript
    private static bool TestScript(IActivatorGetter activator, List<string> list) {
        return activator.VirtualMachineAdapter != null && activator.VirtualMachineAdapter.Scripts.Any(s => string.Equals(s.Name, "MineOreScript", StringComparison));
    }
    
    //Correct Vein/Ore
    private static bool TestVeinOre(IActivatorGetter activator, List<string> list) {
        if (activator.EditorID == null) return false;
        var script = activator.VirtualMachineAdapter?.Scripts.FirstOrDefault(s => string.Equals(s.Name, "MineOreScript", StringComparison));
        var oreProperty = script?.GetProperty<IScriptObjectPropertyGetter>("Ore");
        if (oreProperty == null) return false;
        
        foreach (var oreLink in oreProperty.EnumerateFormLinks()) {
            var ore = oreLink.TryResolve<IMiscItemGetter>(IRecordTest.LinkCache);
            var oreSubStrings = ore?.Name?.String?.Split(' ');
            if (oreSubStrings == null) return false;
            
            foreach (var subString in oreSubStrings) {
                if (subString.Equals("Ore", StringComparison) || subString.Equals("Gem", StringComparison)) continue;

                if (activator.EditorID.Contains(subString, StringComparison)) return true;
            }
        }

        return false;
    }
}