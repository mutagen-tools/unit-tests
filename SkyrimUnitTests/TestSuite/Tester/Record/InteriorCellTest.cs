﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mutagen.Bethesda.FormKeys.SkyrimSE;
using Mutagen.Bethesda.Skyrim;
namespace SkyrimUnitTests.TestSuite.Tester.Record; 

public class InteriorCellRecordTest : RecordTest<ICell, ICellGetter, ICellGetter> {
    public override string Name => "Interior Cell";
    public override List<RecordBug<ICell, ICellGetter>> Bugs { get; } = [
        new RecordBug<ICell, ICellGetter>("No Music", "Cell has no music",
            BugSeverity.Warning, true, TestMusic),
        new RecordBug<ICell, ICellGetter>("No LightingTemplate", "Cell has no lighting template",
            BugSeverity.Warning, true, TestLightingTemplate),
        new RecordBug<ICell, ICellGetter>("No AcousticSpace", "Cell has no acoustic space",
            BugSeverity.Warning, true, TestAcousticSpace),
        new RecordBug<ICell, ICellGetter>("No Location", "Cell has no location",
            BugSeverity.Warning, true, TestLocation),
        new RecordBug<ICell, ICellGetter>("Unowned Bed", "Beds in owned interiors that are not public should be owned themselves",
            BugSeverity.Inconsistency, true, TestUnownedBed),
        new RecordBug<ICell, ICellGetter>("One NorthMarker", "All interiors should have a north marker, but only one.",
            BugSeverity.Inconsistency, true, TestMissingNorthMarker),
        new RecordBug<ICell, ICellGetter>("Placed NPC not in Cell Faction", "All unique NPCs should be in the faction of the cell they are placed in",
            BugSeverity.Warning, true, TestNPCNotInCellFaction),
        new RecordBug<ICell, ICellGetter>("Public cell has lock list", "Public cells with lock lists trigger trespass behavior for regular entry",
            BugSeverity.Warning, true, TestPublicCellHasLockList),
        new RecordBug<ICell, ICellGetter>("Unowned Work Marker", "Sweep, farm or bar markers used by unauthorized NPCs would be strange",
            BugSeverity.Inconsistency, true, TestUnownedWorkMarker, PatchUnownedWorkMarker)
    ];

    protected override IEnumerable<ICellGetter> GetTestRelevantRecords(ICellGetter record) {
        if (record.IsInteriorCell()) yield return record;
    }

    //No Music
    private static bool TestMusic(ICellGetter cell, List<string> list) {
        return !cell.Music.IsNull;
    }
    
    //No LightingTemplate
    private static bool TestLightingTemplate(ICellGetter cell, List<string> list) {
        return !cell.LightingTemplate.IsNull;
    }
    
    //No AcousticSpace
    private static bool TestAcousticSpace(ICellGetter cell, List<string> list) {
        return !cell.AcousticSpace.IsNull;
    }
    
    //No Location
    private static bool TestLocation(ICellGetter cell, List<string> list) {
        return !cell.Location.IsNull;
    }
    
    //Unowned Bed
    private static bool TestUnownedBed(ICellGetter cell, List<string> list) {
        if ((cell.Flags & Cell.Flag.PublicArea) != 0 || cell.Owner.IsNull) return true;

        foreach (var placed in cell.Persistent.Concat(cell.Temporary)) {
            if (placed.IsDeleted) continue;
            if (placed is not IPlacedObjectGetter placedObject) continue;
            if (!placedObject.Owner.IsNull) continue;
            if (!IRecordTest.LinkCache.TryResolve<IFurnitureGetter>(placedObject.Base.FormKey, out var furniture)) continue;
            if (furniture.EditorID == null || !furniture.EditorID.Contains("bed", StringComparison.OrdinalIgnoreCase)) continue;

            list.Add(placed.FormKey.ToString());
        }

        return !list.Any();
    }
    
    //Missing NorthMarker
    private static bool TestMissingNorthMarker(ICellGetter cell, List<string> list) {
        var count = 0;
        foreach (var placed in cell.Persistent.Concat(cell.Temporary)) {
            if (placed.IsDeleted) continue;
            if (placed is not IPlacedObjectGetter placedObject) continue;
            
            if (placedObject.Base.FormKey == Skyrim.Static.NorthMarker.FormKey) count++;
        }
    
        return count == 1;
    }
    
    //Placed NPC not in Cell Faction
    private static bool TestNPCNotInCellFaction(ICellGetter cell, List<string> list) {
        if (cell.Owner.IsNull) return true;
        if (!IRecordTest.LinkCache.TryResolve<IFactionGetter>(cell.Owner.FormKey, out var faction)) return true;

        foreach (var placed in cell.Persistent.Concat(cell.Temporary)) {
            if (placed.IsDeleted) continue;
            if (placed is not IPlacedNpcGetter placedNpc) continue;

            var npc = placedNpc.Base.TryResolve(IRecordTest.LinkCache);
            if (npc == null || !npc.IsUnique()) continue;

            if (npc.Factions.Any(r => r.Faction.FormKey == faction.FormKey)) continue;
            if (!IRecordTest.LinkCache.TryResolve<ILocationGetter>(cell.Location.FormKey, out var location)) continue;

            // Skip if cell is inn and npc is not innkeeper or server
            if (location.IsInnLocation()) {
                if (npc.HasFaction(editorId =>
                    editorId != null && (
                        editorId.Contains("JobInn", StringComparison.Ordinal)
                     || editorId.Contains("JobBard", StringComparison.Ordinal)))) {
                    list.Add(placed.FormKey.ToString());
                }
                continue;
            }

            // Skip prisoners
            if (npc.HasFaction(editorId => editorId != null && editorId.Contains("Prisoner"))) continue;

            list.Add(placed.FormKey.ToString());
        }

        return list.Count == 0;
    }
    
    //Public cell has Lock List
    private static bool TestPublicCellHasLockList(ICellGetter cell, List<string> list) {
        if (!cell.IsPublic()) return true;

        return cell.LockList.IsNull;
    }
    
    //Unowned Work Marker
    private static bool TestUnownedWorkMarker(ICellGetter cell, List<string> list) {
        if (cell.Owner.IsNull) return true;
        
        foreach (var placed in cell.Temporary.Concat(cell.Persistent)) {
            if (placed.IsDeleted) continue;
            if (placed is not IPlacedObjectGetter placedObject) continue;
            if (placedObject.Base.FormKey != Skyrim.IdleMarker.SweepIdleMarker.FormKey && placedObject.Base.FormKey != Skyrim.IdleMarker.IdleFarmingMarker.FormKey && placedObject.Base.FormKey != Skyrim.Furniture.CounterBarLeanMarker.FormKey) continue;

            if (placedObject.Owner.IsNull) {
                list.Add(placedObject.FormKey.ToString());
            }
        }

        return !list.Any();
    }

    private static void PatchUnownedWorkMarker(ICell cell, ISkyrimMod mod) {
        if (cell.Owner.IsNull) return;
        
        foreach (var placed in cell.Temporary.Concat(cell.Persistent)) {
            if (placed.IsDeleted) continue;
            if (placed is not IPlacedObjectGetter placedObject) continue;
            if (placedObject.Base.FormKey != Skyrim.IdleMarker.SweepIdleMarker.FormKey && placedObject.Base.FormKey != Skyrim.IdleMarker.IdleFarmingMarker.FormKey && placedObject.Base.FormKey != Skyrim.Furniture.CounterBarLeanMarker.FormKey) continue;

            if (placedObject.Owner.IsNull) {
                var context = IRecordTest.LinkCache.ResolveContext<IPlacedObject, IPlacedObjectGetter>(placedObject.FormKey);
                var placedOverride = context.GetOrAddAsOverride(mod);
                placedOverride.Owner.SetTo(cell.Owner.FormKey);
            }
        }
    }
}