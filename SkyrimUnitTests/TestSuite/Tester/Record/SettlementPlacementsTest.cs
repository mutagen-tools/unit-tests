﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mutagen.Bethesda.FormKeys.SkyrimSE;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Skyrim;
using Noggog;
namespace SkyrimUnitTests.TestSuite.Tester.Record; 

public class SettlementPlacementsRecordTest : RecordTest<IPlaced, IPlacedGetter, ICellGetter> {
    public override string Name => "Settlement Placements";
    public override List<RecordBug<IPlaced, IPlacedGetter>> Bugs { get; } = [
        new RecordBug<IPlaced, IPlacedGetter>("No Boss LocTypeRef", "All unique adult NPCs in settlements should usually have LocRefType boss for NPCs",
            BugSeverity.Warning, true, TestAllNPCsBoss, PatchAllNPCsBoss),
        new RecordBug<IPlaced, IPlacedGetter>("No Bed LocTypeRef", "Cell has no lighting template",
            BugSeverity.Warning, true, TestBedLocRefType, PatchBedLocRefType),
        new RecordBug<IPlaced, IPlacedGetter>("No Merchant LocTypeRef", "Chests that are linked by vendor factions should have the MerchantContainerRefType LocRefType",
            BugSeverity.Warning, true, TestMerchantLocRefType, PatchMerchantLocRefType),
        new RecordBug<IPlaced, IPlacedGetter>("Door not locked", "Non-public settlement cells should be locked",
            BugSeverity.Warning, true, TestDoorLocked, PatchDoorLocked),
        new RecordBug<IPlaced, IPlacedGetter>("Door locked from outside", "Non-public settlement cells should be locked",
            BugSeverity.Warning, true, TestDoorLockedFromOutside),
        new RecordBug<IPlaced, IPlacedGetter>("Door not locked with key", "Non-public settlement cells should be locked with a key",
            BugSeverity.Warning, true, TestDoorLockedKey),
        new RecordBug<IPlaced, IPlacedGetter>("Door without owner faction", "Non-public settlement cells should have a faction own the door",
            BugSeverity.Warning, true, TestDoorOwnerFaction, PatchDoorOwnerFaction),
    ];

    protected override IEnumerable<IPlacedGetter> GetTestRelevantRecords(ICellGetter record) {
        if (!record.IsSettlementCell()) yield break;

        foreach (var placed in record.Temporary.Combine(record.Persistent)) {
            if (placed.IsDeleted) continue;

            yield return placed;
        }
    }
    
    //No Boss LocTypeRef
    private static bool TestAllNPCsBoss(IPlacedGetter placed, List<string> list) {
        if (placed is not IPlacedNpcGetter placedNpc) return true;
        if ((placedNpc.MajorFlags & PlacedNpc.MajorFlag.InitiallyDisabled) != 0) return true;
        if (placedNpc.LocationRefTypes != null && placedNpc.LocationRefTypes.Any(refType => refType.FormKey == Skyrim.LocationReferenceType.Boss.FormKey)) return true;

        var npc = placedNpc.Base.TryResolve(IRecordTest.LinkCache);
        if (npc is null || !npc.IsUnique()) return true;
        
        return !IRecordTest.LinkCache.TryResolve<IRaceGetter>(npc.Race.FormKey, out var race) || race.IsChildRace();
    }

    private static void PatchAllNPCsBoss(IPlaced placed, ISkyrimMod patchFile) {
        if (placed is not IPlacedObject placedObject) return;

        placedObject.LocationRefTypes ??= [];
        placedObject.LocationRefTypes.Add(Skyrim.LocationReferenceType.Boss);
    }

    //No Bed LocTypeRef
    private static bool TestBedLocRefType(IPlacedGetter placed, List<string> list) {
        if (placed is not IPlacedObjectGetter placedObject) return true;
        var isBed = placedObject.IsBed();
        var hasLocRefType = placedObject.LocationRefTypes != null && placedObject.LocationRefTypes.Any(locRefType => locRefType.FormKey == Skyrim.LocationReferenceType.HouseBedRefType.FormKey);

        if (isBed) return hasLocRefType;
        if (!hasLocRefType) return true;

        list.Add("Special case: This is not a bed, but has the HouseBedRefType LocRefType");
        return false;
    }

    private static void PatchBedLocRefType(IPlaced placed, ISkyrimMod patchFile) {
        if (placed is not IPlacedObject placedObject) return;

        placedObject.LocationRefTypes ??= [];
        placedObject.LocationRefTypes.Add(Skyrim.LocationReferenceType.HouseBedRefType);
    }

    //Merchant Chest no LocRefType
    private static bool TestMerchantLocRefType(IPlacedGetter placed, List<string> list) {
        if (placed is not IPlacedObjectGetter placedObject) return true;
        if (!placedObject.IsMerchantChest()) return true;

        return placedObject.LocationRefTypes != null && placedObject.LocationRefTypes.Any(locRefType => locRefType.FormKey == Skyrim.LocationReferenceType.MerchantContainerRefType.FormKey);
    }

    private static void PatchMerchantLocRefType(IPlaced placed, ISkyrimMod patchFile) {
        if (placed is not IPlacedObject placedObject) return;

        placedObject.LocationRefTypes ??= [];
        placedObject.LocationRefTypes.Add(Skyrim.LocationReferenceType.MerchantContainerRefType);
    }

    //Door not locked
    private static bool TestDoorLocked(IPlacedGetter placed, List<string> list) {
        if (placed is not IPlacedObjectGetter placedObject) return true;
        if (!placedObject.LeadsToExterior()) return true;
        if (!IRecordTest.LinkCache.TryResolveSimpleContext<IPlacedObjectGetter>(placedObject.FormKey, out var context)) return true;
        if (context.Parent?.Record is not ICellGetter cell) return true;
        if (cell.IsPublic()) return true;

        return placedObject.Lock is not null;
    }
    
    private static void PatchDoorLocked(IPlaced placed, ISkyrimMod patchFile) {
        if (placed is not IPlacedObject placedObject) return;

        placedObject.Lock ??= new LockData {
            Level = Random.Shared.Next(0, 3) switch {
                0 => LockLevel.Novice,
                1 => LockLevel.Apprentice,
                2 => LockLevel.Adept,
            },
        };
        // todo try to infer key via editorID
    }
    
    //Door locked from outside
    private static bool TestDoorLockedFromOutside(IPlacedGetter placed, List<string> list) {
        if (placed is not IPlacedObjectGetter placedObject) return true;
        if (placedObject.Lock is not null) return true;
        if (!placedObject.LeadsToExterior(out var exteriorDoor)) return true;

        return exteriorDoor.Lock is null;
    }
    
    //Door not locked with Key
    private static bool TestDoorLockedKey(IPlacedGetter placed, List<string> list) {
        if (placed is not IPlacedObjectGetter placedObject) return true;
        if (placedObject.Lock is null) return true;
        if (!placedObject.LeadsToExterior()) return true;

        return !placedObject.Lock.Key.IsNull;
    }

    //Door without owner faction
    private static bool TestDoorOwnerFaction(IPlacedGetter placed, List<string> list) {
        if (placed is not IPlacedObjectGetter placedObject) return true;
        if (!placedObject.LeadsToExterior()) return true;
        if (!IRecordTest.LinkCache.TryResolveSimpleContext<IPlacedObjectGetter>(placedObject.FormKey, out var context)) return true;
        if (context.Parent?.Record is not ICellGetter cell) return true;
        if (cell.IsPublic()) return true;

        return !placedObject.Owner.IsNull;
    }
    
    private static void PatchDoorOwnerFaction(IPlaced placed, ISkyrimMod patchFile) {
        if (placed is not IPlacedObject placedObject) return;
        if (!IRecordTest.LinkCache.TryResolveContext<PlacedObject, IPlacedObjectGetter>(placedObject.FormKey, out var context)) return;
        if (context.Parent?.Record is not ICellGetter cell) return;
        if (cell.Owner.IsNull) return;

        placedObject.Owner = new FormLinkNullable<IOwnerGetter>(cell.Owner.FormKey);
    }
}