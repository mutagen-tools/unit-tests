﻿using System;
using System.Collections.Generic;
using Mutagen.Bethesda;
using Mutagen.Bethesda.FormKeys.SkyrimSE;
using Mutagen.Bethesda.Skyrim;
namespace SkyrimUnitTests.TestSuite.Tester.Record; 

public class UniqueNPCRecordTest : RecordTest<INpc, INpcGetter, INpcGetter> {
    private const string CleanupScriptName = "WIDeadBodyCleanupScript";

    public override string Name => "Unique NPC";

    public override List<RecordBug<INpc, INpcGetter>> Bugs { get; } = [
        new RecordBug<INpc, INpcGetter>("No Voice Type", "All unique NPCs should have a voice type",
            BugSeverity.Warning, true, TestVoiceType),
        new RecordBug<INpc, INpcGetter>("No Cleanup Script", "All unique NPCs should have a cleanup script to store their items in a grave when they die",
            BugSeverity.Inconsistency, true, TestCleanupScript),
        new RecordBug<INpc, INpcGetter>("DeathContainer respawn", "DeathContainer containers shouldn't have the respawn flag set",
            BugSeverity.Warning, true, TestDeathContainerRespawn),
        new RecordBug<INpc, INpcGetter>("No Crime Faction", "All unique NPCs should be assigned to a crime faction",
            BugSeverity.Inconsistency, true, TestCrimeFaction),
        new RecordBug<INpc, INpcGetter>("Empty Inventory", "All unique NPCs should have some items",
            BugSeverity.Inconsistency, true, TestEmptyInventory),
        new RecordBug<INpc, INpcGetter>("No Outfit", "All unique NPCs should wear something",
            BugSeverity.Inconsistency, true, TestNoOutfit),
        new RecordBug<INpc, INpcGetter>("No sleep package", "All unique NPCs should wear something",
            BugSeverity.Inconsistency, true, TestNoSleepPackage),
        new RecordBug<INpc, INpcGetter>("Matching class", "Certain types of npcs should have matching classes",
            BugSeverity.Inconsistency, true, TestMatchingClass),
    ];

    protected override IEnumerable<INpcGetter> GetTestRelevantRecords(INpcGetter record) {
        if (record.HasKeyword(Skyrim.Keyword.ActorTypeNPC) && record.IsUnique()) yield return record;
    }

    //No Voice Type
    private static bool TestVoiceType(INpcGetter npc, List<string> list) {
        return npc.Configuration.TemplateFlags.HasFlag(NpcConfiguration.TemplateFlag.Traits) || !npc.Voice.IsNull;
    }

    //No Cleanup Script
    private static bool TestCleanupScript(INpcGetter npc, List<string> list) {
        if (npc.Configuration.TemplateFlags.HasFlag(NpcConfiguration.TemplateFlag.Script)) return true;
        
        var script = npc.GetScript(CleanupScriptName);
        if (script is null) {
            list.Add("No cleanup script found");
            return false;
        }

        var deathContainer = script.GetProperty<IScriptObjectPropertyGetter>("DeathContainer");
        if (deathContainer is null) {
            list.Add("DeathContainer property not filled");
            return false;
        }

        return true;
    }

    //DeathContainer respawn
    private static bool TestDeathContainerRespawn(INpcGetter npc, List<string> list) {
        var script = npc.GetScript(CleanupScriptName);
        var deathContainer = script?.GetProperty<IScriptObjectPropertyGetter>("DeathContainer");
        if (deathContainer is null) return true;

        if (!IRecordTest.LinkCache.TryResolve<IPlacedObjectGetter>(deathContainer.Object.FormKey, out var placedObjectGetter)) return true;
        if (!IRecordTest.LinkCache.TryResolve<IContainerGetter>(placedObjectGetter.Base.FormKey, out var container)) return true;

        if (container.Flags.HasFlag(Container.Flag.Respawns)) {
            list.Add($"DeathContainer {container.EditorID} has Respawn flag set");
            return false;
        }

        return true;
    }

    //No Crime Faction
    private static bool TestCrimeFaction(INpcGetter npc, List<string> list) {
        return (npc.Configuration.TemplateFlags.HasFlag(NpcConfiguration.TemplateFlag.Factions)
         || npc.AIData.Responsibility == Responsibility.NoCrime || !npc.CrimeFaction.IsNull);
    }
        
    //Empty Inventory
    private static bool TestEmptyInventory(INpcGetter npc, List<string> list) {
        return (npc.Configuration.TemplateFlags.HasFlag(NpcConfiguration.TemplateFlag.Inventory)
         || npc.Items != null && npc.Items.Count != 0);
    }
        
    //No Outfit
    private static bool TestNoOutfit(INpcGetter npc, List<string> list) {
        return npc.Configuration.TemplateFlags.HasFlag(NpcConfiguration.TemplateFlag.Inventory) || !npc.DefaultOutfit.IsNull;
    }

    //No sleep package
    private static bool TestNoSleepPackage(INpcGetter npc, List<string> list) {
        // Skip npcs with templated packages
        if (!npc.Template.IsNull
         && npc.Configuration.TemplateFlags.HasFlag(NpcConfiguration.TemplateFlag.AIPackages)) return true;

        // Skip innkeepers
        if (npc.HasFaction(editorId => editorId != null
         && editorId.Contains("JobInnkeeper", StringComparison.Ordinal))) return true;

        foreach (var packageLink in npc.Packages) {
            if (!IRecordTest.LinkCache.TryResolve<IPackageGetter>(packageLink.FormKey, out var package)) continue;
            if (package.PackageTemplate.FormKey != Skyrim.Package.Sleep.FormKey
             && package.PackageTemplate.FormKey != Skyrim.Package.SleepAndGuard.FormKey) continue;

            return true;
        }

        return false;
    }

    //Matching class
    private static bool TestMatchingClass(INpcGetter npc, List<string> list) {
        if (npc.Class.IsNull) return false;

        var @class = npc.Class.TryResolve(IRecordTest.LinkCache);
        if (@class is null) return false;

        if (!IRecordTest.LinkCache.TryResolve<IRaceGetter>(npc.Race.FormKey, out var race)) return true;

        if (race.IsChildRace()) {
            if (npc.Class.FormKey == Skyrim.Class.Citizen.FormKey) {
                list.Add("Child NPC without Child class");
            }
        } else {
            if (npc.Class.FormKey == Skyrim.Class.Child.FormKey) {
                list.Add("Non-Child NPC with Child class");
            }
        }

        var isFarmer = Is("JobFarmer");
        var isBeggar = Is("Beggar");
        var isPriest = Is("JobPriest");
        var isLumberjack = Is("JobLumberjack");
        var isTrainer = Is("JobTrainer");
        var isMerchant = Is("JobMerchant");

        if (isFarmer && npc.Class.FormKey != Skyrim.Class.Farmer.FormKey) {
            list.Add("Farmer NPC without Farmer class");
            return false;
        }

        if (isBeggar && npc.Class.FormKey != Skyrim.Class.Beggar.FormKey) {
            list.Add("Beggar NPC without Beggar class");
            return false;
        }

        if (isPriest && npc.Class.FormKey != Skyrim.Class.Priest.FormKey) {
            list.Add("Priest NPC without Priest class");
            return false;
        }

        if (isLumberjack && npc.Class.FormKey != Skyrim.Class.Lumberjack.FormKey) {
            list.Add("Lumberjack NPC without Lumberjack class");
            return false;
        }

        var trainerClass = @class.EditorID is not null
                           && @class.EditorID.Contains("Trainer", StringComparison.OrdinalIgnoreCase);
        if (isTrainer && !trainerClass) {
            list.Add("Trainer NPC without Trainer class");
            return false;
        }

        var isMerchantClass = @class.EditorID is not null
                              && @class.EditorID.Contains("Vendor", StringComparison.OrdinalIgnoreCase);
        if (!isTrainer && isMerchant && !isMerchantClass && ! isPriest) {
            list.Add("Merchant NPC without Merchant class");
            return false;
        }

        if (npc.Class.FormKey == Skyrim.Class.Citizen.FormKey
            && (isFarmer || isBeggar || isPriest || isLumberjack || isTrainer || isMerchant)) {
            list.Add("Job NPC without Job class");
            return false;
        }

        return true;

        bool Is(string factionContains) {
            return npc.HasFaction(editorId =>
                editorId is not null && editorId.Contains(factionContains, StringComparison.OrdinalIgnoreCase));
        }
    }
}