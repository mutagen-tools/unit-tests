﻿using System.Collections.Generic;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Skyrim;
namespace SkyrimUnitTests.TestSuite.Tester.Record; 

public class LeveledItemRecordTest : RecordTest<ILeveledItem, ILeveledItemGetter, ILeveledItemGetter> {
    public override string Name => "Leveled Items";
    public override List<RecordBug<ILeveledItem, ILeveledItemGetter>> Bugs { get; } = [
        new RecordBug<ILeveledItem, ILeveledItemGetter>("Circular Leveled List", "Circular Leveled Lists cause issues",
            BugSeverity.Error, true, TestCircularLeveledList)
    ];

    protected override IEnumerable<ILeveledItemGetter> GetTestRelevantRecords(ILeveledItemGetter record) {
        yield return record;
    }

    //Circular Leveled List
    private static bool TestCircularLeveledList(ILeveledItemGetter leveledItem, List<string> list) {
        if (leveledItem.Entries == null) return true;

        return FindCircularList(leveledItem, new Stack<FormKey>(), list);
    }
    
    private static bool FindCircularList(ILeveledItemGetter leveledItem, Stack<FormKey> stack, List<string> reports) {
        if (stack.Contains(leveledItem.FormKey)) return false;
        stack.Push(leveledItem.FormKey);

        if (leveledItem.Entries != null) {
            foreach (var entry in leveledItem.Entries) {
                if (entry.Data == null || !IRecordTest.LinkCache.TryResolve<ILeveledItemGetter>(entry.Data.Reference.FormKey, out var leveledList)) continue;

                if (!FindCircularList(leveledList, stack, reports)) {
                    reports.Add($"{leveledList.FormKey} in {leveledItem.FormKey}");

                    return false;
                }
            }
        }
        stack.Pop();

        return true;
    }
}