﻿using System.Collections.Generic;
using System.Linq;
using Mutagen.Bethesda;
using Mutagen.Bethesda.FormKeys.SkyrimSE;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Skyrim;
namespace SkyrimUnitTests.TestSuite.Tester.Record;

public class SettlementCellTest : RecordTest<ICell, ICellGetter, ICellGetter> {
    public override string Name => "Settlement Cell";
    public override List<RecordBug<ICell, ICellGetter>> Bugs { get; } = [
        new RecordBug<ICell, ICellGetter>("No Faction", "Settlement cells should have a faction",
            BugSeverity.Warning, true, TestFaction),
        new RecordBug<ICell, ICellGetter>("No lock list", "Non-public settlement cells should have a lock list",
            BugSeverity.Warning, true, TestLockList, PatchLockList)
    ];
    
    protected override IEnumerable<ICellGetter> GetTestRelevantRecords(ICellGetter record) {
        if (record.IsSettlementCell()) yield return record;
    }
    
    //No Faction
    private static bool TestFaction(ICellGetter cell, List<string> list) {
        // Skip player homes
        if (IRecordTest.LinkCache.TryResolve<ILocationGetter>(cell.Location.FormKey, out var location)
         && location.HasKeyword(Skyrim.Keyword.LocTypePlayerHouse)) {
            return true;
        }
        
        return !cell.Owner.IsNull;
    }
    
    //No lock list
    private static bool TestLockList(ICellGetter cell, List<string> list) {
        if (cell.IsPublic()) return true;

        return !cell.LockList.IsNull;
    }

    private static void PatchLockList(ICell cell, ISkyrimMod patchFile) {
        if (!cell.IsSettlementCell()) return;

        var lockListEditorId = cell.EditorID + "LockList";
        
        // If the lock list already exists, use it
        if (IRecordTest.LinkCache.TryResolve<ILockListGetter>(lockListEditorId, out var lockListGetter)) {
            cell.LockList = new FormLinkNullable<ILockListGetter>(lockListGetter.FormKey);
            return;
        }

        var npcs = cell.Persistent.Concat(cell.Temporary)
            .Where(x => !x.IsDeleted)
            .OfType<IPlacedNpcGetter>()
            .Select(x => x.Base.FormKey)
            .ToList();

        var lockList = IRecordTest.PatchMod.FormLists.AddNew(lockListEditorId);
        lockList.Items.AddRange(npcs);
        cell.LockList = new FormLinkNullable<ILockListGetter>(lockList.FormKey);
    }
}
