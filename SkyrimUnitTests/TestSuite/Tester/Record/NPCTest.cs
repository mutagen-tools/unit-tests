﻿using System;
using System.Collections.Generic;
using Mutagen.Bethesda;
using Mutagen.Bethesda.FormKeys.SkyrimSE;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Skyrim;
namespace SkyrimUnitTests.TestSuite.Tester.Record; 

public class NPCRecordTest : RecordTest<INpc, INpcGetter, INpcGetter> {
    public override string Name => "NPC";
    public override List<RecordBug<INpc, INpcGetter>> Bugs { get; } = [
        new RecordBug<INpc, INpcGetter>("Default Face", "NPCs shouldn't use the default face for variation and immersion",
            BugSeverity.Inconsistency, true, TestDefaultFace),
        new RecordBug<INpc, INpcGetter>("Apply DefaultHomeOwnerPackageList", "DefaultHomeOwnerPackageList list can be applied instead of DefaultSandboxHomeowner as the least priority package",
            BugSeverity.Inconsistency, true, TestDefaultSandboxHomeownerList, PatchDefaultSandboxHomeownerList),
        new RecordBug<INpc, INpcGetter>("Level 0", "NPCs shouldn't be level zero, as it can cause with intimidation",
            BugSeverity.Warning, true, TestLevel0, PatchLevel0),
        new RecordBug<INpc, INpcGetter>("Trainer requires script", "NPC with trainer faction needs to have TrainerGoldScript",
            BugSeverity.Warning, true, TestTrainerGoldScript, PatchTrainerGoldScript),
        new RecordBug<INpc, INpcGetter>("Merchant without specialization", "NPC has JobMerchant faction, without specialized Job faction",
            BugSeverity.Warning, true, TestMerchantSpecialization),
        new RecordBug<INpc, INpcGetter>("Trainer without specialization", "NPCs with a JobMerchant faction need specialized Job faction, otherwise their merchant dialogue might not work",
            BugSeverity.Warning, true, TestTrainerSpecialization),
        new RecordBug<INpc, INpcGetter>("Ghost keyword", "NPC with Ghost script or IsGhost flag should also have ActorTypeGhost keyword",
            BugSeverity.Inconsistency, true, TestGhostKeyword, PatchGhostKeyword),
        new RecordBug<INpc, INpcGetter>("Carry package without script", "NPC uses carry package, but doesn't have carry script attached",
            BugSeverity.Inconsistency, true, TestCarryPackageScript),
        new RecordBug<INpc, INpcGetter>("Child low confidence", "Children have low confidence so they dont attempt to draw weapon",
            BugSeverity.Inconsistency, true, TestChildLowConfidence, PatchChildLowConfidence),
        new RecordBug<INpc, INpcGetter>("Duplicate short name", "When full name is the same as short name, short name can be cut",
            BugSeverity.Inconsistency, true, TestDuplicateShortName, DuplicateShortName),
    ];

    protected override IEnumerable<INpcGetter> GetTestRelevantRecords(INpcGetter record) {
        yield return record;
    }

    //Default Face
    private static bool TestDefaultFace(INpcGetter npc, List<string> list) {
        if (!npc.HasKeyword(Skyrim.Keyword.ActorTypeNPC)) return true;
        //todo load all masters of activate plugin into cache
        var race = npc.Race.TryResolve(IRecordTest.LinkCache);
            
        //Return if race is null or doesn't use custom face gen
        if (race == null || (race.Flags & Race.Flag.FaceGenHead) == 0) return true;
            
        //This can be improved, the faces might still be default, but it checks if face wasn't edited
        if (npc.FaceMorph == null) {
            const string faceMorph = "Face Morph";
            list.Add(faceMorph);
        }
        if (npc.FaceParts == null) {
            const string faceParts = "Face Parts";
            list.Add(faceParts);
        }
        
        return npc is { FaceMorph: not null, FaceParts: not null };
    }

    //Apply Default Sandbox Homeowner List
    private static bool TestDefaultSandboxHomeownerList(INpcGetter npc, List<string> list) {
        if (npc.Packages.Count == 0) return true;

        // Can't apply DefaultHomeOwnerPackageList if the NPC already has a package list, or if the package list is DefaultHomeOwnerPackageList
        if (!npc.DefaultPackageList.IsNull) return true;
        if (npc.DefaultPackageList.FormKey == Skyrim.FormList.DefaultHomeOwnerPackageList.FormKey) return true;

        return npc.Packages[^1].FormKey != Skyrim.Package.DefaultSandboxHomeowner.FormKey;
    }

    private static void PatchDefaultSandboxHomeownerList(INpc npc, ISkyrimMod mod) {
        npc.Packages.RemoveAt(npc.Packages.Count - 1);
        npc.DefaultPackageList = new FormLinkNullable<IFormListGetter>(Skyrim.FormList.DefaultHomeOwnerPackageList.FormKey);
    }

    //Level 0
    private static bool TestLevel0(INpcGetter npc, List<string> list) {
        return npc.Configuration.Level switch {
            INpcLevelGetter npcLevel => npcLevel.Level != 0,
            IPcLevelMultGetter pcLevelMult => Math.Abs(pcLevelMult.LevelMult) > 0.001 && npc.Configuration.CalcMinLevel > 0,
            _ => true
        };
    }

    private static void PatchLevel0(INpc npc, ISkyrimMod mod) {
        switch (npc.Configuration.Level) {
            case INpcLevel npcLevel:
                npcLevel.Level = 1;
                break;
            case IPcLevelMult:
                npc.Configuration.CalcMinLevel = 1;
                break;
        }
    }

    //NPC with trainer faction needs to have TrainerGoldScript
    private static bool TestTrainerGoldScript(INpcGetter npc, List<string> list) {
        if (!npc.Template.IsNull) return true;

        var hasTrainerGoldScript = npc.VirtualMachineAdapter is not null && npc.HasScript("TrainerGoldScript");
        foreach (var rankPlacement in npc.Factions) {
            if (!rankPlacement.Faction.TryResolve(IRecordTest.LinkCache, out var faction)) continue;
            if (faction.EditorID == null) continue;
            if (!faction.EditorID.Contains("Trainer", StringComparison.OrdinalIgnoreCase)) continue;

            if (hasTrainerGoldScript) return true;

            list.Add($"Has {faction.EditorID} faction but no TrainerGoldScript");
            return false;
        }

        // Doesn't have trainer faction
        if (!hasTrainerGoldScript) return true;
        
        list.Add("Has no Trainer faction, but has TrainerGoldScript");
        return false;
    }

    private static void PatchTrainerGoldScript(INpc npc, ISkyrimMod mod) {
        var hasTrainerGoldScript = npc.VirtualMachineAdapter != null && npc.VirtualMachineAdapter.Scripts.Exists(s => s.Name == "TrainerGoldScript");

        if (!hasTrainerGoldScript) {
            npc.VirtualMachineAdapter ??= new VirtualMachineAdapter();
            npc.VirtualMachineAdapter.Scripts.Add(new ScriptEntry {
                Name = "TrainerGoldScript",
                Flags = ScriptEntry.Flag.Local,
                Properties = [
                    new ScriptObjectProperty {
                        Name = "Gold001",
                        Flags = ScriptProperty.Flag.Edited,
                        Object = Skyrim.MiscItem.Gold001,
                    },
                ]
            });
        }
    }
    
    //Merchant without specialization
    private static bool TestMerchantSpecialization(INpcGetter npc, List<string> list) {
        var isMerchant = npc.HasFaction(editorId =>
            editorId != null && editorId.Contains("JobMerchant", StringComparison.OrdinalIgnoreCase));
        if (!isMerchant) return true;
        
        var hasSpecialization = npc.HasFaction(editorId =>
            editorId != null
            && !editorId.Contains("JobMerchant", StringComparison.OrdinalIgnoreCase)
            && !editorId.Contains("JobTrainer", StringComparison.OrdinalIgnoreCase)
            && editorId.Contains("Job", StringComparison.OrdinalIgnoreCase));
        
        return hasSpecialization;
    }
    
    //Trainer without specialization
    private static bool TestTrainerSpecialization(INpcGetter npc, List<string> list) {
        var isMerchant = npc.HasFaction(editorId =>
            editorId != null && editorId.EndsWith("JobTrainer", StringComparison.OrdinalIgnoreCase));
        if (!isMerchant) return true;
        
        var hasSpecialization = npc.HasFaction(editorId =>
            editorId != null
            && !editorId.EndsWith("JobTrainer", StringComparison.OrdinalIgnoreCase)
            && editorId.Contains("JobTrainer", StringComparison.OrdinalIgnoreCase));

        return hasSpecialization;
    }
    
    //Ghost keyword
    private static bool TestGhostKeyword(INpcGetter npc, List<string> list) {
        if (!npc.IsGhost()) return true;
        
        return npc.HasKeyword(Skyrim.Keyword.ActorTypeGhost);
    }
    
    private static void PatchGhostKeyword(INpc npc, ISkyrimMod mod) {
        npc.Keywords ??= [];
        npc.Keywords.Add(Skyrim.Keyword.ActorTypeGhost);
    }
    
    //Carry package without script
    private static bool TestCarryPackageScript(INpcGetter npc, List<string> list) {
        var hasCarryPackage = false;
        foreach (var packageLink in npc.Packages) {
            if (!IRecordTest.LinkCache.TryResolve<IPackageGetter>(packageLink.FormKey, out var package)) continue;
            if (package.PackageTemplate.FormKey != Skyrim.Package.CarryAndDropItem.FormKey
                && package.PackageTemplate.FormKey != Skyrim.Package.CarryAndKeepItem.FormKey) continue;

            hasCarryPackage = true;
        }
        
        if (!hasCarryPackage) return true;
        
        var scriptEntry = npc.GetScript("CarryActorScript");
        if (scriptEntry is null) {
            list.Add("Carry package without CarryActorScript");
            return false;
        }

        var stopCarryingEventProperty = scriptEntry.GetProperty<IScriptObjectPropertyGetter>("StopCarryingEvent");
        if (stopCarryingEventProperty is null) {
            list.Add("CarryActorScript without StopCarryingEvent property filled");
            return false;
        }
        
        if (stopCarryingEventProperty.Object.FormKey != Skyrim.IdleAnimation.OffsetStop.FormKey) {
            list.Add("CarryActorScript StopCarryingEvent property not set to OffsetStop");
            return false;
        }
        
        var carryItemMiscProperty = scriptEntry.GetProperty<IScriptObjectPropertyGetter>("CarryItemMisc");
        if (carryItemMiscProperty is not null) return true;
        
        var carryItemPotionProperty = scriptEntry.GetProperty<IScriptObjectPropertyGetter>("CarryItemPotion");
        if (carryItemPotionProperty is not null) return true;
        
        var carryItemIngredientProperty = scriptEntry.GetProperty<IScriptObjectPropertyGetter>("CarryItemIngredient");
        if (carryItemIngredientProperty is not null) return true;

        list.Add("CarryActorScript without any CarryItem properties filled");
        return false;
    }
    
    //Child low confidence
    private static bool TestChildLowConfidence(INpcGetter npc, List<string> list) {
        if (npc.Race.TryResolve(IRecordTest.LinkCache, out var race)) {
            if (!race.IsChildRace()) return true;

            return  npc.AIData.Confidence is not Confidence.Brave and not Confidence.Foolhardy;
        }

        return true;
    }
    
    private static void PatchChildLowConfidence(INpc npc, ISkyrimMod mod) {
        npc.AIData.Confidence = Confidence.Cowardly;
    }
    
    //Duplicate short name
    private static bool TestDuplicateShortName(INpcGetter npc, List<string> list) {
        if (npc.Name == null || npc.ShortName == null) return true;
        
        return npc.Name.String != npc.ShortName.String;
    }
    
    private static void DuplicateShortName(INpc npc, ISkyrimMod mod) {
        npc.ShortName = null;
    }
}