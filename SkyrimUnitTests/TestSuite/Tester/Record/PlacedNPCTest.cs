﻿using System.Collections.Generic;
using System.Linq;
using Mutagen.Bethesda.FormKeys.SkyrimSE;
using Mutagen.Bethesda.Skyrim;
using Noggog;
namespace SkyrimUnitTests.TestSuite.Tester.Record; 

public class PlacedNpcRecordTest : RecordTest<IPlacedNpc, IPlacedNpcGetter, ICellGetter> {
    public override string Name => "Placed NPC";
    public override List<RecordBug<IPlacedNpc, IPlacedNpcGetter>> Bugs { get; } = [
        new RecordBug<IPlacedNpc, IPlacedNpcGetter>("No EditorID", "Placed NPCs should have an EditorID if the NPC is unique, usually: TheirNameRef",
            BugSeverity.Inconsistency, true, TestEditorID, PatchEditorID),
        new RecordBug<IPlacedNpc, IPlacedNpcGetter>("Unique NPC without Persistence Location", "Placed NPCs should have a persistence location if the NPC is unique, excludes always persistent npc or initially disabled NPCs",
            BugSeverity.Error, true, TestUniqueNPCWithoutPersistenceLocation),
        new RecordBug<IPlacedNpc, IPlacedNpcGetter>("Unique NPC not in Persistence Location", "Placed NPCs should be placed in persistence location, otherwise they might not be loaded",
            BugSeverity.Error, true, NotTestNPCInPersistenceLocation)
    ];
    
    protected override IEnumerable<IPlacedNpcGetter> GetTestRelevantRecords(ICellGetter record) {
        var placedNPCs = new List<IPlacedNpcGetter>();
        
        foreach (var placed in record.Persistent) {
            if (placed.IsDeleted) continue;

            if (placed is IPlacedNpcGetter placedNpc) {
                placedNPCs.Add(placedNpc);
            }
        }

        return placedNPCs;
    }

    //No EditorID
    private static bool TestEditorID(IPlacedNpcGetter placedNpc, List<string> list) {
        IRecordTest.LinkCache.TryResolve<INpcGetter>(placedNpc.Base.FormKey, out var npc);
        if (npc == null || !npc.IsUnique()) return true;
        
        return placedNpc.EditorID != null && !placedNpc.EditorID.IsNullOrWhitespace();
    }
    
    private static void PatchEditorID(IPlacedNpc placedNpc, ISkyrimMod patchFile) {
        var npc = placedNpc.Base.TryResolve(IRecordTest.LinkCache);
        if (npc == null) return;

        placedNpc.EditorID = $"{npc.EditorID}Ref";
    }
    
    //Unique NPC without Persistence Location
    private static bool TestUniqueNPCWithoutPersistenceLocation(IPlacedNpcGetter placedNpc, List<string> list) {
        if (!placedNpc.PersistentLocation.IsNull || (placedNpc.MajorFlags & PlacedNpc.MajorFlag.InitiallyDisabled) != 0 || (placedNpc.MajorFlags & PlacedNpc.MajorFlag.StartsDead) != 0) return true;

        IRecordTest.LinkCache.TryResolve<INpcGetter>(placedNpc.Base.FormKey, out var npc);
        if (npc == null || !npc.IsUnique()) return true;

        return false;
    }

    //Unique NPC without Persistence Location
    private static bool NotTestNPCInPersistenceLocation(IPlacedNpcGetter placedNpc, List<string> list) {
        if (placedNpc.PersistentLocation.IsNull) return true;
        if (placedNpc.PersistentLocation.FormKey == Skyrim.Location.PersistAll.FormKey) return true;
        if (!IRecordTest.LinkCache.TryResolveSimpleContext<IPlacedNpc>(placedNpc.FormKey, out var placedNpcContext)) return true;
        if (placedNpcContext.Parent?.Record is not ICellGetter cell) return true;

        var persistLocation = placedNpc.PersistentLocation.TryResolve(IRecordTest.LinkCache);
        if (persistLocation == null) return true;
        
        return cell.GetAllLocations()
            .Select(location => location.FormKey)
            .Contains(persistLocation.FormKey);
    }
}