﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mutagen.Bethesda.Skyrim;
namespace SkyrimUnitTests.TestSuite.Tester.Record; 

public class OutfitRecordTest : RecordTest<IOutfit, IOutfitGetter, IOutfitGetter> {
    public override string Name => "Outfit";
    public override List<RecordBug<IOutfit, IOutfitGetter>> Bugs { get; } = [
        new RecordBug<IOutfit, IOutfitGetter>("Conflicting Slots", "Outfits shouldn't have entries for the same armor slot or not equipable items",
            BugSeverity.Warning, true, TestConflictingSlots),
        new RecordBug<IOutfit, IOutfitGetter>("Empty Outfit", "Outfit shouldn't be empty, just use no outfit at all",
            BugSeverity.Warning, true, TestEmptyOutfit)
    ];

    protected override IEnumerable<IOutfitGetter> GetTestRelevantRecords(IOutfitGetter record) {
        yield return record;
    }

    //Conflicting Slots
    private static void GetAllArmors(ILeveledItemGetter leveledItem, ISet<IArmorGetter> fillList) {
        if (leveledItem.Entries == null) return;

        foreach (var item in leveledItem.Entries) {
            if (item.Data == null) continue;

            if (IRecordTest.LinkCache.TryResolve<IArmorGetter>(item.Data.Reference.FormKey, out var armor)) {
                fillList.Add(armor);
            } else if(IRecordTest.LinkCache.TryResolve<ILeveledItemGetter>(item.Data.Reference.FormKey, out var leveled)) {
                GetAllArmors(leveled, fillList);
            }
        }
    }

    private static IEnumerable<BipedObjectFlag> GetSlots(IArmorGetter armor) {
        if ((armor.BodyTemplate?.FirstPersonFlags & BipedObjectFlag.Amulet) != 0) yield return BipedObjectFlag.Amulet;
        if ((armor.BodyTemplate?.FirstPersonFlags & BipedObjectFlag.Body) != 0) yield return BipedObjectFlag.Body;
        if ((armor.BodyTemplate?.FirstPersonFlags & BipedObjectFlag.Calves) != 0) yield return BipedObjectFlag.Calves;
        if ((armor.BodyTemplate?.FirstPersonFlags & BipedObjectFlag.Decapitate) != 0) yield return BipedObjectFlag.Decapitate;
        if ((armor.BodyTemplate?.FirstPersonFlags & BipedObjectFlag.Ears) != 0) yield return BipedObjectFlag.Ears;
        if ((armor.BodyTemplate?.FirstPersonFlags & BipedObjectFlag.Feet) != 0) yield return BipedObjectFlag.Feet;
        if ((armor.BodyTemplate?.FirstPersonFlags & BipedObjectFlag.Forearms) != 0) yield return BipedObjectFlag.Forearms;
        if ((armor.BodyTemplate?.FirstPersonFlags & BipedObjectFlag.Hair) != 0) yield return BipedObjectFlag.Hair;
        if ((armor.BodyTemplate?.FirstPersonFlags & BipedObjectFlag.Hands) != 0) yield return BipedObjectFlag.Hands;
        if ((armor.BodyTemplate?.FirstPersonFlags & BipedObjectFlag.Head) != 0) yield return BipedObjectFlag.Head;
        if ((armor.BodyTemplate?.FirstPersonFlags & BipedObjectFlag.Ring) != 0) yield return BipedObjectFlag.Ring;
        if ((armor.BodyTemplate?.FirstPersonFlags & BipedObjectFlag.Shield) != 0) yield return BipedObjectFlag.Shield;
        if ((armor.BodyTemplate?.FirstPersonFlags & BipedObjectFlag.Tail) != 0) yield return BipedObjectFlag.Tail;
        if ((armor.BodyTemplate?.FirstPersonFlags & BipedObjectFlag.DecapitateHead) != 0) yield return BipedObjectFlag.DecapitateHead;
        if ((armor.BodyTemplate?.FirstPersonFlags & BipedObjectFlag.FX01) != 0) yield return BipedObjectFlag.FX01;
        if ((armor.BodyTemplate?.FirstPersonFlags & BipedObjectFlag.LongHair) != 0) yield return BipedObjectFlag.LongHair;
    }

    private static bool TestConflictingSlots(IOutfitGetter outfit, List<string> list) {
        if (outfit.Items == null) return true;

        var blockedSlots = new Dictionary<BipedObjectFlag, List<string?>>();

        foreach (var item in outfit.Items) {
            void ProcessSlots(IReadOnlyCollection<BipedObjectFlag> slots, string? editorID) {
                //Fill error list
                var sb = new StringBuilder();
                foreach (var (slot, conflictingEditorIDs) in blockedSlots) {
                    if (!slots.Contains(slot)) continue;

                    foreach (var conflictingEditorID in conflictingEditorIDs) {
                        sb.AppendLine($"{conflictingEditorID}: {slot}");
                    }
                }
                if (sb.Length > 0) list.Add($"{editorID} conflicts with:\n{sb}");
                
                //Add slots to list
                foreach (var slot in slots) {
                    if (blockedSlots.TryGetValue(slot, out var blockedSlot)) {
                        blockedSlot.Add(editorID);
                    } else {
                        blockedSlots.Add(slot, [editorID]);                        
                    }
                }
            }
            
            var outfitTarget = item.TryResolve(IRecordTest.LinkCache);
            switch (outfitTarget) {
                case IArmorGetter armor:
                    ProcessSlots(GetSlots(armor).ToList(), armor.EditorID);
                    break;
                case ILeveledItemGetter leveledItem:
                    var armors = new HashSet<IArmorGetter>();
                    GetAllArmors(leveledItem, armors);
                    var leveledListSlots = new HashSet<BipedObjectFlag>();
                    foreach (var slot in armors.SelectMany(GetSlots)) leveledListSlots.Add(slot);
                    ProcessSlots(leveledListSlots, leveledItem.EditorID);
                    break;
            }
        }

        return list.Count == 0;
    }
    
    //Empty Outfit
    private static bool TestEmptyOutfit(IOutfitGetter outfit, List<string> list) {
        return outfit.Items != null && outfit.Items.Any();
    }
}