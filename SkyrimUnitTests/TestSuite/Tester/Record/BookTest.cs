﻿using System.Collections.Generic;
using System.Linq;
using Mutagen.Bethesda.Skyrim;
namespace SkyrimUnitTests.TestSuite.Tester.Record; 

public class BookTest : RecordTest<IBook, IBookGetter, IBookGetter> {
    public override string Name => "Book";
    public override List<RecordBug<IBook, IBookGetter>> Bugs { get; } = [ 
        new RecordBug<IBook, IBookGetter>("Invalid Char", "Book text contains invalid characters that can't be displayed in-game.",
            BugSeverity.Warning, true, TestInvalidCharacters, PatchInvalidCharacters),
    ];

    protected override IEnumerable<IBookGetter> GetTestRelevantRecords(IBookGetter record) {
        yield return record;
    }

    //Invalid characters
    private static bool TestInvalidCharacters(IBookGetter book, List<string> list) {
        if (book.BookText.String is null) return true;

        return InvalidString.InvalidStrings.All(invalidString => !book.BookText.String.Contains(invalidString.Key));
    }
    
    private static void PatchInvalidCharacters(IBook book, ISkyrimMod mod) {
        if (book.BookText.String is null) return;

        foreach (var (invalid, replacement) in InvalidString.InvalidStrings) {
            book.BookText.String = book.BookText.String.Replace(invalid, replacement);
        }
    }
}
