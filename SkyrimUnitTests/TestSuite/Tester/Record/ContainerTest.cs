﻿using System.Collections.Generic;
using Mutagen.Bethesda.Skyrim;
using SkyrimUnitTests.TestSuite.Tester.Asset.Nif;
namespace SkyrimUnitTests.TestSuite.Tester.Record; 

public class ContainerRecordTest : RecordTest<IContainer, IContainerGetter, IContainerGetter> {
    public override string Name => "Container";
    public override List<RecordBug<IContainer, IContainerGetter>> Bugs { get; } = [
        new RecordBug<IContainer, IContainerGetter>("No Container Sounds", "Containers should have open/close sound",
            BugSeverity.Inconsistency, true, TestSounds)
    ];

    protected override IEnumerable<IContainerGetter> GetTestRelevantRecords(IContainerGetter record) {
        yield return record;
    }

    //Container Sounds
    private static bool TestSounds(IContainerGetter container, List<string> list) {
        var file = container.Model?.File.DataRelativePath;

        var noOpen = container.OpenSound.IsNull && (file == null || NifHelper.HasTextKey(file, "Open", "Sound:"));
        var noClose = container.CloseSound.IsNull && (file == null || NifHelper.HasTextKey(file, "Close", "Sound:"));

        if (noOpen) list.Add("Open Sound");
        if (noClose) list.Add("Close Sound");

        return !noOpen && !noClose;
    }
}