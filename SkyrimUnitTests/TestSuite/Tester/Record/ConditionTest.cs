﻿using System;
using System.Collections.Generic;
using Mutagen.Bethesda.FormKeys.SkyrimSE;
using Mutagen.Bethesda.Skyrim;
namespace SkyrimUnitTests.TestSuite.Tester.Record;

public class ConditionTest : RecordTest<ISkyrimMajorRecord, ISkyrimMajorRecordGetter, ISkyrimMajorRecordGetter> {
	public override string Name => "Conditions";
	public override List<RecordBug<ISkyrimMajorRecord, ISkyrimMajorRecordGetter>> Bugs { get; } = [
		new RecordBug<ISkyrimMajorRecord, ISkyrimMajorRecordGetter>("Null Reference", "Condition runs on reference, but reference is null. Patch will replace null with the player.",
			BugSeverity.Warning, true, TestNullReference, PatchNullReferencePlayer)
	];

	protected override IEnumerable<ISkyrimMajorRecordGetter> GetTestRelevantRecords(ISkyrimMajorRecordGetter record) {
		yield return record;
	}

	private static void PatchNullReferencePlayer(ISkyrimMajorRecord record, ISkyrimMod mod) {
		var conditions = record.GetConditions();
		if (conditions is null) return;

		foreach (var condition in conditions) {
			if (condition.Data.RunOnType != Condition.RunOnType.Reference) continue;
			if (!condition.Data.Reference.IsNull) continue;

			condition.Data.Reference.SetTo(Skyrim.PlayerRef.FormKey);
		}
	}

	//Test Null Reference
	private static bool TestNullReference(ISkyrimMajorRecordGetter record, List<string> list) {
		var conditions = record.GetConditions();
		if (conditions is null) return true;

		var allValid = true;
		foreach (var condition in conditions) {
			if (condition.Data.RunOnType != Condition.RunOnType.Reference) continue;
			if (!condition.Data.Reference.IsNull) continue;

			switch (condition.Data) {
				case IGetEventDataConditionDataGetter getEventData:
					Console.WriteLine($"Event Condition {getEventData.Function} runs on reference, but reference is null");
					break;
				case {} conditionData:
					list.Add($"Condition {conditionData.Function} runs on reference, but reference is null");
					break;
			}
			allValid = false;
		}

		return allValid;
	}
}
