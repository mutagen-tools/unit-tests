﻿using System.Collections.Generic;
using Mutagen.Bethesda.Skyrim;
using Noggog;
namespace SkyrimUnitTests.TestSuite.Tester.Record; 

public class MessageRecordTest : RecordTest<IMessage, IMessageGetter, IMessageGetter> {
    public override string Name => "Message";
    public override List<RecordBug<IMessage, IMessageGetter>> Bugs { get; } = [
        new RecordBug<IMessage, IMessageGetter>("No Content", "Messages without any content (name, description or buttons) might be a bug",
            BugSeverity.Warning, true, TestDescription),
        new RecordBug<IMessage, IMessageGetter>("Buttons for Notifications", "Notification messages shouldn't have buttons, they won't be displayed",
            BugSeverity.Warning, true, TestButtonsForNotifications)
    ];

    protected override IEnumerable<IMessageGetter> GetTestRelevantRecords(IMessageGetter record) {
        yield return record;
    }

    //No Description
    private static bool TestDescription(IMessageGetter message, List<string> list) {
        return (message.Name != null && !message.Name.String.IsNullOrWhitespace()) || !message.Description.String.IsNullOrWhitespace() || message.MenuButtons.Any();
    }
    
    //Buttons for Notifications
    private static bool TestButtonsForNotifications(IMessageGetter message, List<string> list) {
        return (message.Flags & Message.Flag.MessageBox) != 0 || !message.MenuButtons.Any();
    }
}