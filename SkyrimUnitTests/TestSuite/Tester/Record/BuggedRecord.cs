﻿using System;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Plugins.Records;
namespace SkyrimUnitTests.TestSuite.Tester.Record; 

public class BuggedRecord : BuggedItem {
    public BuggedRecord(IMajorRecordIdentifier record) : this(record.FormKey, record.EditorID) {}
    public BuggedRecord(FormKey formKey, string? editorID) {
        FormKey = formKey;
        EditorID = editorID;
    }

    public FormKey FormKey { get; }
    public string? EditorID { get; }
    
    public override int GetHashCode() => HashCode.Combine(FormKey);
    public override int CompareTo(object? obj) {
        if (obj is not BuggedRecord other) return 1;
        
        if (ReferenceEquals(this, other)) return 0;

        return string.Compare(EditorID, other.EditorID, StringComparison.Ordinal);
    }

    public override string ToString() {
        var str = $"{EditorID} {FormKey}";
            
        if (!string.IsNullOrEmpty(BugDescription)) {
            str += Environment.NewLine + BugDescription;
        }

        return str;
    }
}