﻿using System.Collections.Generic;
using System.Linq;
using Mutagen.Bethesda.FormKeys.SkyrimSE;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Skyrim;
using Noggog;
namespace SkyrimUnitTests.TestSuite.Tester.Record; 

public class MapMarkerRecordTest : RecordTest<IPlacedObject, IPlacedObjectGetter, IPlacedObjectGetter> {
    private static readonly FormLink<ILinkedReferenceGetter> XMarkerHeadingKey = new(Skyrim.Static.XMarkerHeading.FormKey);
    private static readonly FormKey MapMarkerRefTypeKey = Skyrim.LocationReferenceType.MapMarkerRefType.FormKey;
    
    
    public override string Name => "Map Marker";
    public override List<RecordBug<IPlacedObject, IPlacedObjectGetter>> Bugs { get; } = [
        new RecordBug<IPlacedObject, IPlacedObjectGetter>("Not Persistent", "Map markers must be persistent",
            BugSeverity.Error, true, TestPersistence, PatchPersistence),
        new RecordBug<IPlacedObject, IPlacedObjectGetter>("No Loc Ref Type", "Map markers should have the location ref type MapMarkerRefType to allow generic setups",
            BugSeverity.Inconsistency, true, TestLocRefType, PatchLocRefType),
        new RecordBug<IPlacedObject, IPlacedObjectGetter>("No EditorID", "Map markers usually should have an editor id",
            BugSeverity.Inconsistency, false, TestEditorID),
        new RecordBug<IPlacedObject, IPlacedObjectGetter>("No Linked Reference", "When travelling to a map marker, players will be moved to the map marker or a linked xMarker if available"
          + "as map markers are usually not placed in a good location for player teleporting, they should have a linked xMarker",
            BugSeverity.Warning, true, TestLinkedRef),
        new RecordBug<IPlacedObject, IPlacedObjectGetter>("No Map Marker Data", "Map markers must have map marker data, which is also responsible for the map marker name",
            BugSeverity.Error, true, TestMapMarkerData)
    ];

    protected override IEnumerable<IPlacedObjectGetter> GetTestRelevantRecords(IPlacedObjectGetter record) {
        if (record.Base.FormKey == Skyrim.Static.MapMarker.FormKey) yield return record;
    }

    //Persistence
    private static bool TestPersistence(IPlacedObjectGetter placedObject, List<string> list) {
        return (placedObject.MajorRecordFlagsRaw & 0x400) != 0;
    }
        
    private static void PatchPersistence(IPlacedObject placedObject, ISkyrimMod patchFile) {
        placedObject.MajorRecordFlagsRaw |= 0x400;
    }

    //Loc Ref Type
    private static bool TestLocRefType(IPlacedObjectGetter placedObject, List<string> list) {
        return placedObject.LocationRefTypes != null
         && placedObject.LocationRefTypes.Any(link => link.FormKey == MapMarkerRefTypeKey);
    }

    private static void PatchLocRefType(IPlacedObject placedObject, ISkyrimMod patchFile) {
        placedObject.LocationRefTypes = new ExtendedList<IFormLinkGetter<ILocationReferenceTypeGetter>> {
            new FormLink<ILocationReferenceTypeGetter>(MapMarkerRefTypeKey)
        };
    }

    //Editor ID
    private static bool TestEditorID(IPlacedObjectGetter placedObject, List<string> list) { return placedObject.EditorID != null; }

    //Linked Ref
    private static bool TestLinkedRef(IPlacedObjectGetter placedObject, List<string> list) {
        return placedObject.LinkedReferences.Contains(new LinkedReferences { Reference = XMarkerHeadingKey });
    }

    //Map Marker Data
    private static bool TestMapMarkerData(IPlacedObjectGetter placedObject, List<string> list) { return placedObject.MapMarker != null; }
}