﻿using System.Collections.Generic;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Skyrim;
namespace SkyrimUnitTests.TestSuite.Tester.Record; 

public class LeveledNpcRecordTest : RecordTest<ILeveledNpc, ILeveledNpcGetter, ILeveledNpcGetter> {
    public override string Name => "Leveled Characters";
    public override List<RecordBug<ILeveledNpc, ILeveledNpcGetter>> Bugs { get; } = [
        new RecordBug<ILeveledNpc, ILeveledNpcGetter>("Circular Leveled List", "Circular Leveled Lists cause issues",
            BugSeverity.Error, true, TestCircularLeveledList)
    ];

    protected override IEnumerable<ILeveledNpcGetter> GetTestRelevantRecords(ILeveledNpcGetter record) {
        yield return record;
    }

    //Circular Leveled List
    private static bool TestCircularLeveledList(ILeveledNpcGetter leveledNpc, List<string> list) {
        if (leveledNpc.Entries == null) return true;

        return FindCircularList(leveledNpc, new Stack<FormKey>(), list);
    }
    
    private static bool FindCircularList(ILeveledNpcGetter leveledNpc, Stack<FormKey> stack, List<string> reports) {
        if (stack.Contains(leveledNpc.FormKey)) return false;
        stack.Push(leveledNpc.FormKey);

        if (leveledNpc.Entries != null) {
            foreach (var entry in leveledNpc.Entries) {
                if (entry.Data == null || !IRecordTest.LinkCache.TryResolve<ILeveledNpcGetter>(entry.Data.Reference.FormKey, out var leveledList)) continue;

                if (!FindCircularList(leveledList, stack, reports)) {
                    reports.Add($"{leveledList.FormKey} in {leveledNpc.FormKey}");

                    return false;
                }
            }
        }
        stack.Pop();

        return true;
    }
}