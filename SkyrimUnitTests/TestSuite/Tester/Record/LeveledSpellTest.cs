﻿using System.Collections.Generic;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Skyrim;
namespace SkyrimUnitTests.TestSuite.Tester.Record; 

public class LeveledSpellRecordTest : RecordTest<ILeveledSpell, ILeveledSpellGetter, ILeveledSpellGetter> {
    public override string Name => "Leveled Spells ";
    public override List<RecordBug<ILeveledSpell, ILeveledSpellGetter>> Bugs { get; } = [
        new RecordBug<ILeveledSpell, ILeveledSpellGetter>("Circular Leveled List", "Circular Leveled Lists cause issues",
            BugSeverity.Error, true, TestCircularLeveledList)
    ];

    protected override IEnumerable<ILeveledSpellGetter> GetTestRelevantRecords(ILeveledSpellGetter record) {
        yield return record;
    }

    //Circular Leveled List
    private static bool TestCircularLeveledList(ILeveledSpellGetter leveledSpell, List<string> list) {
        if (leveledSpell.Entries == null) return true;

        return FindCircularList(leveledSpell, new Stack<FormKey>(), list);
    }
    
    private static bool FindCircularList(ILeveledSpellGetter leveledSpell, Stack<FormKey> stack, List<string> reports) {
        if (stack.Contains(leveledSpell.FormKey)) return false;
        stack.Push(leveledSpell.FormKey);

        if (leveledSpell.Entries != null) {
            foreach (var entry in leveledSpell.Entries) {
                if (entry.Data == null || !IRecordTest.LinkCache.TryResolve<ILeveledSpellGetter>(entry.Data.Reference.FormKey, out var leveledList)) continue;

                if (!FindCircularList(leveledList, stack, reports)) {
                    reports.Add($"{leveledList.FormKey} in {leveledSpell.FormKey}");

                    return false;
                }
            }
        }
        stack.Pop();

        return true;
    }
}