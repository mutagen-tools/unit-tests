﻿namespace SkyrimUnitTests.CMD; 

public struct ScriptOptions {
    public string PatchPath;
    public bool AttemptToPatch;
    public bool SaveLog;
    public string LogPath;
}