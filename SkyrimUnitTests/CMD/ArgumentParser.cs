﻿using System;
using System.IO;
using System.Linq;
using Mono.Options;
namespace SkyrimUnitTests.CMD; 

public class ArgumentParser {
    private readonly OptionSet _options;
    private readonly string[] _args;

    private ScriptOptions _inputs;

    public ArgumentParser(string[] args) {
        _args = args;
        _options = new OptionSet {
            { "p|patch=", "Attempt to correct any errors and save them to a given output file", EnablePatch },
            { "l|log=", "Store the output of this program to a given log file", SaveLog },
            { "h|help", "Show this help message", ShowHelp }
        };
    }

    public ScriptOptions Parse() {
        if (!_args.Any()) {
            //No arguments? Show the help and exit
            ShowHelp();
            return _inputs;
        }

        try {
            _options.Parse(_args);
        } catch (OptionException e) {
            Console.WriteLine("UnitTests: " + e.OptionName + " Error: " + e.Message);
            Console.WriteLine("Try 'UnitTests --help' for more information");
            return _inputs;
        }

        return _inputs;
    }

    private void EnablePatch(string path) {
        var fileInfo = new FileInfo(path);
        fileInfo.Directory?.Create();

        //File path must contain the proper file extension.
        if (!path.Contains(".esm") && !path.Contains(".esp")) throw new OptionException("Invalid Patch Output File " + path, "-p|--patch");

        _inputs.PatchPath = path;
        _inputs.AttemptToPatch = true;
    }

    private void SaveLog(string file) {
        try {
            var testFile = File.Open(file, FileMode.OpenOrCreate);
            testFile.Close();
            File.Delete(file);
        } catch (Exception e) {
            throw new OptionException($"Error with patch file: {e.Message}", "-l|--log");
        }

        _inputs.SaveLog = true;
        _inputs.LogPath = file;
    }

    private void ShowHelp(string input = "") {
        Console.WriteLine(
            "MapMarkerRefType\n" 
          + "--------------------------------------------------------\n"
          + "This script loads the game environment\n"
          + "in AppData\\Local\\Skyrim Special Edition\\plugin.txt\n"
          + "Options:"
        );

        foreach (var opt in _options) {
            var output = opt.GetNames().Aggregate(string.Empty, (current, name) => current + name + "|")[1..];

            //Append the option description
            output += " -- " + opt.Description;

            //Write the output to the console
            Console.WriteLine(output);
        }
    }
}