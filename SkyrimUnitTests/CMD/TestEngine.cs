﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Plugins.Binary.Parameters;
using Noggog;
using SkyrimUnitTests.TestSuite.Tester.Record;
namespace SkyrimUnitTests.CMD;

public static class TestEngine {
    public static void Run(string[] args) {
        var parser = new ArgumentParser(args);
        var opts = parser.Parse();

        Warmup.Init();
        
        //TODO add setting saving/loading
        //TODO add setting modification in GUI

        //Load tests
        foreach (var test in IRecordTest.Tests) {
            var time = Stopwatch.StartNew();
            Console.WriteLine($"Testing {test.Name}");

            //Apply settings
            //test.GetBugs();

            //Run test
            foreach (var mod in IRecordTest.Environment.LoadOrder.Items.Select(i => i.Mod).NotNull()) {
                if (test.Verify(mod)) {
                    time.Stop();
                    Console.WriteLine($"Verify Time: {time.Elapsed}");

                    Console.WriteLine("Success");
                } else {
                    time.Stop();

                    //Show errors
                    var errorOutput = test.ToString();

                    Console.WriteLine("Failure");
                    // Console.WriteLine("The forms did not pass the test criteria:");
                    // Console.WriteLine(errorOutput);
                    Console.WriteLine($"Verify Time: {time.Elapsed}\n");

                    if (opts.SaveLog) File.WriteAllText(opts.LogPath, errorOutput);

                    //Apply patch
                    if (!opts.AttemptToPatch) continue;

                    Console.WriteLine($"Patching {test.Name}");
                    time.Reset();
                    time.Start();
                    test.Patch(IRecordTest.PatchMod);
                    time.Stop();
                    Console.WriteLine($"Patch Time: {time.Elapsed}");
                    time.Reset();
                    Console.WriteLine($"Finished Patching {test.Name}");

                    errorOutput = test.ToString();
                    if (errorOutput == "") {
                        Console.WriteLine("Everything was patched successfully");
                    } else {
                        Console.WriteLine("Result after patching");
                        Console.WriteLine("The forms will need to be fixed manually:");
                        Console.WriteLine(errorOutput);
                    }
                }

                Console.WriteLine();
            }
        }

        //Save patch
        var param = BinaryWriteParameters.Default;
        param.ModKey = ModKeyOption.NoCheck;
        IRecordTest.PatchMod.WriteToBinaryParallel(opts.PatchPath, param);
    }
}
