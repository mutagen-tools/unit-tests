﻿using System.Windows;
using System.Windows.Media;
using SkyrimUnitTests.TestSuite.Tester;
namespace SkyrimUnitTests.UI; 

public record SeverityStates(BugSeverity Severity) {
    public BugSeverity Severity { get; } = Severity;
    public SolidColorBrush Color { get; } = IBug.SeverityColors[Severity];
    public bool Enabled { get; set; } = true;
}

public partial class MainWindow {
    public delegate void SeverityUpdateEvent();
    public static event SeverityUpdateEvent? SeverityUpdated;
    public static void UpdateSeverity() => SeverityUpdated?.Invoke();
    
    public MainWindow() {
        App.UpdateTheme(this);
        InitializeComponent();

        DataContext = MainViewModel.Instance;
    }
    
    private void Severity_OnToggle(object sender, RoutedEventArgs e) {
        UpdateSeverity();
    }
}