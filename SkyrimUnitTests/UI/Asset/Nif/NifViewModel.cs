﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using SkyrimUnitTests.TestSuite.Tester.Asset.Nif;
namespace SkyrimUnitTests.UI.Asset.Nif;

public class NifViewModel : ReactiveObject {
    public INifTest Test { get; set; }
    public ObservableCollection<INifBug> Bugs { get; } = new();
    public ObservableCollection<BuggedNif> BuggedItems { get; } = new();
    
    [Reactive]
    public float ProgressPercentage { get; set; }
    [Reactive]
    public string ProgressString { get; set; } = string.Empty;
    [Reactive]
    public bool IsWorking { get; set; }

    private void OnProgressUpdate(string message, float progress) {
        ProgressString = message;
        ProgressPercentage = progress;
    }

    //Test
    [Reactive]
    public bool IsTesting { get; set; }
    
    [Reactive]
    public bool WasTested { get; set; }

    //Patch
    [Reactive]
    public bool IsPatching { get; set; }
    [Reactive]
    public INifBug? SelectedBug { get; set; }
    
    private NifViewModel(INifTest test) {
        Test = test;
        Test.Progress += OnProgressUpdate;
        UpdateBugs();
        MainWindow.SeverityUpdated += UpdateBugs;

        this.WhenAnyValue(v => v.SelectedBug)!
            .Subscribe(new Action<object>(_ => UpdateBuggedRecords()));
    }

    public static readonly Dictionary<INifTest, NifViewModel> TestViewModels = new(); 

    public static NifViewModel GetTestViewModel(INifTest test) {
        if (TestViewModels.ContainsKey(test)) {
            return TestViewModels[test];
        }
        
        var testViewModel = new NifViewModel(test);
        TestViewModels.Add(test, testViewModel);
        return testViewModel;
    }
    
    public async void Verify(INifBug? bug = default) {
        IsTesting = true;
        IsWorking = true;
        await Task.Run(() => Test.Verify(NifUnitTestViewModel.Instance.Directory, bug));
        IsTesting = false;
        WasTested = true;
        IsWorking = false;
        ProgressPercentage = 0;
        ProgressString = string.Empty;

        UpdateBuggedRecords();
    }
    public async void Patch(INifBug? bug = default, BuggedNif? buggedItem = default) {
        IsPatching = true;
        IsWorking = true;
        if (buggedItem != null && bug != null) {
            await Task.Run(() => Test.Patch(NifUnitTestViewModel.Instance.Directory, bug, buggedItem));
        } else if (bug != null) {
            await Task.Run(() => Test.Patch(NifUnitTestViewModel.Instance.Directory, bug));
        } else {
            await Task.Run(() => Test.Patch(NifUnitTestViewModel.Instance.Directory));
        }
        IsPatching = false;
        IsWorking = false;
        ProgressPercentage = 0;
        ProgressString = string.Empty;

        UpdateBuggedRecords();
    }

    private void UpdateBugs() {
        while (Bugs.Count > 0) Bugs.RemoveAt(0);

        foreach (var record in Test.Bugs
            .Where(b => MainViewModel.SeverityStates
                .First(s => s.Severity == b.Severity).Enabled)) {
            Bugs.Add(record);
        }
    }

    private void UpdateBuggedRecords() {
        while (BuggedItems.Count > 0) BuggedItems.RemoveAt(0);
        
        if (SelectedBug == null) return;
        
        foreach (var buggedItem in SelectedBug.BuggedItems.ToImmutableSortedSet()) {
            BuggedItems.Add(buggedItem);
        }
    }
}
