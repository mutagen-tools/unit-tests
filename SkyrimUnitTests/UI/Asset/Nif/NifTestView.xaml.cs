﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using SkyrimUnitTests.TestSuite.Tester.Asset.Nif;
namespace SkyrimUnitTests.UI.Asset.Nif; 

public partial class NifTestView {
    public static readonly DependencyProperty TestProperty = DependencyProperty.Register(
        "Test",
        typeof(INifTest), 
        typeof(NifTestView), 
        new PropertyMetadata(null)
    );
    public INifTest Test {
        get => (INifTest) GetValue(TestProperty);
        set => SetValue(TestProperty, value);
    }
    
    public NifViewModel ViewModel { get; set;  }
    
    public NifTestView() {
        InitializeComponent();
        
        DependencyPropertyDescriptor.FromProperty(TestProperty, typeof(NifTestView))?.AddValueChanged(this, delegate {
            ViewModel = NifViewModel.GetTestViewModel(Test);
        });
    }

    private void TestBug_OnClick(object sender, RoutedEventArgs e) {
        if ((sender as Button)?.DataContext is not INifBug bug) return;

        ViewModel.Verify(bug);
    }
    private void TestTest_OnClick(object sender, RoutedEventArgs e) => ViewModel.Verify();
}