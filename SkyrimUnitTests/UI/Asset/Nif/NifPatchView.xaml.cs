﻿using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using SkyrimUnitTests.TestSuite.Tester.Asset.Nif;
namespace SkyrimUnitTests.UI.Asset.Nif; 

public partial class NifPatchView {
    public static readonly RoutedUICommand GenericCommand = new("Some Command", "SomeCommand", typeof(NifPatchView));

    public static readonly DependencyProperty TestProperty = DependencyProperty.Register(
        "Test",
        typeof(INifTest), 
        typeof(NifPatchView), 
        new PropertyMetadata(null)
    );
    
    public INifTest Test {
        get => (INifTest) GetValue(TestProperty);
        set => SetValue(TestProperty, value);
    }

    public NifViewModel ViewModel { get; set; }
    
    public NifPatchView() {
        InitializeComponent();
        
        DependencyPropertyDescriptor.FromProperty(TestProperty, typeof(NifPatchView))?.AddValueChanged(this, delegate {
            ViewModel = NifViewModel.GetTestViewModel(Test);
        });
    }

    private void PatchNif_OnClick(object sender, RoutedEventArgs e) {
        if ((sender as Button)?.DataContext is not BuggedNif identifier || ViewModel.SelectedBug == null) return;

        ViewModel.Patch(ViewModel.SelectedBug, identifier);
    }

    private void PatchBug_OnClick(object sender, RoutedEventArgs e) {
        if ((sender as Button)?.DataContext is not INifBug bug) return;

        ViewModel.Patch(bug);
    }
    
    private void PatchTest_OnClick(object sender, RoutedEventArgs e) => ViewModel.Patch();

    private static void CopyEntries(IEnumerable list) {
        var clipboard = new StringBuilder();
        
        foreach (var item in list) {
            if (item is not BuggedNif bug) continue;

            clipboard.AppendLine(bug.ToString());
        }

        var copy = clipboard.ToString();
        if (copy.Length != 0) Clipboard.SetText(copy);
    }

    private void CopyListEntries(object sender, ExecutedRoutedEventArgs e) {
        if (sender is not MenuItem { Parent: ContextMenu { PlacementTarget: ListView list } }) return;

        CopyEntries(list.SelectedItems);
    }
    
    private void CopyTabEntries(object sender, ExecutedRoutedEventArgs e) {
        if (ViewModel.SelectedBug == null) return;
        
        CopyEntries(ViewModel.SelectedBug.BuggedItems);
    }
    
    private void CopyAllEntries(object sender, ExecutedRoutedEventArgs e) {
        CopyEntries(ViewModel.Bugs.SelectMany(b => b.BuggedItems));
    }

    private void OpenPath(object sender, RoutedEventArgs e) {
        if ((sender as Button)?.DataContext is not BuggedNif buggedNif) return;

        Process.Start(new ProcessStartInfo {
            FileName = buggedNif.FullPath,
            UseShellExecute = true,
            Verb = "open"
        });
    }
}