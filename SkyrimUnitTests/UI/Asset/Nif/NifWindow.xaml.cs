﻿using System.Windows;
namespace SkyrimUnitTests.UI.Asset.Nif; 

public partial class NifWindow {
    public NifWindow() {
        App.UpdateTheme(this);
        InitializeComponent();

        DataContext = NifUnitTestViewModel.Instance;
    }

    private void TestEverything_OnClick(object sender, RoutedEventArgs e) {
        foreach (var (_, model) in NifViewModel.TestViewModels) model.Verify();
    }
    
    private void PatchEverything_OnClick(object sender, RoutedEventArgs e) {
        foreach (var (_, model) in NifViewModel.TestViewModels) model.Patch();
    }
    
    private void DirectoryChooser_OnDirectorySelectionChanged(object sender, RoutedEventArgs e) {
        NifUnitTestViewModel.Instance.Directory = DirectoryChooser.GetSelectedDirectory() ?? string.Empty;
    }
    private void OpenDirectory_OnClick(object sender, RoutedEventArgs e) => NifUnitTestViewModel.Instance.OpenDirectory();
}