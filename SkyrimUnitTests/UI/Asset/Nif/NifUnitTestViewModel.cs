﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using ReactiveUI;
using SkyrimUnitTests.TestSuite.Tester.Asset.Nif;
namespace SkyrimUnitTests.UI.Asset.Nif;

public class NifUnitTestViewModel : ReactiveObject {
    public static readonly NifUnitTestViewModel Instance = new();

    public ObservableCollection<string> Directories { get; set; } = new();

    public string Directory { get; set; } = string.Empty;
    public IEnumerable<INifTest> Tests { get; set; } = INifTest.Tests;

    public void OpenDirectory() {
        if (System.IO.Directory.Exists(Directory)) {
            Process.Start("explorer.exe", Directory);
        }
    }
}
