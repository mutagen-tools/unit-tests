﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using SkyrimUnitTests.TestSuite.Tester.Record;
namespace SkyrimUnitTests.UI.Record;

public class RecordViewModel : ReactiveObject {
    public IRecordTest Test { get; set; }
    public ObservableCollection<IRecordBug> Bugs { get; } = new();
    public ObservableCollection<BuggedRecord> BuggedItems { get; } = new();

    [Reactive]
    public float ProgressPercentage { get; set; }
    [Reactive]
    public string ProgressString { get; set; } = string.Empty;
    [Reactive]
    public bool IsWorking { get; set; }

    private void OnProgressUpdate(string message, float progress) {
        ProgressString = message;
        ProgressPercentage = progress;
    }

    //Test
    [Reactive]
    public bool IsTesting { get; set; }
    
    [Reactive]
    public bool WasTested { get; set; }

    //Patch
    [Reactive]
    public bool IsPatching { get; set; }
    [Reactive]
    public IRecordBug? SelectedBug { get; set; }
    
    private RecordViewModel(IRecordTest test)  {
        Test = test;
        Test.Progress += OnProgressUpdate;
        UpdateBugs();
        MainWindow.SeverityUpdated += UpdateBugs;

        this.WhenAnyValue(v => v.SelectedBug)!
            .Subscribe(new Action<object>(_ => UpdateBuggedRecords()));
    }

    public static readonly Dictionary<IRecordTest, RecordViewModel> TestViewModels = new(); 

    public static RecordViewModel GetTestViewModel(IRecordTest test) {
        if (TestViewModels.ContainsKey(test)) {
            return TestViewModels[test];
        }
        
        var testViewModel = new RecordViewModel(test);
        TestViewModels.Add(test, testViewModel);
        return testViewModel;
    }
    
    public async void Verify(IRecordBug? bug = default) {
        IsTesting = true;
        IsWorking = true;
        await Task.Run(() => Test.Verify(RecordUnitTestViewModel.Instance.CurrentMod, bug));
        IsTesting = false;
        WasTested = true;
        IsWorking = false;
        ProgressPercentage = 0;
        ProgressString = string.Empty;

        UpdateBuggedRecords();
    }
    
    public async void Patch(IRecordBug? bug = default, BuggedRecord? buggedItem = default) {
        IsPatching = true;
        IsWorking = true;
        if (buggedItem != null && bug != null) {
            await Task.Run(() => Test.Patch(IRecordTest.PatchMod, bug, buggedItem));
        } else if (bug != null) {
            await Task.Run(() => Test.Patch(IRecordTest.PatchMod, bug));
        } else {
            await Task.Run(() => Test.Patch(IRecordTest.PatchMod));
        }
        IsPatching = false;
        IsWorking = false;
        ProgressPercentage = 0;
        ProgressString = string.Empty;

        UpdateBuggedRecords();
    }

    private void UpdateBugs() {
        while (Bugs.Count > 0) Bugs.RemoveAt(0);

        foreach (var record in Test.Bugs
            .Where(b => MainViewModel.SeverityStates
                .First(s => s.Severity == b.Severity).Enabled)) {
            Bugs.Add(record);
        }
    }

    private void UpdateBuggedRecords() {
        while (BuggedItems.Count > 0) BuggedItems.RemoveAt(0);
        
        if (SelectedBug == null) return;
        
        foreach (var buggedItem in SelectedBug.BuggedItems.ToImmutableSortedSet()) {
            BuggedItems.Add(buggedItem);
        }
    }
}
