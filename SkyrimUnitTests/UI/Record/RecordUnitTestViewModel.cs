﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Skyrim;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using SkyrimUnitTests.TestSuite.Tester.Record;
namespace SkyrimUnitTests.UI.Record;

public class RecordUnitTestViewModel : ReactiveObject {
    private static readonly string OutputFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Output");
    public static readonly RecordUnitTestViewModel Instance = new();

    [Reactive]
    public int LoadOrderIndex { get; set; }
    
    public IEnumerable<ModKey> LoadOrder { get; set; } = IRecordTest.Environment.LoadOrder.Select(m => m.Value.ModKey);
    public ISkyrimModGetter CurrentMod { get; private set; }
    public IEnumerable<IRecordTest> Tests { get; set; } = IRecordTest.Tests;
    
    private RecordUnitTestViewModel() {
        this.WhenAnyValue(v => v.LoadOrderIndex)
            .Subscribe(_ => {
                var skyrimModGetter = IRecordTest.Environment.LoadOrder[LoadOrderIndex].Mod;
                if (skyrimModGetter != null) CurrentMod = skyrimModGetter;
            });
    }
    
    public void WritePatch() {
        Directory.CreateDirectory(OutputFolder);
        IRecordTest.PatchMod.WriteToBinaryParallel(Path.Combine(OutputFolder, IRecordTest.PatchMod.ModKey.FileName));
    }

    public void OpenPatchFolder() {
        Directory.CreateDirectory(OutputFolder);
        Process.Start("explorer.exe", OutputFolder);
    }
}
