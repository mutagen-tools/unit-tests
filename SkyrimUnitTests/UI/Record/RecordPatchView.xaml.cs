﻿using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Noggog;
using SkyrimUnitTests.TestSuite.Tester.Record;
namespace SkyrimUnitTests.UI.Record; 

public partial class RecordPatchView {
    public static readonly RoutedUICommand GenericCommand = new("Some Command", "SomeCommand", typeof(RecordTestView));
    
    public static readonly DependencyProperty TestProperty = DependencyProperty.Register(
        "Test",
        typeof(IRecordTest), 
        typeof(RecordPatchView), 
        new PropertyMetadata(null)
    );
    
    public IRecordTest Test {
        get => (IRecordTest) GetValue(TestProperty);
        set => SetValue(TestProperty, value);
    }

    public RecordViewModel ViewModel { get; set; }
    
    public RecordPatchView() {
        InitializeComponent();
        
        DependencyPropertyDescriptor.FromProperty(TestProperty, typeof(RecordPatchView))?.AddValueChanged(this, delegate {
            ViewModel = RecordViewModel.GetTestViewModel(Test);
        });
    }

    private void PatchRecord_OnClick(object sender, RoutedEventArgs e) {
        if ((sender as Button)?.DataContext is not BuggedRecord identifier || ViewModel.SelectedBug == null) return;

        ViewModel.Patch(ViewModel.SelectedBug, identifier);
    }

    private void PatchBug_OnClick(object sender, RoutedEventArgs e) {
        if ((sender as Button)?.DataContext is not IRecordBug bug) return;

        ViewModel.Patch(bug);
    }
    
    private void PatchTest_OnClick(object sender, RoutedEventArgs e) => ViewModel.Patch();

    private static void CopyEntries(IEnumerable list) {
        var clipboard = new StringBuilder();
        
        foreach (var bug in list.OfType<BuggedRecord>().OrderBy(x => x.EditorID)) {
            clipboard.AppendLine(bug.ToString());
        }

        var copy = clipboard.ToString();
        if (copy.Length != 0) Clipboard.SetText(copy);
    }

    private void CopyListEntries(object sender, ExecutedRoutedEventArgs e) {
        if (sender is not MenuItem { Parent: ContextMenu { PlacementTarget: TreeView tree } }) return;

        CopyEntries(tree.SelectedItem.AsEnumerable());
    }
    
    private void CopyTabEntries(object sender, ExecutedRoutedEventArgs e) {
        if (ViewModel.SelectedBug == null) return;
        
        CopyEntries(ViewModel.SelectedBug.BuggedItems);
    }
    
    private void CopyAllEntries(object sender, ExecutedRoutedEventArgs e) {
        CopyEntries(ViewModel.Bugs.SelectMany(b => b.BuggedItems));
    }
}