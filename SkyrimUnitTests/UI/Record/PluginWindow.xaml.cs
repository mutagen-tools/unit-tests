﻿using System.Windows;
namespace SkyrimUnitTests.UI.Record; 

public partial class PluginWindow {
    public PluginWindow() {
        App.UpdateTheme(this);
        InitializeComponent();

        DataContext = RecordUnitTestViewModel.Instance;
    }
    
    private void SaveOutput_OnClick(object sender, RoutedEventArgs e) => RecordUnitTestViewModel.Instance.WritePatch();

    private void OpenOutput_OnClick(object sender, RoutedEventArgs e) => RecordUnitTestViewModel.Instance.OpenPatchFolder();

    private void TestEverything_OnClick(object sender, RoutedEventArgs e) {
        foreach (var (_, model) in RecordViewModel.TestViewModels) model.Verify();
    }
    
    private void PatchEverything_OnClick(object sender, RoutedEventArgs e) {
        foreach (var (_, model) in RecordViewModel.TestViewModels) model.Patch();
    }
}