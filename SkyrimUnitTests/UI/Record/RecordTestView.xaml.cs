﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using SkyrimUnitTests.TestSuite.Tester.Record;
namespace SkyrimUnitTests.UI.Record; 

public partial class RecordTestView {
    public static readonly DependencyProperty TestProperty = DependencyProperty.Register(
        "Test",
        typeof(IRecordTest), 
        typeof(RecordTestView), 
        new PropertyMetadata(null)
    );
    public IRecordTest Test {
        get => (IRecordTest) GetValue(TestProperty);
        set => SetValue(TestProperty, value);
    }
    
    public RecordViewModel ViewModel { get; set;  }
    
    public RecordTestView() {
        InitializeComponent();
        
        DependencyPropertyDescriptor.FromProperty(TestProperty, typeof(RecordTestView))?.AddValueChanged(this, delegate {
            ViewModel = RecordViewModel.GetTestViewModel(Test);
        });
    }

    private void TestBug_OnClick(object sender, RoutedEventArgs e) {
        if ((sender as Button)?.DataContext is not IRecordBug bug) return;

        ViewModel.Verify(bug);
    }
    private void TestTest_OnClick(object sender, RoutedEventArgs e) => ViewModel.Verify();
}