﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using ReactiveUI;
using SkyrimUnitTests.TestSuite.Tester;
namespace SkyrimUnitTests.UI;

public class MainViewModel : ReactiveObject {
    public static readonly MainViewModel Instance = new();
    
    public static ObservableCollection<SeverityStates> SeverityStates { get; } = new(Enum.GetValues<BugSeverity>().Select(s => new SeverityStates(s)));
}
